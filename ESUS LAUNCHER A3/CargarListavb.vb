﻿Imports System.Net
Imports System.IO

Public Class CargarListavb
    Dim arma3 As String = Nothing
    Private Sub CargarListavb_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        If Form1.FlatCheckBox16.Checked = True Then
            arma3 = Form1.FlatTextBox8.Text
        Else
            Try
                arma3 = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\bohemia interactive\arma 3", "main", Nothing)
            Catch ex As Exception
            End Try
            If IsNothing(arma3) Then
                Try
                    Dim steam As String
                    steam = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\Valve\Steam", "SteamPath", Nothing)
                    steam = steam.Replace("/", "\")
                    arma3 = steam & "\steamapps\common\Arma 3"
                Catch ex As Exception
                End Try
            Else
            End If
        End If
    End Sub
    Private Sub FlatComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles FlatComboBox1.SelectedIndexChanged
        Dim dias As String
        dias = FlatComboBox1.SelectedItem.ToString
        RichTextBox1.Clear()
        If dias = "LUNES" Then
            Try
                Dim instance As WebClient = New WebClient
                Dim address As String = "https://dl.dropbox.com/s/fmixrx85uduqqa8/lunes.txt"
                FlatLabel1.Visible = True
                RichTextBox1.Text = instance.DownloadString(address)
                FlatLabel1.Visible = False
            Catch ex As WebException
                MessageBox.Show(ex.ToString)
            End Try
        End If
        If dias = "MARTES" Then
            Try
                Dim instance As WebClient = New WebClient
                Dim address As String = "https://www.dropbox.com/s/lj56r2thmzsp82y/martes.txt?raw=1"
                FlatLabel1.Visible = True
                RichTextBox1.Text = instance.DownloadString(address)
                FlatLabel1.Visible = False
            Catch ex As WebException
                MessageBox.Show(ex.ToString)
            End Try
        End If
        If dias = "MIÉRCOLES" Then
            Try
                Dim instance As WebClient = New WebClient
                Dim address As String = "https://www.dropbox.com/s/bc5t7v5ajazlcu7/miercoles.txt?raw=1"
                FlatLabel1.Visible = True
                RichTextBox1.Text = instance.DownloadString(address)
                FlatLabel1.Visible = False
            Catch ex As WebException
                MessageBox.Show(ex.ToString)
            End Try
        End If
        If dias = "JUEVES" Then
            Try
                Dim instance As WebClient = New WebClient
                Dim address As String = "https://www.dropbox.com/s/y518is09c48js2f/jueves.txt?raw=1"
                FlatLabel1.Visible = True
                RichTextBox1.Text = instance.DownloadString(address)
                FlatLabel1.Visible = False
            Catch ex As WebException
                MessageBox.Show(ex.ToString)
            End Try
        End If
        If dias = "VIERNES" Then
            Try
                Dim instance As WebClient = New WebClient
                Dim address As String = "https://www.dropbox.com/s/wcht0q0d3q08otw/viernes.txt?raw=1"
                FlatLabel1.Visible = True
                RichTextBox1.Text = instance.DownloadString(address)
                FlatLabel1.Visible = False
            Catch ex As WebException
                MessageBox.Show(ex.ToString)
            End Try
        End If
        If dias = "SÁBADO" Then
            Try
                Dim instance As WebClient = New WebClient
                Dim address As String = "https://www.dropbox.com/s/z5ecuta3t8xs8tn/sabado.txt?raw=1"
                FlatLabel1.Visible = True
                RichTextBox1.Text = instance.DownloadString(address)
                FlatLabel1.Visible = False
            Catch ex As WebException
                MessageBox.Show(ex.ToString)
            End Try
        End If
        If dias = "DOMINGO" Then
            Try

                Dim instance As WebClient = New WebClient
                Dim address As String = "https://www.dropbox.com/s/0odh9iz5lq2tl7t/domingo.txt?raw=1"
                FlatLabel1.Visible = True
                RichTextBox1.Text = instance.DownloadString(address)
                FlatLabel1.Visible = False
            Catch ex As WebException
                MessageBox.Show(ex.ToString)
            End Try
        End If
        If dias = "PERMANENTE" Then
            Try
                Dim instance As WebClient = New WebClient
                Dim address As String = "https://www.dropbox.com/s/tp1wv9fpl3rci99/s.permanente.txt?raw=1"
                FlatLabel1.Visible = True
                RichTextBox1.Text = instance.DownloadString(address)
                FlatLabel1.Visible = False
            Catch ex As WebException
                MessageBox.Show(ex.ToString)
            End Try
        End If
        If dias = "OTROS" Then
            Try
                Dim instance As WebClient = New WebClient
                Dim address As String = "https://www.dropbox.com/s/a3d4k1qpqy499ut/otros.txt?raw=1"
                FlatLabel1.Visible = True
                RichTextBox1.Text = instance.DownloadString(address)
                FlatLabel1.Visible = False
            Catch ex As WebException
                MessageBox.Show(ex.ToString)
            End Try
        End If
        If dias = "ACADEMIA 1" Then
            Try
                Dim instance As WebClient = New WebClient
                Dim address As String = "https://www.dropbox.com/s/djz5gv25vtzmtik/academia.txt?raw=1"
                FlatLabel1.Visible = True
                RichTextBox1.Text = instance.DownloadString(address)
                FlatLabel1.Visible = False
            Catch ex As WebException
                MessageBox.Show(ex.ToString)
            End Try
        End If
        If dias = "ACADEMIA 2" Then
            Try
                Dim instance As WebClient = New WebClient
                Dim address As String = "https://www.dropbox.com/s/aio7iusa9gs788f/Academia2.txt?raw=1"
                FlatLabel1.Visible = True
                RichTextBox1.Text = instance.DownloadString(address)
                FlatLabel1.Visible = False
            Catch ex As WebException
                MessageBox.Show(ex.ToString)
            End Try
        End If
        If dias = "PRUEBAS" Then
            Try
                Dim instance As WebClient = New WebClient
                Dim address As String = "https://www.dropbox.com/s/7zsa1mws02pmmob/Pruebas.txt?raw=1"
                FlatLabel1.Visible = True
                RichTextBox1.Text = instance.DownloadString(address)
                FlatLabel1.Visible = False
            Catch ex As WebException
                MessageBox.Show(ex.ToString)
            End Try
        End If
        If dias = "LOADOUT" Then
            Try
                Dim instance As WebClient = New WebClient
                Dim address As String = "https://www.dropbox.com/s/ogdckle7g2a4ryb/loadout.txt?raw=1"
                FlatLabel1.Visible = True
                RichTextBox1.Text = instance.DownloadString(address)
                FlatLabel1.Visible = False
            Catch ex As WebException
                MessageBox.Show(ex.ToString)
            End Try
        End If
    End Sub

    Private Sub FlatButton1_Click(sender As Object, e As EventArgs) Handles FlatButton1.Click
        If RichTextBox1.Text <> "" Then
            Me.RichTextBox1.Lines = Me.RichTextBox1.Text.Split(New Char() {ControlChars.Lf}, _
                                                           StringSplitOptions.RemoveEmptyEntries)
            Dim lines() As String = RichTextBox1.Lines
            CheckedListBox1.Items.Clear()
            CheckedListBox1.Items.AddRange(lines)
            Dim marcar As Boolean = True
            For i As Integer = 0 To CheckedListBox1.Items.Count - 1
                CheckedListBox1.SetItemChecked(i, marcar)
            Next
            Dim desmarcar As Boolean = False
            For i As Integer = 0 To Form1.CheckedListBox1.Items.Count - 1
                Form1.CheckedListBox1.SetItemChecked(i, desmarcar)
            Next
            Dim elegido As String = Nothing
            For Each itemChecked In CheckedListBox1.CheckedItems
                elegido = itemChecked
                Try
                    Dim index As Integer = Form1.CheckedListBox1.FindString(elegido)
                    If (index <> -1) Then
                        Form1.CheckedListBox1.SetItemChecked(index, True)
                    End If
                Catch ex As Exception
                End Try
            Next
            Form1.ListBox1.Items.Clear()
            If Form1.CheckedListBox1.CheckedItems.Count > 0 Then
                For i As Integer = 0 To Form1.CheckedListBox1.CheckedItems.Count - 1
                    Form1.ListBox1.Items.Add(Form1.CheckedListBox1.CheckedItems(i))
                Next
            Else
                Form1.ListBox1.Items.Clear()
            End If
        End If
        Dim rutaaddons As String
        For Each line In RichTextBox1.Lines
            rutaaddons = arma3 & "\" & line
            If My.Computer.FileSystem.DirectoryExists(rutaaddons) Then
            Else
                MessageBox.Show("El addon " & line & " no ha podido ser localizado entre sus addons, por favor revise Arma3Sync.", _
                                                  "ATENCIÓN!!", _
                                                  MessageBoxButtons.OK, _
                                                  MessageBoxIcon.Warning, _
                                                  MessageBoxDefaultButton.Button1)
            End If
        Next
        Form1.FlatTextBox9.Text = Form1.CheckedListBox1.Items.Count
        Form1.FlatTextBox6.Text = Form1.CheckedListBox1.CheckedItems.Count
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Me.Close()
    End Sub


    Private Sub FormSkin1_Click(sender As Object, e As EventArgs) Handles FormSkin1.Click

    End Sub
    Private Sub FormSkin1_Click_1(sender As Object, e As EventArgs)

    End Sub

    Private Sub CopiarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CopiarToolStripMenuItem.Click
        Try
            RichTextBox1.Copy()
        Catch ex As Exception
            MessageBox.Show("No hay nada para copiar")
        End Try
    End Sub

    Private Sub PegarToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles PegarToolStripMenuItem.Click
        Try
            RichTextBox1.Paste()
        Catch ex As Exception
            MessageBox.Show("No hay nada para copiar")
        End Try
    End Sub

    Private Sub RichTextBox1_TextChanged(sender As Object, e As EventArgs) Handles RichTextBox1.TextChanged
    End Sub
End Class