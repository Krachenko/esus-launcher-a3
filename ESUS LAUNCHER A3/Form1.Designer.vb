﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits MaterialSkin.Controls.MaterialForm

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Form1))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.ContextMenuStrip2 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.CopiarParametrosToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.LanzarArma3exeToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.FolderBrowserDialog1 = New System.Windows.Forms.FolderBrowserDialog()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.ContextMenuStrip1 = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.CopiarToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolTip1 = New System.Windows.Forms.ToolTip(Me.components)
        Me.BottomToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.TopToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.RightToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.LeftToolStripPanel = New System.Windows.Forms.ToolStripPanel()
        Me.ContentPanel = New System.Windows.Forms.ToolStripContentPanel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.MaterialRaisedButton10 = New MaterialSkin.Controls.MaterialRaisedButton()
        Me.MaterialRaisedButton11 = New MaterialSkin.Controls.MaterialRaisedButton()
        Me.CheckBox1 = New System.Windows.Forms.CheckBox()
        Me.FlatLabel10 = New ESUS_LAUNCHER_A3.FlatLabel()
        Me.FlatComboBox1 = New ESUS_LAUNCHER_A3.FlatComboBox()
        Me.FlatComboBox3 = New ESUS_LAUNCHER_A3.FlatComboBox()
        Me.FlatToggle1 = New ESUS_LAUNCHER_A3.FlatToggle()
        Me.FlatTabControl1 = New ESUS_LAUNCHER_A3.FlatTabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.FlatGroupBox1 = New ESUS_LAUNCHER_A3.FlatGroupBox()
        Me.FlatLabel17 = New ESUS_LAUNCHER_A3.FlatLabel()
        Me.FlatTextBox9 = New ESUS_LAUNCHER_A3.FlatTextBox()
        Me.FlatLabel16 = New ESUS_LAUNCHER_A3.FlatLabel()
        Me.FlatTextBox6 = New ESUS_LAUNCHER_A3.FlatTextBox()
        Me.FlatLabel15 = New ESUS_LAUNCHER_A3.FlatLabel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.FlatLabel9 = New ESUS_LAUNCHER_A3.FlatLabel()
        Me.FlatLabel8 = New ESUS_LAUNCHER_A3.FlatLabel()
        Me.MaterialRaisedButton1 = New MaterialSkin.Controls.MaterialRaisedButton()
        Me.CheckedListBox2 = New System.Windows.Forms.CheckedListBox()
        Me.RichTextBox1 = New System.Windows.Forms.RichTextBox()
        Me.RichTextBox2 = New System.Windows.Forms.RichTextBox()
        Me.MaterialRaisedButton5 = New MaterialSkin.Controls.MaterialRaisedButton()
        Me.MaterialRaisedButton4 = New MaterialSkin.Controls.MaterialRaisedButton()
        Me.MaterialRaisedButton3 = New MaterialSkin.Controls.MaterialRaisedButton()
        Me.MaterialRaisedButton2 = New MaterialSkin.Controls.MaterialRaisedButton()
        Me.CheckedListBox1 = New System.Windows.Forms.CheckedListBox()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.ListBox3 = New System.Windows.Forms.ListBox()
        Me.MaterialRaisedButton9 = New MaterialSkin.Controls.MaterialRaisedButton()
        Me.MaterialRaisedButton8 = New MaterialSkin.Controls.MaterialRaisedButton()
        Me.MaterialRaisedButton7 = New MaterialSkin.Controls.MaterialRaisedButton()
        Me.MaterialRaisedButton6 = New MaterialSkin.Controls.MaterialRaisedButton()
        Me.ListBox1 = New System.Windows.Forms.ListBox()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.WebBrowser1 = New System.Windows.Forms.WebBrowser()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.Button9 = New System.Windows.Forms.Button()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.FlatLabel11 = New ESUS_LAUNCHER_A3.FlatLabel()
        Me.Button6 = New System.Windows.Forms.Button()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.FlatButton2 = New ESUS_LAUNCHER_A3.FlatButton()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.Column1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column3 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column4 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column5 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column6 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column7 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column9 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Column8 = New System.Windows.Forms.DataGridViewButtonColumn()
        Me.PictureBox11 = New System.Windows.Forms.PictureBox()
        Me.PictureBox10 = New System.Windows.Forms.PictureBox()
        Me.PictureBox9 = New System.Windows.Forms.PictureBox()
        Me.PictureBox8 = New System.Windows.Forms.PictureBox()
        Me.PictureBox7 = New System.Windows.Forms.PictureBox()
        Me.PictureBox6 = New System.Windows.Forms.PictureBox()
        Me.PictureBox5 = New System.Windows.Forms.PictureBox()
        Me.PictureBox4 = New System.Windows.Forms.PictureBox()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.PictureBox2 = New System.Windows.Forms.PictureBox()
        Me.TabPage4 = New System.Windows.Forms.TabPage()
        Me.FlatButton7 = New ESUS_LAUNCHER_A3.FlatButton()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.FlatLabel12 = New ESUS_LAUNCHER_A3.FlatLabel()
        Me.TextBox6 = New System.Windows.Forms.TextBox()
        Me.TextBox10 = New System.Windows.Forms.TextBox()
        Me.TextBox9 = New System.Windows.Forms.TextBox()
        Me.FlatCheckBox19 = New ESUS_LAUNCHER_A3.FlatCheckBox()
        Me.FlatLabel14 = New ESUS_LAUNCHER_A3.FlatLabel()
        Me.FlatLabel13 = New ESUS_LAUNCHER_A3.FlatLabel()
        Me.FlatCheckBox18 = New ESUS_LAUNCHER_A3.FlatCheckBox()
        Me.FlatComboBox4 = New ESUS_LAUNCHER_A3.FlatComboBox()
        Me.FlatProgressBar1 = New ESUS_LAUNCHER_A3.FlatProgressBar()
        Me.FlatCheckBox17 = New ESUS_LAUNCHER_A3.FlatCheckBox()
        Me.FlatCheckBox15 = New ESUS_LAUNCHER_A3.FlatCheckBox()
        Me.FlatCheckBox14 = New ESUS_LAUNCHER_A3.FlatCheckBox()
        Me.FlatCheckBox13 = New ESUS_LAUNCHER_A3.FlatCheckBox()
        Me.FlatCheckBox12 = New ESUS_LAUNCHER_A3.FlatCheckBox()
        Me.FlatCheckBox11 = New ESUS_LAUNCHER_A3.FlatCheckBox()
        Me.FlatCheckBox10 = New ESUS_LAUNCHER_A3.FlatCheckBox()
        Me.FlatCheckBox9 = New ESUS_LAUNCHER_A3.FlatCheckBox()
        Me.FlatCheckBox8 = New ESUS_LAUNCHER_A3.FlatCheckBox()
        Me.FlatCheckBox7 = New ESUS_LAUNCHER_A3.FlatCheckBox()
        Me.FlatCheckBox6 = New ESUS_LAUNCHER_A3.FlatCheckBox()
        Me.FlatCheckBox5 = New ESUS_LAUNCHER_A3.FlatCheckBox()
        Me.FlatCheckBox4 = New ESUS_LAUNCHER_A3.FlatCheckBox()
        Me.FlatCheckBox3 = New ESUS_LAUNCHER_A3.FlatCheckBox()
        Me.FlatCheckBox2 = New ESUS_LAUNCHER_A3.FlatCheckBox()
        Me.FlatCheckBox1 = New ESUS_LAUNCHER_A3.FlatCheckBox()
        Me.FlatLabel6 = New ESUS_LAUNCHER_A3.FlatLabel()
        Me.FlatTextBox1 = New ESUS_LAUNCHER_A3.FlatTextBox()
        Me.FlatTextBox3 = New ESUS_LAUNCHER_A3.FlatTextBox()
        Me.FlatTextBox4 = New ESUS_LAUNCHER_A3.FlatTextBox()
        Me.FlatTrackBar4 = New ESUS_LAUNCHER_A3.FlatTrackBar()
        Me.FlatTextBox2 = New ESUS_LAUNCHER_A3.FlatTextBox()
        Me.FlatTrackBar3 = New ESUS_LAUNCHER_A3.FlatTrackBar()
        Me.FlatTrackBar2 = New ESUS_LAUNCHER_A3.FlatTrackBar()
        Me.FlatTrackBar1 = New ESUS_LAUNCHER_A3.FlatTrackBar()
        Me.FlatButton1 = New ESUS_LAUNCHER_A3.FlatButton()
        Me.FlatComboBox2 = New ESUS_LAUNCHER_A3.FlatComboBox()
        Me.FlatTabControl2 = New ESUS_LAUNCHER_A3.FlatTabControl()
        Me.TabPage5 = New System.Windows.Forms.TabPage()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.Button8 = New System.Windows.Forms.Button()
        Me.Button7 = New System.Windows.Forms.Button()
        Me.ProgressBar2 = New System.Windows.Forms.ProgressBar()
        Me.ListBox2 = New System.Windows.Forms.ListBox()
        Me.FlatTextBox7 = New ESUS_LAUNCHER_A3.FlatTextBox()
        Me.Button5 = New System.Windows.Forms.Button()
        Me.Button4 = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.TabPage6 = New System.Windows.Forms.TabPage()
        Me.FlatCheckBox16 = New ESUS_LAUNCHER_A3.FlatCheckBox()
        Me.FlatLabel5 = New ESUS_LAUNCHER_A3.FlatLabel()
        Me.FlatLabel4 = New ESUS_LAUNCHER_A3.FlatLabel()
        Me.FlatLabel2 = New ESUS_LAUNCHER_A3.FlatLabel()
        Me.FlatTextBox8 = New ESUS_LAUNCHER_A3.FlatTextBox()
        Me.FlatButton4 = New ESUS_LAUNCHER_A3.FlatButton()
        Me.FlatLabel1 = New ESUS_LAUNCHER_A3.FlatLabel()
        Me.FlatButton3 = New ESUS_LAUNCHER_A3.FlatButton()
        Me.FlatTextBox5 = New ESUS_LAUNCHER_A3.FlatTextBox()
        Me.TabPage7 = New System.Windows.Forms.TabPage()
        Me.FlatLabel7 = New ESUS_LAUNCHER_A3.FlatLabel()
        Me.FlatButton5 = New ESUS_LAUNCHER_A3.FlatButton()
        Me.FlatButton6 = New ESUS_LAUNCHER_A3.FlatButton()
        Me.CheckedListBox3 = New System.Windows.Forms.CheckedListBox()
        Me.ContextMenuStrip2.SuspendLayout()
        Me.ContextMenuStrip1.SuspendLayout()
        Me.FlatTabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        Me.FlatGroupBox1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        Me.TabPage3.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage4.SuspendLayout()
        Me.FlatTabControl2.SuspendLayout()
        Me.TabPage5.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.TabPage6.SuspendLayout()
        Me.TabPage7.SuspendLayout()
        Me.SuspendLayout()
        '
        'ContextMenuStrip2
        '
        Me.ContextMenuStrip2.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CopiarParametrosToolStripMenuItem, Me.LanzarArma3exeToolStripMenuItem})
        Me.ContextMenuStrip2.Name = "ContextMenuStrip2"
        Me.ContextMenuStrip2.Size = New System.Drawing.Size(227, 48)
        '
        'CopiarParametrosToolStripMenuItem
        '
        Me.CopiarParametrosToolStripMenuItem.Name = "CopiarParametrosToolStripMenuItem"
        Me.CopiarParametrosToolStripMenuItem.Size = New System.Drawing.Size(226, 22)
        Me.CopiarParametrosToolStripMenuItem.Text = "Copiar parametros"
        '
        'LanzarArma3exeToolStripMenuItem
        '
        Me.LanzarArma3exeToolStripMenuItem.Name = "LanzarArma3exeToolStripMenuItem"
        Me.LanzarArma3exeToolStripMenuItem.Size = New System.Drawing.Size(226, 22)
        Me.LanzarArma3exeToolStripMenuItem.Text = "Lanzar Arma3.exe (no steam)"
        '
        'Timer1
        '
        '
        'ContextMenuStrip1
        '
        Me.ContextMenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.CopiarToolStripMenuItem})
        Me.ContextMenuStrip1.Name = "ContextMenuStrip1"
        Me.ContextMenuStrip1.Size = New System.Drawing.Size(191, 26)
        '
        'CopiarToolStripMenuItem
        '
        Me.CopiarToolStripMenuItem.Name = "CopiarToolStripMenuItem"
        Me.CopiarToolStripMenuItem.Size = New System.Drawing.Size(190, 22)
        Me.CopiarToolStripMenuItem.Text = "Copiar Mods Clicados"
        '
        'ToolTip1
        '
        Me.ToolTip1.IsBalloon = True
        Me.ToolTip1.ToolTipIcon = System.Windows.Forms.ToolTipIcon.Info
        Me.ToolTip1.ToolTipTitle = "Información"
        '
        'BottomToolStripPanel
        '
        Me.BottomToolStripPanel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.BottomToolStripPanel.Location = New System.Drawing.Point(0, 175)
        Me.BottomToolStripPanel.Name = "BottomToolStripPanel"
        Me.BottomToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.BottomToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.BottomToolStripPanel.Size = New System.Drawing.Size(150, 0)
        '
        'TopToolStripPanel
        '
        Me.TopToolStripPanel.Dock = System.Windows.Forms.DockStyle.Top
        Me.TopToolStripPanel.Location = New System.Drawing.Point(0, 0)
        Me.TopToolStripPanel.Name = "TopToolStripPanel"
        Me.TopToolStripPanel.Orientation = System.Windows.Forms.Orientation.Horizontal
        Me.TopToolStripPanel.RowMargin = New System.Windows.Forms.Padding(3, 0, 0, 0)
        Me.TopToolStripPanel.Size = New System.Drawing.Size(150, 25)
        '
        'RightToolStripPanel
        '
        Me.RightToolStripPanel.Dock = System.Windows.Forms.DockStyle.Right
        Me.RightToolStripPanel.Location = New System.Drawing.Point(150, 25)
        Me.RightToolStripPanel.Name = "RightToolStripPanel"
        Me.RightToolStripPanel.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.RightToolStripPanel.RowMargin = New System.Windows.Forms.Padding(0, 3, 0, 0)
        Me.RightToolStripPanel.Size = New System.Drawing.Size(0, 150)
        '
        'LeftToolStripPanel
        '
        Me.LeftToolStripPanel.Dock = System.Windows.Forms.DockStyle.Left
        Me.LeftToolStripPanel.Location = New System.Drawing.Point(0, 25)
        Me.LeftToolStripPanel.Name = "LeftToolStripPanel"
        Me.LeftToolStripPanel.Orientation = System.Windows.Forms.Orientation.Vertical
        Me.LeftToolStripPanel.RowMargin = New System.Windows.Forms.Padding(0, 3, 0, 0)
        Me.LeftToolStripPanel.Size = New System.Drawing.Size(0, 150)
        '
        'ContentPanel
        '
        Me.ContentPanel.AutoScroll = True
        Me.ContentPanel.Size = New System.Drawing.Size(579, 691)
        '
        'Label1
        '
        Me.Label1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Label1.AutoSize = True
        Me.Label1.BackColor = System.Drawing.Color.White
        Me.Label1.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.Label1.ForeColor = System.Drawing.Color.Black
        Me.Label1.Location = New System.Drawing.Point(279, 656)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(140, 21)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Datos Guardados!!"
        Me.Label1.Visible = False
        '
        'MaterialRaisedButton10
        '
        Me.MaterialRaisedButton10.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MaterialRaisedButton10.ContextMenuStrip = Me.ContextMenuStrip2
        Me.MaterialRaisedButton10.Depth = 0
        Me.MaterialRaisedButton10.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(128, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.MaterialRaisedButton10.Location = New System.Drawing.Point(426, 642)
        Me.MaterialRaisedButton10.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialRaisedButton10.Name = "MaterialRaisedButton10"
        Me.MaterialRaisedButton10.Primary = True
        Me.MaterialRaisedButton10.Size = New System.Drawing.Size(155, 52)
        Me.MaterialRaisedButton10.TabIndex = 3
        Me.MaterialRaisedButton10.Text = "Lanzar Arma 3"
        Me.MaterialRaisedButton10.UseVisualStyleBackColor = True
        '
        'MaterialRaisedButton11
        '
        Me.MaterialRaisedButton11.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.MaterialRaisedButton11.Depth = 0
        Me.MaterialRaisedButton11.Location = New System.Drawing.Point(151, 642)
        Me.MaterialRaisedButton11.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialRaisedButton11.Name = "MaterialRaisedButton11"
        Me.MaterialRaisedButton11.Primary = True
        Me.MaterialRaisedButton11.Size = New System.Drawing.Size(122, 52)
        Me.MaterialRaisedButton11.TabIndex = 4
        Me.MaterialRaisedButton11.Text = "Guardar"
        Me.MaterialRaisedButton11.UseVisualStyleBackColor = True
        '
        'CheckBox1
        '
        Me.CheckBox1.AutoSize = True
        Me.CheckBox1.BackColor = System.Drawing.Color.Transparent
        Me.CheckBox1.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.CheckBox1.Location = New System.Drawing.Point(279, 655)
        Me.CheckBox1.Name = "CheckBox1"
        Me.CheckBox1.Size = New System.Drawing.Size(147, 25)
        Me.CheckBox1.TabIndex = 41
        Me.CheckBox1.Text = "Conexión Directa"
        Me.CheckBox1.UseVisualStyleBackColor = False
        '
        'FlatLabel10
        '
        Me.FlatLabel10.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FlatLabel10.AutoSize = True
        Me.FlatLabel10.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel10.Font = New System.Drawing.Font("Segoe UI", 8.0!)
        Me.FlatLabel10.ForeColor = System.Drawing.Color.White
        Me.FlatLabel10.Location = New System.Drawing.Point(425, 36)
        Me.FlatLabel10.Name = "FlatLabel10"
        Me.FlatLabel10.Size = New System.Drawing.Size(72, 13)
        Me.FlatLabel10.TabIndex = 40
        Me.FlatLabel10.Text = "MODO ESUS"
        '
        'FlatComboBox1
        '
        Me.FlatComboBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.FlatComboBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.FlatComboBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatComboBox1.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.FlatComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.FlatComboBox1.Font = New System.Drawing.Font("Segoe UI", 8.0!)
        Me.FlatComboBox1.ForeColor = System.Drawing.Color.White
        Me.FlatComboBox1.FormattingEnabled = True
        Me.FlatComboBox1.HoverColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(163, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.FlatComboBox1.ItemHeight = 18
        Me.FlatComboBox1.Location = New System.Drawing.Point(4, 654)
        Me.FlatComboBox1.Name = "FlatComboBox1"
        Me.FlatComboBox1.Size = New System.Drawing.Size(139, 24)
        Me.FlatComboBox1.TabIndex = 1
        '
        'FlatComboBox3
        '
        Me.FlatComboBox3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.FlatComboBox3.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.FlatComboBox3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatComboBox3.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.FlatComboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.FlatComboBox3.Font = New System.Drawing.Font("Segoe UI", 8.0!)
        Me.FlatComboBox3.ForeColor = System.Drawing.Color.White
        Me.FlatComboBox3.FormattingEnabled = True
        Me.FlatComboBox3.HoverColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(163, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.FlatComboBox3.ItemHeight = 18
        Me.FlatComboBox3.Items.AddRange(New Object() {"LUNES", "MARTES", "MIÉRCOLES", "JUEVES", "VIERNES", "SÁBADO", "DOMINGO", "PERMANENTE", "OTROS", "PRUEBAS", "ACADEMIA 1", "ACADEMIA 2", "LOADOUT"})
        Me.FlatComboBox3.Location = New System.Drawing.Point(4, 654)
        Me.FlatComboBox3.Name = "FlatComboBox3"
        Me.FlatComboBox3.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.FlatComboBox3.Size = New System.Drawing.Size(139, 24)
        Me.FlatComboBox3.TabIndex = 6
        Me.FlatComboBox3.Visible = False
        '
        'FlatToggle1
        '
        Me.FlatToggle1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FlatToggle1.BackColor = System.Drawing.Color.Transparent
        Me.FlatToggle1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.FlatToggle1.Checked = False
        Me.FlatToggle1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatToggle1.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.FlatToggle1.Location = New System.Drawing.Point(501, 26)
        Me.FlatToggle1.Name = "FlatToggle1"
        Me.FlatToggle1.Options = ESUS_LAUNCHER_A3.FlatToggle._Options.Style3
        Me.FlatToggle1.Size = New System.Drawing.Size(76, 33)
        Me.FlatToggle1.TabIndex = 5
        Me.FlatToggle1.Text = "FlatToggle1"
        '
        'FlatTabControl1
        '
        Me.FlatTabControl1.ActiveColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(163, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.FlatTabControl1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FlatTabControl1.Appearance = System.Windows.Forms.TabAppearance.FlatButtons
        Me.FlatTabControl1.BaseColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.FlatTabControl1.Controls.Add(Me.TabPage1)
        Me.FlatTabControl1.Controls.Add(Me.TabPage2)
        Me.FlatTabControl1.Controls.Add(Me.TabPage3)
        Me.FlatTabControl1.Controls.Add(Me.TabPage4)
        Me.FlatTabControl1.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.FlatTabControl1.ItemSize = New System.Drawing.Size(120, 40)
        Me.FlatTabControl1.Location = New System.Drawing.Point(0, 61)
        Me.FlatTabControl1.Name = "FlatTabControl1"
        Me.FlatTabControl1.SelectedIndex = 0
        Me.FlatTabControl1.Size = New System.Drawing.Size(586, 581)
        Me.FlatTabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed
        Me.FlatTabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.TabPage1.Controls.Add(Me.FlatGroupBox1)
        Me.TabPage1.Controls.Add(Me.PictureBox1)
        Me.TabPage1.Controls.Add(Me.FlatLabel9)
        Me.TabPage1.Controls.Add(Me.FlatLabel8)
        Me.TabPage1.Controls.Add(Me.MaterialRaisedButton1)
        Me.TabPage1.Controls.Add(Me.CheckedListBox2)
        Me.TabPage1.Controls.Add(Me.RichTextBox1)
        Me.TabPage1.Controls.Add(Me.RichTextBox2)
        Me.TabPage1.Controls.Add(Me.MaterialRaisedButton5)
        Me.TabPage1.Controls.Add(Me.MaterialRaisedButton4)
        Me.TabPage1.Controls.Add(Me.MaterialRaisedButton3)
        Me.TabPage1.Controls.Add(Me.MaterialRaisedButton2)
        Me.TabPage1.Controls.Add(Me.CheckedListBox1)
        Me.TabPage1.ForeColor = System.Drawing.Color.Black
        Me.TabPage1.Location = New System.Drawing.Point(4, 44)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(578, 533)
        Me.TabPage1.TabIndex = 4
        Me.TabPage1.Text = "MODS"
        '
        'FlatGroupBox1
        '
        Me.FlatGroupBox1.BackColor = System.Drawing.Color.Transparent
        Me.FlatGroupBox1.BaseColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.FlatGroupBox1.Controls.Add(Me.FlatLabel17)
        Me.FlatGroupBox1.Controls.Add(Me.FlatTextBox9)
        Me.FlatGroupBox1.Controls.Add(Me.FlatLabel16)
        Me.FlatGroupBox1.Controls.Add(Me.FlatTextBox6)
        Me.FlatGroupBox1.Controls.Add(Me.FlatLabel15)
        Me.FlatGroupBox1.Font = New System.Drawing.Font("Segoe UI", 12.0!, System.Drawing.FontStyle.Bold)
        Me.FlatGroupBox1.ForeColor = System.Drawing.Color.Black
        Me.FlatGroupBox1.Location = New System.Drawing.Point(437, 398)
        Me.FlatGroupBox1.Name = "FlatGroupBox1"
        Me.FlatGroupBox1.ShowText = True
        Me.FlatGroupBox1.Size = New System.Drawing.Size(141, 129)
        Me.FlatGroupBox1.TabIndex = 46
        '
        'FlatLabel17
        '
        Me.FlatLabel17.AutoSize = True
        Me.FlatLabel17.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel17.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.FlatLabel17.ForeColor = System.Drawing.Color.White
        Me.FlatLabel17.Location = New System.Drawing.Point(7, 5)
        Me.FlatLabel17.Name = "FlatLabel17"
        Me.FlatLabel17.Size = New System.Drawing.Size(66, 19)
        Me.FlatLabel17.TabIndex = 46
        Me.FlatLabel17.Text = "ADDONS"
        '
        'FlatTextBox9
        '
        Me.FlatTextBox9.BackColor = System.Drawing.Color.Transparent
        Me.FlatTextBox9.Enabled = False
        Me.FlatTextBox9.Location = New System.Drawing.Point(99, 37)
        Me.FlatTextBox9.MaxLength = 32767
        Me.FlatTextBox9.Multiline = False
        Me.FlatTextBox9.Name = "FlatTextBox9"
        Me.FlatTextBox9.ReadOnly = False
        Me.FlatTextBox9.Size = New System.Drawing.Size(32, 29)
        Me.FlatTextBox9.TabIndex = 42
        Me.FlatTextBox9.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.FlatTextBox9.TextColor = System.Drawing.Color.White
        Me.FlatTextBox9.UseSystemPasswordChar = False
        '
        'FlatLabel16
        '
        Me.FlatLabel16.AutoSize = True
        Me.FlatLabel16.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel16.Font = New System.Drawing.Font("Segoe UI", 8.0!)
        Me.FlatLabel16.ForeColor = System.Drawing.Color.White
        Me.FlatLabel16.Location = New System.Drawing.Point(9, 80)
        Me.FlatLabel16.Name = "FlatLabel16"
        Me.FlatLabel16.Size = New System.Drawing.Size(88, 13)
        Me.FlatLabel16.TabIndex = 44
        Me.FlatLabel16.Text = "SELECCIONADO"
        '
        'FlatTextBox6
        '
        Me.FlatTextBox6.BackColor = System.Drawing.Color.Transparent
        Me.FlatTextBox6.Enabled = False
        Me.FlatTextBox6.Location = New System.Drawing.Point(99, 72)
        Me.FlatTextBox6.MaxLength = 32767
        Me.FlatTextBox6.Multiline = False
        Me.FlatTextBox6.Name = "FlatTextBox6"
        Me.FlatTextBox6.ReadOnly = False
        Me.FlatTextBox6.Size = New System.Drawing.Size(32, 29)
        Me.FlatTextBox6.TabIndex = 45
        Me.FlatTextBox6.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        Me.FlatTextBox6.TextColor = System.Drawing.Color.White
        Me.FlatTextBox6.UseSystemPasswordChar = False
        '
        'FlatLabel15
        '
        Me.FlatLabel15.AutoSize = True
        Me.FlatLabel15.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel15.Font = New System.Drawing.Font("Segoe UI", 8.0!)
        Me.FlatLabel15.ForeColor = System.Drawing.Color.White
        Me.FlatLabel15.Location = New System.Drawing.Point(9, 45)
        Me.FlatLabel15.Name = "FlatLabel15"
        Me.FlatLabel15.Size = New System.Drawing.Size(68, 13)
        Me.FlatLabel15.TabIndex = 43
        Me.FlatLabel15.Text = "DISPONIBLE"
        '
        'PictureBox1
        '
        Me.PictureBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.PictureBox1.BackgroundImage = CType(resources.GetObject("PictureBox1.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PictureBox1.Location = New System.Drawing.Point(457, 285)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(106, 105)
        Me.PictureBox1.TabIndex = 40
        Me.PictureBox1.TabStop = False
        '
        'FlatLabel9
        '
        Me.FlatLabel9.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FlatLabel9.AutoSize = True
        Me.FlatLabel9.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel9.Font = New System.Drawing.Font("Segoe UI", 8.0!)
        Me.FlatLabel9.ForeColor = System.Drawing.Color.White
        Me.FlatLabel9.Location = New System.Drawing.Point(219, 6)
        Me.FlatLabel9.Name = "FlatLabel9"
        Me.FlatLabel9.Size = New System.Drawing.Size(117, 13)
        Me.FlatLabel9.TabIndex = 39
        Me.FlatLabel9.Text = "ADDONS CLIENT-SIDE"
        Me.FlatLabel9.Visible = False
        '
        'FlatLabel8
        '
        Me.FlatLabel8.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FlatLabel8.AutoSize = True
        Me.FlatLabel8.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel8.Font = New System.Drawing.Font("Segoe UI", 8.0!)
        Me.FlatLabel8.ForeColor = System.Drawing.Color.White
        Me.FlatLabel8.Location = New System.Drawing.Point(6, 6)
        Me.FlatLabel8.Name = "FlatLabel8"
        Me.FlatLabel8.Size = New System.Drawing.Size(69, 13)
        Me.FlatLabel8.TabIndex = 38
        Me.FlatLabel8.Text = "PERFIL ESUS"
        Me.FlatLabel8.Visible = False
        '
        'MaterialRaisedButton1
        '
        Me.MaterialRaisedButton1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MaterialRaisedButton1.Depth = 0
        Me.MaterialRaisedButton1.Location = New System.Drawing.Point(446, 12)
        Me.MaterialRaisedButton1.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialRaisedButton1.Name = "MaterialRaisedButton1"
        Me.MaterialRaisedButton1.Primary = True
        Me.MaterialRaisedButton1.Size = New System.Drawing.Size(126, 44)
        Me.MaterialRaisedButton1.TabIndex = 17
        Me.MaterialRaisedButton1.Text = "Borrar Todo"
        Me.MaterialRaisedButton1.UseVisualStyleBackColor = True
        '
        'CheckedListBox2
        '
        Me.CheckedListBox2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CheckedListBox2.CheckOnClick = True
        Me.CheckedListBox2.FormattingEnabled = True
        Me.CheckedListBox2.Location = New System.Drawing.Point(216, 23)
        Me.CheckedListBox2.Name = "CheckedListBox2"
        Me.CheckedListBox2.Size = New System.Drawing.Size(221, 504)
        Me.CheckedListBox2.TabIndex = 16
        Me.CheckedListBox2.Visible = False
        '
        'RichTextBox1
        '
        Me.RichTextBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RichTextBox1.Location = New System.Drawing.Point(446, 285)
        Me.RichTextBox1.Name = "RichTextBox1"
        Me.RichTextBox1.Size = New System.Drawing.Size(121, 104)
        Me.RichTextBox1.TabIndex = 15
        Me.RichTextBox1.Text = ""
        Me.RichTextBox1.Visible = False
        '
        'RichTextBox2
        '
        Me.RichTextBox2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.RichTextBox2.Cursor = System.Windows.Forms.Cursors.Default
        Me.RichTextBox2.DetectUrls = False
        Me.RichTextBox2.Location = New System.Drawing.Point(6, 21)
        Me.RichTextBox2.Name = "RichTextBox2"
        Me.RichTextBox2.ReadOnly = True
        Me.RichTextBox2.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical
        Me.RichTextBox2.Size = New System.Drawing.Size(208, 506)
        Me.RichTextBox2.TabIndex = 12
        Me.RichTextBox2.TabStop = False
        Me.RichTextBox2.Text = ""
        Me.RichTextBox2.Visible = False
        Me.RichTextBox2.WordWrap = False
        '
        'MaterialRaisedButton5
        '
        Me.MaterialRaisedButton5.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MaterialRaisedButton5.Depth = 0
        Me.MaterialRaisedButton5.Location = New System.Drawing.Point(446, 225)
        Me.MaterialRaisedButton5.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialRaisedButton5.Name = "MaterialRaisedButton5"
        Me.MaterialRaisedButton5.Primary = True
        Me.MaterialRaisedButton5.Size = New System.Drawing.Size(126, 39)
        Me.MaterialRaisedButton5.TabIndex = 9
        Me.MaterialRaisedButton5.Text = "Importar Lista Externa"
        Me.MaterialRaisedButton5.UseVisualStyleBackColor = True
        '
        'MaterialRaisedButton4
        '
        Me.MaterialRaisedButton4.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MaterialRaisedButton4.Depth = 0
        Me.MaterialRaisedButton4.Location = New System.Drawing.Point(446, 175)
        Me.MaterialRaisedButton4.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialRaisedButton4.Name = "MaterialRaisedButton4"
        Me.MaterialRaisedButton4.Primary = True
        Me.MaterialRaisedButton4.Size = New System.Drawing.Size(126, 39)
        Me.MaterialRaisedButton4.TabIndex = 8
        Me.MaterialRaisedButton4.Text = "ACTUALIZAR LISTA"
        Me.MaterialRaisedButton4.UseVisualStyleBackColor = True
        '
        'MaterialRaisedButton3
        '
        Me.MaterialRaisedButton3.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MaterialRaisedButton3.Depth = 0
        Me.MaterialRaisedButton3.Location = New System.Drawing.Point(446, 126)
        Me.MaterialRaisedButton3.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialRaisedButton3.Name = "MaterialRaisedButton3"
        Me.MaterialRaisedButton3.Primary = True
        Me.MaterialRaisedButton3.Size = New System.Drawing.Size(126, 39)
        Me.MaterialRaisedButton3.TabIndex = 7
        Me.MaterialRaisedButton3.Text = "Invertir Selección"
        Me.MaterialRaisedButton3.UseVisualStyleBackColor = True
        '
        'MaterialRaisedButton2
        '
        Me.MaterialRaisedButton2.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.MaterialRaisedButton2.Depth = 0
        Me.MaterialRaisedButton2.Location = New System.Drawing.Point(446, 74)
        Me.MaterialRaisedButton2.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialRaisedButton2.Name = "MaterialRaisedButton2"
        Me.MaterialRaisedButton2.Primary = True
        Me.MaterialRaisedButton2.Size = New System.Drawing.Size(126, 39)
        Me.MaterialRaisedButton2.TabIndex = 6
        Me.MaterialRaisedButton2.Text = "Seleccionar Todo"
        Me.MaterialRaisedButton2.UseVisualStyleBackColor = True
        '
        'CheckedListBox1
        '
        Me.CheckedListBox1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.CheckedListBox1.BorderStyle = System.Windows.Forms.BorderStyle.None
        Me.CheckedListBox1.CheckOnClick = True
        Me.CheckedListBox1.ContextMenuStrip = Me.ContextMenuStrip1
        Me.CheckedListBox1.FormattingEnabled = True
        Me.CheckedListBox1.Location = New System.Drawing.Point(1, 9)
        Me.CheckedListBox1.Name = "CheckedListBox1"
        Me.CheckedListBox1.Size = New System.Drawing.Size(436, 520)
        Me.CheckedListBox1.Sorted = True
        Me.CheckedListBox1.TabIndex = 0
        Me.CheckedListBox1.ThreeDCheckBoxes = True
        '
        'TabPage2
        '
        Me.TabPage2.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.TabPage2.Controls.Add(Me.ListBox3)
        Me.TabPage2.Controls.Add(Me.MaterialRaisedButton9)
        Me.TabPage2.Controls.Add(Me.MaterialRaisedButton8)
        Me.TabPage2.Controls.Add(Me.MaterialRaisedButton7)
        Me.TabPage2.Controls.Add(Me.MaterialRaisedButton6)
        Me.TabPage2.Controls.Add(Me.ListBox1)
        Me.TabPage2.Location = New System.Drawing.Point(4, 44)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(578, 533)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "PRIORIDAD"
        '
        'ListBox3
        '
        Me.ListBox3.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ListBox3.FormattingEnabled = True
        Me.ListBox3.ItemHeight = 17
        Me.ListBox3.Location = New System.Drawing.Point(3, 1)
        Me.ListBox3.Name = "ListBox3"
        Me.ListBox3.Size = New System.Drawing.Size(429, 531)
        Me.ListBox3.TabIndex = 9
        Me.ListBox3.Visible = False
        '
        'MaterialRaisedButton9
        '
        Me.MaterialRaisedButton9.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.MaterialRaisedButton9.Depth = 0
        Me.MaterialRaisedButton9.Location = New System.Drawing.Point(441, 58)
        Me.MaterialRaisedButton9.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialRaisedButton9.Name = "MaterialRaisedButton9"
        Me.MaterialRaisedButton9.Primary = True
        Me.MaterialRaisedButton9.Size = New System.Drawing.Size(130, 39)
        Me.MaterialRaisedButton9.TabIndex = 8
        Me.MaterialRaisedButton9.Text = "ARRIBA"
        Me.MaterialRaisedButton9.UseVisualStyleBackColor = True
        '
        'MaterialRaisedButton8
        '
        Me.MaterialRaisedButton8.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.MaterialRaisedButton8.Depth = 0
        Me.MaterialRaisedButton8.Location = New System.Drawing.Point(441, 103)
        Me.MaterialRaisedButton8.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialRaisedButton8.Name = "MaterialRaisedButton8"
        Me.MaterialRaisedButton8.Primary = True
        Me.MaterialRaisedButton8.Size = New System.Drawing.Size(130, 39)
        Me.MaterialRaisedButton8.TabIndex = 7
        Me.MaterialRaisedButton8.Text = "ABAJO"
        Me.MaterialRaisedButton8.UseVisualStyleBackColor = True
        '
        'MaterialRaisedButton7
        '
        Me.MaterialRaisedButton7.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.MaterialRaisedButton7.Depth = 0
        Me.MaterialRaisedButton7.Location = New System.Drawing.Point(441, 148)
        Me.MaterialRaisedButton7.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialRaisedButton7.Name = "MaterialRaisedButton7"
        Me.MaterialRaisedButton7.Primary = True
        Me.MaterialRaisedButton7.Size = New System.Drawing.Size(130, 39)
        Me.MaterialRaisedButton7.TabIndex = 6
        Me.MaterialRaisedButton7.Text = "FINAL"
        Me.MaterialRaisedButton7.UseVisualStyleBackColor = True
        '
        'MaterialRaisedButton6
        '
        Me.MaterialRaisedButton6.Anchor = System.Windows.Forms.AnchorStyles.Right
        Me.MaterialRaisedButton6.Depth = 0
        Me.MaterialRaisedButton6.Location = New System.Drawing.Point(441, 13)
        Me.MaterialRaisedButton6.MouseState = MaterialSkin.MouseState.HOVER
        Me.MaterialRaisedButton6.Name = "MaterialRaisedButton6"
        Me.MaterialRaisedButton6.Primary = True
        Me.MaterialRaisedButton6.Size = New System.Drawing.Size(130, 39)
        Me.MaterialRaisedButton6.TabIndex = 5
        Me.MaterialRaisedButton6.Text = "PRINCIPIO"
        Me.MaterialRaisedButton6.UseVisualStyleBackColor = True
        '
        'ListBox1
        '
        Me.ListBox1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ListBox1.FormattingEnabled = True
        Me.ListBox1.ItemHeight = 17
        Me.ListBox1.Location = New System.Drawing.Point(3, 2)
        Me.ListBox1.Name = "ListBox1"
        Me.ListBox1.Size = New System.Drawing.Size(431, 531)
        Me.ListBox1.TabIndex = 0
        '
        'TabPage3
        '
        Me.TabPage3.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.TabPage3.Controls.Add(Me.WebBrowser1)
        Me.TabPage3.Controls.Add(Me.Label12)
        Me.TabPage3.Controls.Add(Me.Label13)
        Me.TabPage3.Controls.Add(Me.Label8)
        Me.TabPage3.Controls.Add(Me.Label9)
        Me.TabPage3.Controls.Add(Me.Label6)
        Me.TabPage3.Controls.Add(Me.Label7)
        Me.TabPage3.Controls.Add(Me.Label10)
        Me.TabPage3.Controls.Add(Me.Label11)
        Me.TabPage3.Controls.Add(Me.Button9)
        Me.TabPage3.Controls.Add(Me.Label5)
        Me.TabPage3.Controls.Add(Me.Label4)
        Me.TabPage3.Controls.Add(Me.Label3)
        Me.TabPage3.Controls.Add(Me.Label2)
        Me.TabPage3.Controls.Add(Me.FlatLabel11)
        Me.TabPage3.Controls.Add(Me.Button6)
        Me.TabPage3.Controls.Add(Me.Button1)
        Me.TabPage3.Controls.Add(Me.FlatButton2)
        Me.TabPage3.Controls.Add(Me.DataGridView1)
        Me.TabPage3.Controls.Add(Me.PictureBox11)
        Me.TabPage3.Controls.Add(Me.PictureBox10)
        Me.TabPage3.Controls.Add(Me.PictureBox9)
        Me.TabPage3.Controls.Add(Me.PictureBox8)
        Me.TabPage3.Controls.Add(Me.PictureBox7)
        Me.TabPage3.Controls.Add(Me.PictureBox6)
        Me.TabPage3.Controls.Add(Me.PictureBox5)
        Me.TabPage3.Controls.Add(Me.PictureBox4)
        Me.TabPage3.Controls.Add(Me.PictureBox3)
        Me.TabPage3.Controls.Add(Me.PictureBox2)
        Me.TabPage3.Location = New System.Drawing.Point(4, 44)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Size = New System.Drawing.Size(578, 533)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "SERVIDOR"
        '
        'WebBrowser1
        '
        Me.WebBrowser1.AllowNavigation = False
        Me.WebBrowser1.AllowWebBrowserDrop = False
        Me.WebBrowser1.IsWebBrowserContextMenuEnabled = False
        Me.WebBrowser1.Location = New System.Drawing.Point(1, 287)
        Me.WebBrowser1.MinimumSize = New System.Drawing.Size(20, 20)
        Me.WebBrowser1.Name = "WebBrowser1"
        Me.WebBrowser1.ScriptErrorsSuppressed = True
        Me.WebBrowser1.Size = New System.Drawing.Size(280, 244)
        Me.WebBrowser1.TabIndex = 51
        Me.WebBrowser1.Url = New System.Uri("http://teamspeakdzskrachenkouser.comule.com/teamspeak.html", System.UriKind.Absolute)
        Me.WebBrowser1.WebBrowserShortcutsEnabled = False
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Font = New System.Drawing.Font("Segoe UI", 7.0!)
        Me.Label12.ForeColor = System.Drawing.Color.White
        Me.Label12.Location = New System.Drawing.Point(306, 346)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(11, 12)
        Me.Label12.TabIndex = 49
        Me.Label12.Text = "..."
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Font = New System.Drawing.Font("Segoe UI", 7.0!)
        Me.Label13.ForeColor = System.Drawing.Color.White
        Me.Label13.Location = New System.Drawing.Point(306, 332)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(11, 12)
        Me.Label13.TabIndex = 48
        Me.Label13.Text = "..."
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Font = New System.Drawing.Font("Segoe UI", 7.0!)
        Me.Label8.ForeColor = System.Drawing.Color.White
        Me.Label8.Location = New System.Drawing.Point(306, 402)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(11, 12)
        Me.Label8.TabIndex = 47
        Me.Label8.Text = "..."
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Font = New System.Drawing.Font("Segoe UI", 7.0!)
        Me.Label9.ForeColor = System.Drawing.Color.White
        Me.Label9.Location = New System.Drawing.Point(306, 386)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(11, 12)
        Me.Label9.TabIndex = 46
        Me.Label9.Text = "..."
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Segoe UI", 7.0!)
        Me.Label6.ForeColor = System.Drawing.Color.White
        Me.Label6.Location = New System.Drawing.Point(306, 456)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(11, 12)
        Me.Label6.TabIndex = 45
        Me.Label6.Text = "..."
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Font = New System.Drawing.Font("Segoe UI", 7.0!)
        Me.Label7.ForeColor = System.Drawing.Color.White
        Me.Label7.Location = New System.Drawing.Point(306, 439)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(11, 12)
        Me.Label7.TabIndex = 44
        Me.Label7.Text = "..."
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Font = New System.Drawing.Font("Segoe UI", 7.0!)
        Me.Label10.ForeColor = System.Drawing.Color.White
        Me.Label10.Location = New System.Drawing.Point(307, 488)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(11, 12)
        Me.Label10.TabIndex = 43
        Me.Label10.Text = "..."
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Font = New System.Drawing.Font("Segoe UI", 7.0!)
        Me.Label11.ForeColor = System.Drawing.Color.White
        Me.Label11.Location = New System.Drawing.Point(307, 509)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(11, 12)
        Me.Label11.TabIndex = 42
        Me.Label11.Text = "..."
        '
        'Button9
        '
        Me.Button9.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Button9.Location = New System.Drawing.Point(482, 225)
        Me.Button9.Name = "Button9"
        Me.Button9.Size = New System.Drawing.Size(86, 30)
        Me.Button9.TabIndex = 37
        Me.Button9.Text = "Actualizar"
        Me.Button9.UseVisualStyleBackColor = True
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.ForeColor = System.Drawing.Color.White
        Me.Label5.Location = New System.Drawing.Point(302, 469)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(28, 19)
        Me.Label5.TabIndex = 32
        Me.Label5.Text = "PIT"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.ForeColor = System.Drawing.Color.White
        Me.Label4.Location = New System.Drawing.Point(300, 418)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(63, 19)
        Me.Label4.TabIndex = 31
        Me.Label4.Text = "LUPAGO"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.Color.White
        Me.Label3.Location = New System.Drawing.Point(298, 364)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(58, 19)
        Me.Label3.TabIndex = 30
        Me.Label3.Text = "DIABLO"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.Color.White
        Me.Label2.Location = New System.Drawing.Point(298, 309)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(69, 19)
        Me.Label2.TabIndex = 29
        Me.Label2.Text = "SANCHEZ"
        '
        'FlatLabel11
        '
        Me.FlatLabel11.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.FlatLabel11.AutoSize = True
        Me.FlatLabel11.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel11.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.FlatLabel11.ForeColor = System.Drawing.Color.White
        Me.FlatLabel11.Location = New System.Drawing.Point(0, 10)
        Me.FlatLabel11.Name = "FlatLabel11"
        Me.FlatLabel11.Size = New System.Drawing.Size(217, 21)
        Me.FlatLabel11.TabIndex = 23
        Me.FlatLabel11.Text = "ESTADO DE LOS SERVIDORES"
        '
        'Button6
        '
        Me.Button6.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Button6.Location = New System.Drawing.Point(200, 224)
        Me.Button6.Name = "Button6"
        Me.Button6.Size = New System.Drawing.Size(81, 31)
        Me.Button6.TabIndex = 20
        Me.Button6.Text = "Conectar"
        Me.Button6.UseVisualStyleBackColor = True
        '
        'Button1
        '
        Me.Button1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.Button1.Location = New System.Drawing.Point(200, 255)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(81, 31)
        Me.Button1.TabIndex = 19
        Me.Button1.Text = "Actualizar"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'FlatButton2
        '
        Me.FlatButton2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FlatButton2.BackColor = System.Drawing.Color.Transparent
        Me.FlatButton2.BaseColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(163, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.FlatButton2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatButton2.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.FlatButton2.Location = New System.Drawing.Point(487, 6)
        Me.FlatButton2.Name = "FlatButton2"
        Me.FlatButton2.Rounded = False
        Me.FlatButton2.Size = New System.Drawing.Size(86, 32)
        Me.FlatButton2.TabIndex = 17
        Me.FlatButton2.Text = "Actualizar"
        Me.FlatButton2.TextColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AllowUserToOrderColumns = True
        Me.DataGridView1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.DataGridView1.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Raised
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Column1, Me.Column2, Me.Column3, Me.Column4, Me.Column5, Me.Column6, Me.Column7, Me.Column9, Me.Column8})
        Me.DataGridView1.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.DataGridView1.EnableHeadersVisualStyles = False
        Me.DataGridView1.Location = New System.Drawing.Point(3, 44)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.DataGridView1.Size = New System.Drawing.Size(572, 177)
        Me.DataGridView1.TabIndex = 0
        '
        'Column1
        '
        Me.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        DataGridViewCellStyle1.BackColor = System.Drawing.Color.White
        DataGridViewCellStyle1.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Column1.DefaultCellStyle = DataGridViewCellStyle1
        Me.Column1.HeaderText = "Nombre"
        Me.Column1.Name = "Column1"
        Me.Column1.ReadOnly = True
        Me.Column1.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Column1.Width = 84
        '
        'Column2
        '
        Me.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        DataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.Color.Black
        Me.Column2.DefaultCellStyle = DataGridViewCellStyle2
        Me.Column2.HeaderText = "Puerto"
        Me.Column2.Name = "Column2"
        Me.Column2.ReadOnly = True
        Me.Column2.Width = 75
        '
        'Column3
        '
        Me.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        DataGridViewCellStyle3.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.Color.Black
        Me.Column3.DefaultCellStyle = DataGridViewCellStyle3
        Me.Column3.HeaderText = "Misión"
        Me.Column3.Name = "Column3"
        Me.Column3.ReadOnly = True
        Me.Column3.Width = 75
        '
        'Column4
        '
        Me.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        DataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.Color.Black
        Me.Column4.DefaultCellStyle = DataGridViewCellStyle4
        Me.Column4.HeaderText = "Mapa"
        Me.Column4.Name = "Column4"
        Me.Column4.ReadOnly = True
        Me.Column4.Width = 69
        '
        'Column5
        '
        Me.Column5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        DataGridViewCellStyle5.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.Color.Black
        Me.Column5.DefaultCellStyle = DataGridViewCellStyle5
        Me.Column5.HeaderText = "Jugadores"
        Me.Column5.Name = "Column5"
        Me.Column5.ReadOnly = True
        Me.Column5.Width = 96
        '
        'Column6
        '
        Me.Column6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        DataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black
        Me.Column6.DefaultCellStyle = DataGridViewCellStyle6
        Me.Column6.HeaderText = "Slots"
        Me.Column6.Name = "Column6"
        Me.Column6.ReadOnly = True
        Me.Column6.Width = 63
        '
        'Column7
        '
        Me.Column7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        DataGridViewCellStyle7.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.Color.Black
        Me.Column7.DefaultCellStyle = DataGridViewCellStyle7
        Me.Column7.HeaderText = "Firmas"
        Me.Column7.Name = "Column7"
        Me.Column7.ReadOnly = True
        Me.Column7.Width = 74
        '
        'Column9
        '
        Me.Column9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        DataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.Color.Black
        Me.Column9.DefaultCellStyle = DataGridViewCellStyle8
        Me.Column9.HeaderText = "Contraseña"
        Me.Column9.Name = "Column9"
        Me.Column9.ReadOnly = True
        Me.Column9.Width = 104
        '
        'Column8
        '
        Me.Column8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.DisplayedCells
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle9.ForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle9.SelectionBackColor = System.Drawing.Color.White
        DataGridViewCellStyle9.SelectionForeColor = System.Drawing.Color.Black
        Me.Column8.DefaultCellStyle = DataGridViewCellStyle9
        Me.Column8.HeaderText = "Quien Juega"
        Me.Column8.Name = "Column8"
        Me.Column8.ReadOnly = True
        Me.Column8.Text = "VER"
        Me.Column8.Width = 91
        '
        'PictureBox11
        '
        Me.PictureBox11.Image = CType(resources.GetObject("PictureBox11.Image"), System.Drawing.Image)
        Me.PictureBox11.Location = New System.Drawing.Point(386, 470)
        Me.PictureBox11.Name = "PictureBox11"
        Me.PictureBox11.Size = New System.Drawing.Size(72, 20)
        Me.PictureBox11.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox11.TabIndex = 36
        Me.PictureBox11.TabStop = False
        '
        'PictureBox10
        '
        Me.PictureBox10.Image = CType(resources.GetObject("PictureBox10.Image"), System.Drawing.Image)
        Me.PictureBox10.Location = New System.Drawing.Point(386, 418)
        Me.PictureBox10.Name = "PictureBox10"
        Me.PictureBox10.Size = New System.Drawing.Size(72, 20)
        Me.PictureBox10.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox10.TabIndex = 35
        Me.PictureBox10.TabStop = False
        '
        'PictureBox9
        '
        Me.PictureBox9.Image = CType(resources.GetObject("PictureBox9.Image"), System.Drawing.Image)
        Me.PictureBox9.Location = New System.Drawing.Point(386, 364)
        Me.PictureBox9.Name = "PictureBox9"
        Me.PictureBox9.Size = New System.Drawing.Size(72, 20)
        Me.PictureBox9.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox9.TabIndex = 34
        Me.PictureBox9.TabStop = False
        '
        'PictureBox8
        '
        Me.PictureBox8.Image = CType(resources.GetObject("PictureBox8.Image"), System.Drawing.Image)
        Me.PictureBox8.Location = New System.Drawing.Point(386, 309)
        Me.PictureBox8.Name = "PictureBox8"
        Me.PictureBox8.Size = New System.Drawing.Size(72, 20)
        Me.PictureBox8.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox8.TabIndex = 33
        Me.PictureBox8.TabStop = False
        '
        'PictureBox7
        '
        Me.PictureBox7.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PictureBox7.Image = CType(resources.GetObject("PictureBox7.Image"), System.Drawing.Image)
        Me.PictureBox7.Location = New System.Drawing.Point(480, 404)
        Me.PictureBox7.Name = "PictureBox7"
        Me.PictureBox7.Size = New System.Drawing.Size(89, 47)
        Me.PictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox7.TabIndex = 28
        Me.PictureBox7.TabStop = False
        '
        'PictureBox6
        '
        Me.PictureBox6.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PictureBox6.Image = CType(resources.GetObject("PictureBox6.Image"), System.Drawing.Image)
        Me.PictureBox6.Location = New System.Drawing.Point(480, 457)
        Me.PictureBox6.Name = "PictureBox6"
        Me.PictureBox6.Size = New System.Drawing.Size(89, 47)
        Me.PictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox6.TabIndex = 27
        Me.PictureBox6.TabStop = False
        '
        'PictureBox5
        '
        Me.PictureBox5.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PictureBox5.Image = CType(resources.GetObject("PictureBox5.Image"), System.Drawing.Image)
        Me.PictureBox5.Location = New System.Drawing.Point(479, 349)
        Me.PictureBox5.Name = "PictureBox5"
        Me.PictureBox5.Size = New System.Drawing.Size(89, 47)
        Me.PictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox5.TabIndex = 26
        Me.PictureBox5.TabStop = False
        '
        'PictureBox4
        '
        Me.PictureBox4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.PictureBox4.Image = CType(resources.GetObject("PictureBox4.Image"), System.Drawing.Image)
        Me.PictureBox4.Location = New System.Drawing.Point(479, 300)
        Me.PictureBox4.Name = "PictureBox4"
        Me.PictureBox4.Size = New System.Drawing.Size(89, 47)
        Me.PictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox4.TabIndex = 25
        Me.PictureBox4.TabStop = False
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = CType(resources.GetObject("PictureBox3.Image"), System.Drawing.Image)
        Me.PictureBox3.Location = New System.Drawing.Point(356, 226)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(126, 72)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox3.TabIndex = 24
        Me.PictureBox3.TabStop = False
        '
        'PictureBox2
        '
        Me.PictureBox2.BackgroundImage = CType(resources.GetObject("PictureBox2.BackgroundImage"), System.Drawing.Image)
        Me.PictureBox2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.PictureBox2.Location = New System.Drawing.Point(-4, 223)
        Me.PictureBox2.Name = "PictureBox2"
        Me.PictureBox2.Size = New System.Drawing.Size(204, 67)
        Me.PictureBox2.TabIndex = 22
        Me.PictureBox2.TabStop = False
        '
        'TabPage4
        '
        Me.TabPage4.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.TabPage4.Controls.Add(Me.FlatButton7)
        Me.TabPage4.Controls.Add(Me.TextBox1)
        Me.TabPage4.Controls.Add(Me.FlatLabel12)
        Me.TabPage4.Controls.Add(Me.TextBox6)
        Me.TabPage4.Controls.Add(Me.TextBox10)
        Me.TabPage4.Controls.Add(Me.TextBox9)
        Me.TabPage4.Controls.Add(Me.FlatCheckBox19)
        Me.TabPage4.Controls.Add(Me.FlatLabel14)
        Me.TabPage4.Controls.Add(Me.FlatLabel13)
        Me.TabPage4.Controls.Add(Me.FlatCheckBox18)
        Me.TabPage4.Controls.Add(Me.FlatComboBox4)
        Me.TabPage4.Controls.Add(Me.FlatProgressBar1)
        Me.TabPage4.Controls.Add(Me.FlatCheckBox17)
        Me.TabPage4.Controls.Add(Me.FlatCheckBox15)
        Me.TabPage4.Controls.Add(Me.FlatCheckBox14)
        Me.TabPage4.Controls.Add(Me.FlatCheckBox13)
        Me.TabPage4.Controls.Add(Me.FlatCheckBox12)
        Me.TabPage4.Controls.Add(Me.FlatCheckBox11)
        Me.TabPage4.Controls.Add(Me.FlatCheckBox10)
        Me.TabPage4.Controls.Add(Me.FlatCheckBox9)
        Me.TabPage4.Controls.Add(Me.FlatCheckBox8)
        Me.TabPage4.Controls.Add(Me.FlatCheckBox7)
        Me.TabPage4.Controls.Add(Me.FlatCheckBox6)
        Me.TabPage4.Controls.Add(Me.FlatCheckBox5)
        Me.TabPage4.Controls.Add(Me.FlatCheckBox4)
        Me.TabPage4.Controls.Add(Me.FlatCheckBox3)
        Me.TabPage4.Controls.Add(Me.FlatCheckBox2)
        Me.TabPage4.Controls.Add(Me.FlatCheckBox1)
        Me.TabPage4.Controls.Add(Me.FlatLabel6)
        Me.TabPage4.Controls.Add(Me.FlatTextBox1)
        Me.TabPage4.Controls.Add(Me.FlatTextBox3)
        Me.TabPage4.Controls.Add(Me.FlatTextBox4)
        Me.TabPage4.Controls.Add(Me.FlatTrackBar4)
        Me.TabPage4.Controls.Add(Me.FlatTextBox2)
        Me.TabPage4.Controls.Add(Me.FlatTrackBar3)
        Me.TabPage4.Controls.Add(Me.FlatTrackBar2)
        Me.TabPage4.Controls.Add(Me.FlatTrackBar1)
        Me.TabPage4.Controls.Add(Me.FlatButton1)
        Me.TabPage4.Controls.Add(Me.FlatComboBox2)
        Me.TabPage4.Controls.Add(Me.FlatTabControl2)
        Me.TabPage4.Location = New System.Drawing.Point(4, 44)
        Me.TabPage4.Name = "TabPage4"
        Me.TabPage4.Size = New System.Drawing.Size(578, 533)
        Me.TabPage4.TabIndex = 3
        Me.TabPage4.Text = "OPCIONES"
        '
        'FlatButton7
        '
        Me.FlatButton7.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FlatButton7.BackColor = System.Drawing.Color.Transparent
        Me.FlatButton7.BaseColor = System.Drawing.Color.Green
        Me.FlatButton7.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatButton7.Font = New System.Drawing.Font("Segoe UI", 8.0!)
        Me.FlatButton7.Location = New System.Drawing.Point(202, 82)
        Me.FlatButton7.Name = "FlatButton7"
        Me.FlatButton7.Rounded = False
        Me.FlatButton7.Size = New System.Drawing.Size(136, 40)
        Me.FlatButton7.TabIndex = 76
        Me.FlatButton7.Text = "VERIFICAR ARMA 3"
        Me.FlatButton7.TextColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        '
        'TextBox1
        '
        Me.TextBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.TextBox1.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.TextBox1.ForeColor = System.Drawing.Color.White
        Me.TextBox1.Location = New System.Drawing.Point(517, 331)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(51, 25)
        Me.TextBox1.TabIndex = 75
        Me.TextBox1.Visible = False
        '
        'FlatLabel12
        '
        Me.FlatLabel12.AutoSize = True
        Me.FlatLabel12.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel12.Font = New System.Drawing.Font("Segoe UI", 8.0!)
        Me.FlatLabel12.ForeColor = System.Drawing.Color.White
        Me.FlatLabel12.Location = New System.Drawing.Point(527, 316)
        Me.FlatLabel12.Name = "FlatLabel12"
        Me.FlatLabel12.Size = New System.Drawing.Size(29, 13)
        Me.FlatLabel12.TabIndex = 74
        Me.FlatLabel12.Text = "Pass"
        Me.FlatLabel12.Visible = False
        '
        'TextBox6
        '
        Me.TextBox6.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.TextBox6.ForeColor = System.Drawing.Color.White
        Me.TextBox6.Location = New System.Drawing.Point(186, 284)
        Me.TextBox6.Name = "TextBox6"
        Me.TextBox6.Size = New System.Drawing.Size(206, 25)
        Me.TextBox6.TabIndex = 72
        Me.TextBox6.Visible = False
        '
        'TextBox10
        '
        Me.TextBox10.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.TextBox10.ForeColor = System.Drawing.Color.White
        Me.TextBox10.Location = New System.Drawing.Point(478, 331)
        Me.TextBox10.Name = "TextBox10"
        Me.TextBox10.Size = New System.Drawing.Size(39, 25)
        Me.TextBox10.TabIndex = 71
        Me.TextBox10.Visible = False
        '
        'TextBox9
        '
        Me.TextBox9.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.TextBox9.ForeColor = System.Drawing.Color.White
        Me.TextBox9.Location = New System.Drawing.Point(378, 331)
        Me.TextBox9.Name = "TextBox9"
        Me.TextBox9.Size = New System.Drawing.Size(100, 25)
        Me.TextBox9.TabIndex = 70
        Me.TextBox9.Visible = False
        '
        'FlatCheckBox19
        '
        Me.FlatCheckBox19.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.FlatCheckBox19.BaseColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.FlatCheckBox19.BorderColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.FlatCheckBox19.Checked = False
        Me.FlatCheckBox19.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatCheckBox19.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.FlatCheckBox19.Location = New System.Drawing.Point(133, 334)
        Me.FlatCheckBox19.Name = "FlatCheckBox19"
        Me.FlatCheckBox19.Options = ESUS_LAUNCHER_A3.FlatCheckBox._Options.Style1
        Me.FlatCheckBox19.Size = New System.Drawing.Size(128, 22)
        Me.FlatCheckBox19.TabIndex = 69
        Me.FlatCheckBox19.Text = "Perfil De Jugador"
        Me.ToolTip1.SetToolTip(Me.FlatCheckBox19, "Permite seleccionar un Perfil de Jugador Predeterminado.")
        '
        'FlatLabel14
        '
        Me.FlatLabel14.AutoSize = True
        Me.FlatLabel14.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel14.Font = New System.Drawing.Font("Segoe UI", 8.0!)
        Me.FlatLabel14.ForeColor = System.Drawing.Color.White
        Me.FlatLabel14.Location = New System.Drawing.Point(476, 316)
        Me.FlatLabel14.Name = "FlatLabel14"
        Me.FlatLabel14.Size = New System.Drawing.Size(41, 13)
        Me.FlatLabel14.TabIndex = 68
        Me.FlatLabel14.Text = "Puerto"
        Me.FlatLabel14.Visible = False
        '
        'FlatLabel13
        '
        Me.FlatLabel13.AutoSize = True
        Me.FlatLabel13.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel13.Font = New System.Drawing.Font("Segoe UI", 8.0!)
        Me.FlatLabel13.ForeColor = System.Drawing.Color.White
        Me.FlatLabel13.Location = New System.Drawing.Point(379, 316)
        Me.FlatLabel13.Name = "FlatLabel13"
        Me.FlatLabel13.Size = New System.Drawing.Size(16, 13)
        Me.FlatLabel13.TabIndex = 67
        Me.FlatLabel13.Text = "IP"
        Me.FlatLabel13.Visible = False
        '
        'FlatCheckBox18
        '
        Me.FlatCheckBox18.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.FlatCheckBox18.BaseColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.FlatCheckBox18.BorderColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.FlatCheckBox18.Checked = False
        Me.FlatCheckBox18.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatCheckBox18.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.FlatCheckBox18.Location = New System.Drawing.Point(399, 285)
        Me.FlatCheckBox18.Name = "FlatCheckBox18"
        Me.FlatCheckBox18.Options = ESUS_LAUNCHER_A3.FlatCheckBox._Options.Style1
        Me.FlatCheckBox18.Size = New System.Drawing.Size(174, 22)
        Me.FlatCheckBox18.TabIndex = 64
        Me.FlatCheckBox18.Text = "Conf. Conexión Directa"
        Me.ToolTip1.SetToolTip(Me.FlatCheckBox18, resources.GetString("FlatCheckBox18.ToolTip"))
        '
        'FlatComboBox4
        '
        Me.FlatComboBox4.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.FlatComboBox4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatComboBox4.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.FlatComboBox4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.FlatComboBox4.Font = New System.Drawing.Font("Segoe UI", 8.0!)
        Me.FlatComboBox4.ForeColor = System.Drawing.Color.White
        Me.FlatComboBox4.FormattingEnabled = True
        Me.FlatComboBox4.HoverColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.FlatComboBox4.ItemHeight = 18
        Me.FlatComboBox4.Location = New System.Drawing.Point(261, 332)
        Me.FlatComboBox4.Name = "FlatComboBox4"
        Me.FlatComboBox4.Size = New System.Drawing.Size(113, 24)
        Me.FlatComboBox4.TabIndex = 62
        Me.FlatComboBox4.Visible = False
        '
        'FlatProgressBar1
        '
        Me.FlatProgressBar1.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.FlatProgressBar1.DarkerProgress = System.Drawing.Color.FromArgb(CType(CType(23, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.FlatProgressBar1.Location = New System.Drawing.Point(0, 315)
        Me.FlatProgressBar1.Maximum = 100
        Me.FlatProgressBar1.Name = "FlatProgressBar1"
        Me.FlatProgressBar1.ProgressColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.FlatProgressBar1.Size = New System.Drawing.Size(577, 42)
        Me.FlatProgressBar1.TabIndex = 61
        Me.FlatProgressBar1.Text = "FlatProgressBar1"
        Me.FlatProgressBar1.Value = 0
        Me.FlatProgressBar1.Visible = False
        '
        'FlatCheckBox17
        '
        Me.FlatCheckBox17.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.FlatCheckBox17.BaseColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.FlatCheckBox17.BorderColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.FlatCheckBox17.Checked = False
        Me.FlatCheckBox17.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatCheckBox17.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.FlatCheckBox17.Location = New System.Drawing.Point(209, 11)
        Me.FlatCheckBox17.Name = "FlatCheckBox17"
        Me.FlatCheckBox17.Options = ESUS_LAUNCHER_A3.FlatCheckBox._Options.Style1
        Me.FlatCheckBox17.Size = New System.Drawing.Size(161, 22)
        Me.FlatCheckBox17.TabIndex = 60
        Me.FlatCheckBox17.Text = "Usar Battleye"
        Me.ToolTip1.SetToolTip(Me.FlatCheckBox17, "Añade el parámetro -usebe." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Permite iniciar el juego junto al servicio de Battley" &
        "e.")
        '
        'FlatCheckBox15
        '
        Me.FlatCheckBox15.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.FlatCheckBox15.BaseColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.FlatCheckBox15.BorderColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.FlatCheckBox15.Checked = False
        Me.FlatCheckBox15.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatCheckBox15.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.FlatCheckBox15.Location = New System.Drawing.Point(399, 206)
        Me.FlatCheckBox15.Name = "FlatCheckBox15"
        Me.FlatCheckBox15.Options = ESUS_LAUNCHER_A3.FlatCheckBox._Options.Style1
        Me.FlatCheckBox15.Size = New System.Drawing.Size(161, 22)
        Me.FlatCheckBox15.TabIndex = 58
        Me.FlatCheckBox15.Text = "ExtraHilos"
        Me.ToolTip1.SetToolTip(Me.FlatCheckBox15, "Añade el parámetro -exThreads.")
        '
        'FlatCheckBox14
        '
        Me.FlatCheckBox14.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.FlatCheckBox14.BaseColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.FlatCheckBox14.BorderColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.FlatCheckBox14.Checked = False
        Me.FlatCheckBox14.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatCheckBox14.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.FlatCheckBox14.Location = New System.Drawing.Point(399, 140)
        Me.FlatCheckBox14.Name = "FlatCheckBox14"
        Me.FlatCheckBox14.Options = ESUS_LAUNCHER_A3.FlatCheckBox._Options.Style1
        Me.FlatCheckBox14.Size = New System.Drawing.Size(161, 22)
        Me.FlatCheckBox14.TabIndex = 57
        Me.FlatCheckBox14.Text = "Memoria de Video"
        Me.ToolTip1.SetToolTip(Me.FlatCheckBox14, "Añade el parámetro -maxVRAM.")
        '
        'FlatCheckBox13
        '
        Me.FlatCheckBox13.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.FlatCheckBox13.BaseColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.FlatCheckBox13.BorderColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.FlatCheckBox13.Checked = False
        Me.FlatCheckBox13.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatCheckBox13.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.FlatCheckBox13.Location = New System.Drawing.Point(399, 71)
        Me.FlatCheckBox13.Name = "FlatCheckBox13"
        Me.FlatCheckBox13.Options = ESUS_LAUNCHER_A3.FlatCheckBox._Options.Style1
        Me.FlatCheckBox13.Size = New System.Drawing.Size(161, 22)
        Me.FlatCheckBox13.TabIndex = 56
        Me.FlatCheckBox13.Text = "Memoria Ram"
        Me.ToolTip1.SetToolTip(Me.FlatCheckBox13, "Añade el parámetro -maxMem")
        '
        'FlatCheckBox12
        '
        Me.FlatCheckBox12.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.FlatCheckBox12.BaseColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.FlatCheckBox12.BorderColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.FlatCheckBox12.Checked = False
        Me.FlatCheckBox12.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatCheckBox12.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.FlatCheckBox12.Location = New System.Drawing.Point(399, 11)
        Me.FlatCheckBox12.Name = "FlatCheckBox12"
        Me.FlatCheckBox12.Options = ESUS_LAUNCHER_A3.FlatCheckBox._Options.Style1
        Me.FlatCheckBox12.Size = New System.Drawing.Size(161, 22)
        Me.FlatCheckBox12.TabIndex = 55
        Me.FlatCheckBox12.Text = "Numero de CPUS"
        Me.ToolTip1.SetToolTip(Me.FlatCheckBox12, "Añade el parámetro -cpucount.")
        '
        'FlatCheckBox11
        '
        Me.FlatCheckBox11.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.FlatCheckBox11.BaseColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.FlatCheckBox11.BorderColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.FlatCheckBox11.Checked = False
        Me.FlatCheckBox11.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatCheckBox11.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.FlatCheckBox11.Location = New System.Drawing.Point(8, 287)
        Me.FlatCheckBox11.Name = "FlatCheckBox11"
        Me.FlatCheckBox11.Options = ESUS_LAUNCHER_A3.FlatCheckBox._Options.Style1
        Me.FlatCheckBox11.Size = New System.Drawing.Size(178, 22)
        Me.FlatCheckBox11.TabIndex = 54
        Me.FlatCheckBox11.Text = "Añadir parámetro custom"
        Me.ToolTip1.SetToolTip(Me.FlatCheckBox11, "Permite añadir una línea de parámetros definidos por el usuario.")
        '
        'FlatCheckBox10
        '
        Me.FlatCheckBox10.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.FlatCheckBox10.BaseColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.FlatCheckBox10.BorderColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.FlatCheckBox10.Checked = False
        Me.FlatCheckBox10.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatCheckBox10.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.FlatCheckBox10.Location = New System.Drawing.Point(8, 146)
        Me.FlatCheckBox10.Name = "FlatCheckBox10"
        Me.FlatCheckBox10.Options = ESUS_LAUNCHER_A3.FlatCheckBox._Options.Style1
        Me.FlatCheckBox10.Size = New System.Drawing.Size(161, 22)
        Me.FlatCheckBox10.TabIndex = 53
        Me.FlatCheckBox10.Text = "Activar Hyperthreading"
        Me.ToolTip1.SetToolTip(Me.FlatCheckBox10, "Añade el parámetro -enableHT." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Permite el uso de núcleos con Hyperthreading.")
        '
        'FlatCheckBox9
        '
        Me.FlatCheckBox9.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.FlatCheckBox9.BaseColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.FlatCheckBox9.BorderColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.FlatCheckBox9.Checked = False
        Me.FlatCheckBox9.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatCheckBox9.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.FlatCheckBox9.Location = New System.Drawing.Point(8, 229)
        Me.FlatCheckBox9.Name = "FlatCheckBox9"
        Me.FlatCheckBox9.Options = ESUS_LAUNCHER_A3.FlatCheckBox._Options.Style1
        Me.FlatCheckBox9.Size = New System.Drawing.Size(161, 22)
        Me.FlatCheckBox9.TabIndex = 52
        Me.FlatCheckBox9.Text = "Forzar DX9"
        Me.ToolTip1.SetToolTip(Me.FlatCheckBox9, "Añade el parámetro -winxp." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Ejecuta el juego en modo DX9, puede solucionar proble" &
        "mas de compatibilidad en sistemas Multi GPU." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10))
        '
        'FlatCheckBox8
        '
        Me.FlatCheckBox8.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.FlatCheckBox8.BaseColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.FlatCheckBox8.BorderColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.FlatCheckBox8.Checked = False
        Me.FlatCheckBox8.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatCheckBox8.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.FlatCheckBox8.Location = New System.Drawing.Point(8, 257)
        Me.FlatCheckBox8.Name = "FlatCheckBox8"
        Me.FlatCheckBox8.Options = ESUS_LAUNCHER_A3.FlatCheckBox._Options.Style1
        Me.FlatCheckBox8.Size = New System.Drawing.Size(225, 22)
        Me.FlatCheckBox8.TabIndex = 51
        Me.FlatCheckBox8.Text = "Permitir Archivos Descomprimidos"
        Me.ToolTip1.SetToolTip(Me.FlatCheckBox8, "Añade el parámetro -FilePatching." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Permite al juego la carga de archivos descompr" &
        "imidos, en lugar de solo PBOS")
        '
        'FlatCheckBox7
        '
        Me.FlatCheckBox7.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.FlatCheckBox7.BaseColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.FlatCheckBox7.BorderColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.FlatCheckBox7.Checked = False
        Me.FlatCheckBox7.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatCheckBox7.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.FlatCheckBox7.Location = New System.Drawing.Point(8, 201)
        Me.FlatCheckBox7.Name = "FlatCheckBox7"
        Me.FlatCheckBox7.Options = ESUS_LAUNCHER_A3.FlatCheckBox._Options.Style1
        Me.FlatCheckBox7.Size = New System.Drawing.Size(178, 22)
        Me.FlatCheckBox7.TabIndex = 50
        Me.FlatCheckBox7.Text = "Mostrar Errores de Script"
        Me.ToolTip1.SetToolTip(Me.FlatCheckBox7, "Añade el parámetro -showscripterrors." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Permite mostrar errores de script, ideal p" &
        "ara edición y modders.")
        '
        'FlatCheckBox6
        '
        Me.FlatCheckBox6.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.FlatCheckBox6.BaseColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.FlatCheckBox6.BorderColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.FlatCheckBox6.Checked = False
        Me.FlatCheckBox6.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatCheckBox6.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.FlatCheckBox6.Location = New System.Drawing.Point(8, 173)
        Me.FlatCheckBox6.Name = "FlatCheckBox6"
        Me.FlatCheckBox6.Options = ESUS_LAUNCHER_A3.FlatCheckBox._Options.Style1
        Me.FlatCheckBox6.Size = New System.Drawing.Size(119, 22)
        Me.FlatCheckBox6.TabIndex = 49
        Me.FlatCheckBox6.Text = "Modo Ventana"
        Me.ToolTip1.SetToolTip(Me.FlatCheckBox6, "Añade el parámetro -window." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Ejecuta el juego en modo ventana.")
        '
        'FlatCheckBox5
        '
        Me.FlatCheckBox5.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.FlatCheckBox5.BaseColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.FlatCheckBox5.BorderColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.FlatCheckBox5.Checked = False
        Me.FlatCheckBox5.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatCheckBox5.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.FlatCheckBox5.Location = New System.Drawing.Point(8, 121)
        Me.FlatCheckBox5.Name = "FlatCheckBox5"
        Me.FlatCheckBox5.Options = ESUS_LAUNCHER_A3.FlatCheckBox._Options.Style1
        Me.FlatCheckBox5.Size = New System.Drawing.Size(161, 22)
        Me.FlatCheckBox5.TabIndex = 48
        Me.FlatCheckBox5.Text = "No Logs"
        Me.ToolTip1.SetToolTip(Me.FlatCheckBox5, "Añade el parámetro -nologs." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Deshabilita el guardado de registro de errores.")
        '
        'FlatCheckBox4
        '
        Me.FlatCheckBox4.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.FlatCheckBox4.BaseColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.FlatCheckBox4.BorderColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.FlatCheckBox4.Checked = False
        Me.FlatCheckBox4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatCheckBox4.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.FlatCheckBox4.Location = New System.Drawing.Point(8, 93)
        Me.FlatCheckBox4.Name = "FlatCheckBox4"
        Me.FlatCheckBox4.Options = ESUS_LAUNCHER_A3.FlatCheckBox._Options.Style1
        Me.FlatCheckBox4.Size = New System.Drawing.Size(161, 22)
        Me.FlatCheckBox4.TabIndex = 47
        Me.FlatCheckBox4.Text = "Ninguna Pausa"
        Me.ToolTip1.SetToolTip(Me.FlatCheckBox4, "Añade el parámetro -nopause." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Permite que el juego siga funcionando, aun cuando l" &
        "a ventana no tenga el foco (es decir, se encuentre en segundo plano)")
        '
        'FlatCheckBox3
        '
        Me.FlatCheckBox3.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.FlatCheckBox3.BaseColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.FlatCheckBox3.BorderColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.FlatCheckBox3.Checked = False
        Me.FlatCheckBox3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatCheckBox3.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.FlatCheckBox3.Location = New System.Drawing.Point(8, 65)
        Me.FlatCheckBox3.Name = "FlatCheckBox3"
        Me.FlatCheckBox3.Options = ESUS_LAUNCHER_A3.FlatCheckBox._Options.Style1
        Me.FlatCheckBox3.Size = New System.Drawing.Size(161, 22)
        Me.FlatCheckBox3.TabIndex = 46
        Me.FlatCheckBox3.Text = "Mundo vacío"
        Me.ToolTip1.SetToolTip(Me.FlatCheckBox3, "Añade el parámetro -world=empty." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Inicia el juego sin ningún mapa precargado.")
        '
        'FlatCheckBox2
        '
        Me.FlatCheckBox2.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.FlatCheckBox2.BaseColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.FlatCheckBox2.BorderColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.FlatCheckBox2.Checked = False
        Me.FlatCheckBox2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatCheckBox2.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.FlatCheckBox2.Location = New System.Drawing.Point(8, 37)
        Me.FlatCheckBox2.Name = "FlatCheckBox2"
        Me.FlatCheckBox2.Options = ESUS_LAUNCHER_A3.FlatCheckBox._Options.Style1
        Me.FlatCheckBox2.Size = New System.Drawing.Size(161, 22)
        Me.FlatCheckBox2.TabIndex = 45
        Me.FlatCheckBox2.Text = "Saltar Intro"
        Me.ToolTip1.SetToolTip(Me.FlatCheckBox2, "Añade el parámetro -skipintro." & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Deshabilita intros de mapas en el menu principal " &
        "de forma permanente.")
        '
        'FlatCheckBox1
        '
        Me.FlatCheckBox1.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.FlatCheckBox1.BaseColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.FlatCheckBox1.BorderColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.FlatCheckBox1.Checked = False
        Me.FlatCheckBox1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatCheckBox1.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.FlatCheckBox1.Location = New System.Drawing.Point(8, 11)
        Me.FlatCheckBox1.Name = "FlatCheckBox1"
        Me.FlatCheckBox1.Options = ESUS_LAUNCHER_A3.FlatCheckBox._Options.Style1
        Me.FlatCheckBox1.Size = New System.Drawing.Size(161, 22)
        Me.FlatCheckBox1.TabIndex = 44
        Me.FlatCheckBox1.Text = "Saltar Pantalla de Inicio"
        Me.ToolTip1.SetToolTip(Me.FlatCheckBox1, "Añade el parámetro -nosplash" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Deshabilita la pantalla de bienvenida")
        '
        'FlatLabel6
        '
        Me.FlatLabel6.AutoSize = True
        Me.FlatLabel6.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel6.Font = New System.Drawing.Font("Segoe UI", 8.0!)
        Me.FlatLabel6.ForeColor = System.Drawing.Color.White
        Me.FlatLabel6.Location = New System.Drawing.Point(7, 317)
        Me.FlatLabel6.Name = "FlatLabel6"
        Me.FlatLabel6.Size = New System.Drawing.Size(110, 13)
        Me.FlatLabel6.TabIndex = 37
        Me.FlatLabel6.Text = "Después de lanzar..."
        '
        'FlatTextBox1
        '
        Me.FlatTextBox1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FlatTextBox1.BackColor = System.Drawing.Color.Transparent
        Me.FlatTextBox1.Enabled = False
        Me.FlatTextBox1.Location = New System.Drawing.Point(515, 38)
        Me.FlatTextBox1.MaxLength = 32767
        Me.FlatTextBox1.Multiline = False
        Me.FlatTextBox1.Name = "FlatTextBox1"
        Me.FlatTextBox1.ReadOnly = False
        Me.FlatTextBox1.Size = New System.Drawing.Size(48, 29)
        Me.FlatTextBox1.TabIndex = 33
        Me.FlatTextBox1.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.FlatTextBox1.TextColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.FlatTextBox1.UseSystemPasswordChar = False
        '
        'FlatTextBox3
        '
        Me.FlatTextBox3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FlatTextBox3.BackColor = System.Drawing.Color.Transparent
        Me.FlatTextBox3.Enabled = False
        Me.FlatTextBox3.Location = New System.Drawing.Point(516, 97)
        Me.FlatTextBox3.MaxLength = 32767
        Me.FlatTextBox3.Multiline = False
        Me.FlatTextBox3.Name = "FlatTextBox3"
        Me.FlatTextBox3.ReadOnly = False
        Me.FlatTextBox3.Size = New System.Drawing.Size(47, 29)
        Me.FlatTextBox3.TabIndex = 1
        Me.FlatTextBox3.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.FlatTextBox3.TextColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.FlatTextBox3.UseSystemPasswordChar = False
        '
        'FlatTextBox4
        '
        Me.FlatTextBox4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FlatTextBox4.BackColor = System.Drawing.Color.Transparent
        Me.FlatTextBox4.Enabled = False
        Me.FlatTextBox4.Location = New System.Drawing.Point(515, 236)
        Me.FlatTextBox4.MaxLength = 32767
        Me.FlatTextBox4.Multiline = False
        Me.FlatTextBox4.Name = "FlatTextBox4"
        Me.FlatTextBox4.ReadOnly = False
        Me.FlatTextBox4.Size = New System.Drawing.Size(45, 29)
        Me.FlatTextBox4.TabIndex = 22
        Me.FlatTextBox4.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.FlatTextBox4.TextColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.FlatTextBox4.UseSystemPasswordChar = False
        '
        'FlatTrackBar4
        '
        Me.FlatTrackBar4.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FlatTrackBar4.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.FlatTrackBar4.HatchColor = System.Drawing.Color.FromArgb(CType(CType(23, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.FlatTrackBar4.Location = New System.Drawing.Point(433, 242)
        Me.FlatTrackBar4.Maximum = 7
        Me.FlatTrackBar4.Minimum = 0
        Me.FlatTrackBar4.Name = "FlatTrackBar4"
        Me.FlatTrackBar4.ShowValue = False
        Me.FlatTrackBar4.Size = New System.Drawing.Size(75, 23)
        Me.FlatTrackBar4.Style = ESUS_LAUNCHER_A3.FlatTrackBar._Style.Slider
        Me.FlatTrackBar4.TabIndex = 21
        Me.FlatTrackBar4.Text = "FlatTrackBar4"
        Me.FlatTrackBar4.TrackColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.FlatTrackBar4.Value = 0
        '
        'FlatTextBox2
        '
        Me.FlatTextBox2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FlatTextBox2.BackColor = System.Drawing.Color.Transparent
        Me.FlatTextBox2.Enabled = False
        Me.FlatTextBox2.Location = New System.Drawing.Point(515, 168)
        Me.FlatTextBox2.MaxLength = 32767
        Me.FlatTextBox2.Multiline = False
        Me.FlatTextBox2.Name = "FlatTextBox2"
        Me.FlatTextBox2.ReadOnly = False
        Me.FlatTextBox2.Size = New System.Drawing.Size(46, 29)
        Me.FlatTextBox2.TabIndex = 0
        Me.FlatTextBox2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.FlatTextBox2.TextColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.FlatTextBox2.UseSystemPasswordChar = False
        '
        'FlatTrackBar3
        '
        Me.FlatTrackBar3.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FlatTrackBar3.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.FlatTrackBar3.HatchColor = System.Drawing.Color.FromArgb(CType(CType(23, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.FlatTrackBar3.Location = New System.Drawing.Point(433, 99)
        Me.FlatTrackBar3.Maximum = 32768
        Me.FlatTrackBar3.Minimum = 0
        Me.FlatTrackBar3.Name = "FlatTrackBar3"
        Me.FlatTrackBar3.ShowValue = False
        Me.FlatTrackBar3.Size = New System.Drawing.Size(75, 23)
        Me.FlatTrackBar3.Style = ESUS_LAUNCHER_A3.FlatTrackBar._Style.Slider
        Me.FlatTrackBar3.TabIndex = 20
        Me.FlatTrackBar3.Text = "FlatTrackBar3"
        Me.FlatTrackBar3.TrackColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.FlatTrackBar3.Value = 0
        '
        'FlatTrackBar2
        '
        Me.FlatTrackBar2.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FlatTrackBar2.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.FlatTrackBar2.HatchColor = System.Drawing.Color.FromArgb(CType(CType(23, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.FlatTrackBar2.Location = New System.Drawing.Point(433, 173)
        Me.FlatTrackBar2.Maximum = 8192
        Me.FlatTrackBar2.Minimum = 0
        Me.FlatTrackBar2.Name = "FlatTrackBar2"
        Me.FlatTrackBar2.ShowValue = False
        Me.FlatTrackBar2.Size = New System.Drawing.Size(75, 23)
        Me.FlatTrackBar2.Style = ESUS_LAUNCHER_A3.FlatTrackBar._Style.Slider
        Me.FlatTrackBar2.TabIndex = 19
        Me.FlatTrackBar2.Text = "FlatTrackBar2"
        Me.FlatTrackBar2.TrackColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.FlatTrackBar2.Value = 0
        '
        'FlatTrackBar1
        '
        Me.FlatTrackBar1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FlatTrackBar1.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.FlatTrackBar1.HatchColor = System.Drawing.Color.FromArgb(CType(CType(23, Byte), Integer), CType(CType(148, Byte), Integer), CType(CType(92, Byte), Integer))
        Me.FlatTrackBar1.Location = New System.Drawing.Point(433, 40)
        Me.FlatTrackBar1.Maximum = 16
        Me.FlatTrackBar1.Minimum = 0
        Me.FlatTrackBar1.Name = "FlatTrackBar1"
        Me.FlatTrackBar1.ShowValue = False
        Me.FlatTrackBar1.Size = New System.Drawing.Size(75, 23)
        Me.FlatTrackBar1.Style = ESUS_LAUNCHER_A3.FlatTrackBar._Style.Slider
        Me.FlatTrackBar1.TabIndex = 18
        Me.FlatTrackBar1.Text = "FlatTrackBar1"
        Me.FlatTrackBar1.TrackColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.FlatTrackBar1.Value = 0
        '
        'FlatButton1
        '
        Me.FlatButton1.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FlatButton1.BackColor = System.Drawing.Color.Black
        Me.FlatButton1.BaseColor = System.Drawing.Color.Green
        Me.FlatButton1.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatButton1.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.FlatButton1.ForeColor = System.Drawing.Color.Black
        Me.FlatButton1.Location = New System.Drawing.Point(202, 40)
        Me.FlatButton1.Name = "FlatButton1"
        Me.FlatButton1.Rounded = False
        Me.FlatButton1.Size = New System.Drawing.Size(136, 41)
        Me.FlatButton1.TabIndex = 16
        Me.FlatButton1.Text = "CONFIGURACIÓN RECOMENDADA"
        Me.FlatButton1.TextColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        Me.ToolTip1.SetToolTip(Me.FlatButton1, "Realiza una selección de opciones recomendadas, de acuerdo a su PC")
        '
        'FlatComboBox2
        '
        Me.FlatComboBox2.BackColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(45, Byte), Integer), CType(CType(48, Byte), Integer))
        Me.FlatComboBox2.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatComboBox2.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed
        Me.FlatComboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.FlatComboBox2.Font = New System.Drawing.Font("Segoe UI", 8.0!)
        Me.FlatComboBox2.ForeColor = System.Drawing.Color.White
        Me.FlatComboBox2.FormattingEnabled = True
        Me.FlatComboBox2.HoverColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.FlatComboBox2.ItemHeight = 18
        Me.FlatComboBox2.Items.AddRange(New Object() {"No Hacer Nada", "Minimizar", "Cerrar"})
        Me.FlatComboBox2.Location = New System.Drawing.Point(6, 333)
        Me.FlatComboBox2.Name = "FlatComboBox2"
        Me.FlatComboBox2.Size = New System.Drawing.Size(121, 24)
        Me.FlatComboBox2.TabIndex = 2
        '
        'FlatTabControl2
        '
        Me.FlatTabControl2.ActiveColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(163, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.FlatTabControl2.Anchor = CType(((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.FlatTabControl2.Appearance = System.Windows.Forms.TabAppearance.FlatButtons
        Me.FlatTabControl2.BaseColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.FlatTabControl2.Controls.Add(Me.TabPage5)
        Me.FlatTabControl2.Controls.Add(Me.TabPage6)
        Me.FlatTabControl2.Controls.Add(Me.TabPage7)
        Me.FlatTabControl2.Font = New System.Drawing.Font("Segoe UI", 10.0!)
        Me.FlatTabControl2.ItemSize = New System.Drawing.Size(120, 40)
        Me.FlatTabControl2.Location = New System.Drawing.Point(0, 363)
        Me.FlatTabControl2.Name = "FlatTabControl2"
        Me.FlatTabControl2.SelectedIndex = 0
        Me.FlatTabControl2.Size = New System.Drawing.Size(577, 170)
        Me.FlatTabControl2.SizeMode = System.Windows.Forms.TabSizeMode.Fixed
        Me.FlatTabControl2.TabIndex = 0
        '
        'TabPage5
        '
        Me.TabPage5.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.TabPage5.Controls.Add(Me.Panel1)
        Me.TabPage5.Location = New System.Drawing.Point(4, 44)
        Me.TabPage5.Name = "TabPage5"
        Me.TabPage5.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage5.Size = New System.Drawing.Size(569, 122)
        Me.TabPage5.TabIndex = 0
        Me.TabPage5.Text = "Perfiles"
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.Button8)
        Me.Panel1.Controls.Add(Me.Button7)
        Me.Panel1.Controls.Add(Me.ProgressBar2)
        Me.Panel1.Controls.Add(Me.ListBox2)
        Me.Panel1.Controls.Add(Me.FlatTextBox7)
        Me.Panel1.Controls.Add(Me.Button5)
        Me.Panel1.Controls.Add(Me.Button4)
        Me.Panel1.Controls.Add(Me.Button3)
        Me.Panel1.Controls.Add(Me.Button2)
        Me.Panel1.Location = New System.Drawing.Point(2, 0)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(560, 126)
        Me.Panel1.TabIndex = 0
        '
        'Button8
        '
        Me.Button8.Font = New System.Drawing.Font("Segoe UI", 8.0!)
        Me.Button8.Location = New System.Drawing.Point(2, 87)
        Me.Button8.Name = "Button8"
        Me.Button8.Size = New System.Drawing.Size(111, 32)
        Me.Button8.TabIndex = 36
        Me.Button8.Text = "Source Code R.29"
        Me.Button8.UseVisualStyleBackColor = True
        '
        'Button7
        '
        Me.Button7.Location = New System.Drawing.Point(1, 35)
        Me.Button7.Name = "Button7"
        Me.Button7.Size = New System.Drawing.Size(131, 32)
        Me.Button7.TabIndex = 35
        Me.Button7.Text = "Revisar Archivos"
        Me.Button7.UseVisualStyleBackColor = True
        Me.Button7.Visible = False
        '
        'ProgressBar2
        '
        Me.ProgressBar2.Location = New System.Drawing.Point(136, 37)
        Me.ProgressBar2.Name = "ProgressBar2"
        Me.ProgressBar2.Size = New System.Drawing.Size(196, 23)
        Me.ProgressBar2.TabIndex = 34
        Me.ProgressBar2.Visible = False
        '
        'ListBox2
        '
        Me.ListBox2.FormattingEnabled = True
        Me.ListBox2.ItemHeight = 17
        Me.ListBox2.Location = New System.Drawing.Point(425, -1)
        Me.ListBox2.Name = "ListBox2"
        Me.ListBox2.Size = New System.Drawing.Size(132, 123)
        Me.ListBox2.TabIndex = 32
        '
        'FlatTextBox7
        '
        Me.FlatTextBox7.BackColor = System.Drawing.Color.Transparent
        Me.FlatTextBox7.Location = New System.Drawing.Point(2, 7)
        Me.FlatTextBox7.MaxLength = 32767
        Me.FlatTextBox7.Multiline = False
        Me.FlatTextBox7.Name = "FlatTextBox7"
        Me.FlatTextBox7.ReadOnly = False
        Me.FlatTextBox7.Size = New System.Drawing.Size(225, 29)
        Me.FlatTextBox7.TabIndex = 25
        Me.FlatTextBox7.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.FlatTextBox7.TextColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.FlatTextBox7.UseSystemPasswordChar = False
        '
        'Button5
        '
        Me.Button5.Location = New System.Drawing.Point(337, 66)
        Me.Button5.Name = "Button5"
        Me.Button5.Size = New System.Drawing.Size(87, 32)
        Me.Button5.TabIndex = 31
        Me.Button5.Text = "Renombrar"
        Me.Button5.UseVisualStyleBackColor = True
        '
        'Button4
        '
        Me.Button4.Location = New System.Drawing.Point(337, 35)
        Me.Button4.Name = "Button4"
        Me.Button4.Size = New System.Drawing.Size(87, 32)
        Me.Button4.TabIndex = 30
        Me.Button4.Text = "Borrar"
        Me.Button4.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(337, 4)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(87, 32)
        Me.Button3.TabIndex = 29
        Me.Button3.Text = "Favorito"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(227, 5)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 32)
        Me.Button2.TabIndex = 28
        Me.Button2.Text = "Nuevo"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'TabPage6
        '
        Me.TabPage6.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.TabPage6.Controls.Add(Me.FlatCheckBox16)
        Me.TabPage6.Controls.Add(Me.FlatLabel5)
        Me.TabPage6.Controls.Add(Me.FlatLabel4)
        Me.TabPage6.Controls.Add(Me.FlatLabel2)
        Me.TabPage6.Controls.Add(Me.FlatTextBox8)
        Me.TabPage6.Controls.Add(Me.FlatButton4)
        Me.TabPage6.Controls.Add(Me.FlatLabel1)
        Me.TabPage6.Controls.Add(Me.FlatButton3)
        Me.TabPage6.Controls.Add(Me.FlatTextBox5)
        Me.TabPage6.Location = New System.Drawing.Point(4, 44)
        Me.TabPage6.Name = "TabPage6"
        Me.TabPage6.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage6.Size = New System.Drawing.Size(569, 122)
        Me.TabPage6.TabIndex = 1
        Me.TabPage6.Text = "Rutas"
        '
        'FlatCheckBox16
        '
        Me.FlatCheckBox16.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.FlatCheckBox16.BaseColor = System.Drawing.Color.FromArgb(CType(CType(45, Byte), Integer), CType(CType(47, Byte), Integer), CType(CType(49, Byte), Integer))
        Me.FlatCheckBox16.BorderColor = System.Drawing.Color.FromArgb(CType(CType(35, Byte), Integer), CType(CType(168, Byte), Integer), CType(CType(109, Byte), Integer))
        Me.FlatCheckBox16.Checked = False
        Me.FlatCheckBox16.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatCheckBox16.Font = New System.Drawing.Font("Segoe UI", 11.0!)
        Me.FlatCheckBox16.Location = New System.Drawing.Point(396, 8)
        Me.FlatCheckBox16.Name = "FlatCheckBox16"
        Me.FlatCheckBox16.Options = ESUS_LAUNCHER_A3.FlatCheckBox._Options.Style1
        Me.FlatCheckBox16.Size = New System.Drawing.Size(149, 22)
        Me.FlatCheckBox16.TabIndex = 61
        Me.FlatCheckBox16.Text = "Rutas Alternativas"
        '
        'FlatLabel5
        '
        Me.FlatLabel5.AutoSize = True
        Me.FlatLabel5.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel5.Font = New System.Drawing.Font("Segoe UI", 8.0!)
        Me.FlatLabel5.ForeColor = System.Drawing.Color.White
        Me.FlatLabel5.Location = New System.Drawing.Point(5, 68)
        Me.FlatLabel5.Name = "FlatLabel5"
        Me.FlatLabel5.Size = New System.Drawing.Size(89, 13)
        Me.FlatLabel5.TabIndex = 37
        Me.FlatLabel5.Text = "Ruta de addons"
        '
        'FlatLabel4
        '
        Me.FlatLabel4.AutoSize = True
        Me.FlatLabel4.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel4.Font = New System.Drawing.Font("Segoe UI", 8.0!)
        Me.FlatLabel4.ForeColor = System.Drawing.Color.White
        Me.FlatLabel4.Location = New System.Drawing.Point(7, 18)
        Me.FlatLabel4.Name = "FlatLabel4"
        Me.FlatLabel4.Size = New System.Drawing.Size(78, 13)
        Me.FlatLabel4.TabIndex = 36
        Me.FlatLabel4.Text = "Exe de Arma 3"
        '
        'FlatLabel2
        '
        Me.FlatLabel2.AutoSize = True
        Me.FlatLabel2.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel2.Font = New System.Drawing.Font("Segoe UI", 8.0!)
        Me.FlatLabel2.ForeColor = System.Drawing.Color.White
        Me.FlatLabel2.Location = New System.Drawing.Point(10, 92)
        Me.FlatLabel2.Name = "FlatLabel2"
        Me.FlatLabel2.Size = New System.Drawing.Size(59, 13)
        Me.FlatLabel2.TabIndex = 35
        Me.FlatLabel2.Text = "FlatLabel2"
        '
        'FlatTextBox8
        '
        Me.FlatTextBox8.BackColor = System.Drawing.Color.Transparent
        Me.FlatTextBox8.Location = New System.Drawing.Point(6, 82)
        Me.FlatTextBox8.MaxLength = 32767
        Me.FlatTextBox8.Multiline = False
        Me.FlatTextBox8.Name = "FlatTextBox8"
        Me.FlatTextBox8.ReadOnly = False
        Me.FlatTextBox8.Size = New System.Drawing.Size(456, 29)
        Me.FlatTextBox8.TabIndex = 34
        Me.FlatTextBox8.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.FlatTextBox8.TextColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.FlatTextBox8.UseSystemPasswordChar = False
        '
        'FlatButton4
        '
        Me.FlatButton4.BackColor = System.Drawing.Color.Transparent
        Me.FlatButton4.BaseColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(163, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.FlatButton4.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatButton4.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.FlatButton4.Location = New System.Drawing.Point(463, 82)
        Me.FlatButton4.Name = "FlatButton4"
        Me.FlatButton4.Rounded = False
        Me.FlatButton4.Size = New System.Drawing.Size(77, 29)
        Me.FlatButton4.TabIndex = 18
        Me.FlatButton4.Text = "..."
        Me.FlatButton4.TextColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        '
        'FlatLabel1
        '
        Me.FlatLabel1.AutoSize = True
        Me.FlatLabel1.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel1.Font = New System.Drawing.Font("Segoe UI", 8.0!)
        Me.FlatLabel1.ForeColor = System.Drawing.Color.White
        Me.FlatLabel1.Location = New System.Drawing.Point(8, 45)
        Me.FlatLabel1.Name = "FlatLabel1"
        Me.FlatLabel1.Size = New System.Drawing.Size(59, 13)
        Me.FlatLabel1.TabIndex = 19
        Me.FlatLabel1.Text = "FlatLabel1"
        '
        'FlatButton3
        '
        Me.FlatButton3.BackColor = System.Drawing.Color.Transparent
        Me.FlatButton3.BaseColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(163, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.FlatButton3.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatButton3.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.FlatButton3.Location = New System.Drawing.Point(462, 36)
        Me.FlatButton3.Name = "FlatButton3"
        Me.FlatButton3.Rounded = False
        Me.FlatButton3.Size = New System.Drawing.Size(76, 29)
        Me.FlatButton3.TabIndex = 17
        Me.FlatButton3.Text = "..."
        Me.FlatButton3.TextColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        '
        'FlatTextBox5
        '
        Me.FlatTextBox5.BackColor = System.Drawing.Color.Transparent
        Me.FlatTextBox5.Location = New System.Drawing.Point(8, 36)
        Me.FlatTextBox5.MaxLength = 32767
        Me.FlatTextBox5.Multiline = False
        Me.FlatTextBox5.Name = "FlatTextBox5"
        Me.FlatTextBox5.ReadOnly = False
        Me.FlatTextBox5.Size = New System.Drawing.Size(455, 29)
        Me.FlatTextBox5.TabIndex = 23
        Me.FlatTextBox5.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
        Me.FlatTextBox5.TextColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.FlatTextBox5.UseSystemPasswordChar = False
        '
        'TabPage7
        '
        Me.TabPage7.BackColor = System.Drawing.Color.FromArgb(CType(CType(60, Byte), Integer), CType(CType(70, Byte), Integer), CType(CType(73, Byte), Integer))
        Me.TabPage7.Controls.Add(Me.FlatLabel7)
        Me.TabPage7.Controls.Add(Me.FlatButton5)
        Me.TabPage7.Controls.Add(Me.FlatButton6)
        Me.TabPage7.Controls.Add(Me.CheckedListBox3)
        Me.TabPage7.Location = New System.Drawing.Point(4, 44)
        Me.TabPage7.Name = "TabPage7"
        Me.TabPage7.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage7.Size = New System.Drawing.Size(569, 122)
        Me.TabPage7.TabIndex = 2
        Me.TabPage7.Text = "Programas Externos"
        '
        'FlatLabel7
        '
        Me.FlatLabel7.AutoSize = True
        Me.FlatLabel7.BackColor = System.Drawing.Color.Transparent
        Me.FlatLabel7.Font = New System.Drawing.Font("Segoe UI", 8.0!)
        Me.FlatLabel7.ForeColor = System.Drawing.Color.White
        Me.FlatLabel7.Location = New System.Drawing.Point(8, 8)
        Me.FlatLabel7.Name = "FlatLabel7"
        Me.FlatLabel7.Size = New System.Drawing.Size(233, 13)
        Me.FlatLabel7.TabIndex = 38
        Me.FlatLabel7.Text = "Programas que se ejecutarán junto a Arma 3"
        '
        'FlatButton5
        '
        Me.FlatButton5.BackColor = System.Drawing.Color.Transparent
        Me.FlatButton5.BaseColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(163, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.FlatButton5.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatButton5.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.FlatButton5.Location = New System.Drawing.Point(444, 56)
        Me.FlatButton5.Name = "FlatButton5"
        Me.FlatButton5.Rounded = False
        Me.FlatButton5.Size = New System.Drawing.Size(96, 30)
        Me.FlatButton5.TabIndex = 20
        Me.FlatButton5.Text = "Quitar"
        Me.FlatButton5.TextColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        '
        'FlatButton6
        '
        Me.FlatButton6.BackColor = System.Drawing.Color.Transparent
        Me.FlatButton6.BaseColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(163, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.FlatButton6.Cursor = System.Windows.Forms.Cursors.Hand
        Me.FlatButton6.Font = New System.Drawing.Font("Segoe UI", 12.0!)
        Me.FlatButton6.Location = New System.Drawing.Point(444, 26)
        Me.FlatButton6.Name = "FlatButton6"
        Me.FlatButton6.Rounded = False
        Me.FlatButton6.Size = New System.Drawing.Size(96, 27)
        Me.FlatButton6.TabIndex = 19
        Me.FlatButton6.Text = "Añadir"
        Me.FlatButton6.TextColor = System.Drawing.Color.FromArgb(CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(243, Byte), Integer))
        '
        'CheckedListBox3
        '
        Me.CheckedListBox3.CheckOnClick = True
        Me.CheckedListBox3.FormattingEnabled = True
        Me.CheckedListBox3.Location = New System.Drawing.Point(6, 26)
        Me.CheckedListBox3.Name = "CheckedListBox3"
        Me.CheckedListBox3.Size = New System.Drawing.Size(432, 84)
        Me.CheckedListBox3.TabIndex = 15
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(96.0!, 96.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Dpi
        Me.BackColor = System.Drawing.Color.Black
        Me.ClientSize = New System.Drawing.Size(582, 695)
        Me.Controls.Add(Me.CheckBox1)
        Me.Controls.Add(Me.FlatLabel10)
        Me.Controls.Add(Me.MaterialRaisedButton11)
        Me.Controls.Add(Me.FlatComboBox1)
        Me.Controls.Add(Me.FlatComboBox3)
        Me.Controls.Add(Me.FlatToggle1)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.MaterialRaisedButton10)
        Me.Controls.Add(Me.FlatTabControl1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Form1"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "ESUS LAUNCHER A3"
        Me.ContextMenuStrip2.ResumeLayout(False)
        Me.ContextMenuStrip1.ResumeLayout(False)
        Me.FlatTabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        Me.TabPage1.PerformLayout()
        Me.FlatGroupBox1.ResumeLayout(False)
        Me.FlatGroupBox1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        Me.TabPage3.ResumeLayout(False)
        Me.TabPage3.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox11, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox10, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox2, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage4.ResumeLayout(False)
        Me.TabPage4.PerformLayout()
        Me.FlatTabControl2.ResumeLayout(False)
        Me.TabPage5.ResumeLayout(False)
        Me.Panel1.ResumeLayout(False)
        Me.TabPage6.ResumeLayout(False)
        Me.TabPage6.PerformLayout()
        Me.TabPage7.ResumeLayout(False)
        Me.TabPage7.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents FolderBrowserDialog1 As System.Windows.Forms.FolderBrowserDialog
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents ContextMenuStrip1 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents CopiarToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ContextMenuStrip2 As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents CopiarParametrosToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolTip1 As System.Windows.Forms.ToolTip
    Friend WithEvents LanzarArma3exeToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents FlatTabControl1 As ESUS_LAUNCHER_A3.FlatTabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents FlatLabel9 As ESUS_LAUNCHER_A3.FlatLabel
    Friend WithEvents FlatLabel8 As ESUS_LAUNCHER_A3.FlatLabel
    Friend WithEvents MaterialRaisedButton1 As MaterialSkin.Controls.MaterialRaisedButton
    Friend WithEvents CheckedListBox2 As System.Windows.Forms.CheckedListBox
    Friend WithEvents RichTextBox1 As System.Windows.Forms.RichTextBox
    Friend WithEvents RichTextBox2 As System.Windows.Forms.RichTextBox
    Friend WithEvents MaterialRaisedButton5 As MaterialSkin.Controls.MaterialRaisedButton
    Friend WithEvents MaterialRaisedButton4 As MaterialSkin.Controls.MaterialRaisedButton
    Friend WithEvents MaterialRaisedButton3 As MaterialSkin.Controls.MaterialRaisedButton
    Friend WithEvents MaterialRaisedButton2 As MaterialSkin.Controls.MaterialRaisedButton
    Friend WithEvents CheckedListBox1 As System.Windows.Forms.CheckedListBox
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents ListBox3 As System.Windows.Forms.ListBox
    Friend WithEvents MaterialRaisedButton9 As MaterialSkin.Controls.MaterialRaisedButton
    Friend WithEvents MaterialRaisedButton8 As MaterialSkin.Controls.MaterialRaisedButton
    Friend WithEvents MaterialRaisedButton7 As MaterialSkin.Controls.MaterialRaisedButton
    Friend WithEvents MaterialRaisedButton6 As MaterialSkin.Controls.MaterialRaisedButton
    Friend WithEvents ListBox1 As System.Windows.Forms.ListBox
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents Label12 As System.Windows.Forms.Label
    Friend WithEvents Label13 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents Button9 As System.Windows.Forms.Button
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents FlatLabel11 As ESUS_LAUNCHER_A3.FlatLabel
    Friend WithEvents Button6 As System.Windows.Forms.Button
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents PictureBox11 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox10 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox9 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox8 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox7 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox6 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox5 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox4 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents PictureBox2 As System.Windows.Forms.PictureBox
    Friend WithEvents FlatButton2 As ESUS_LAUNCHER_A3.FlatButton
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents Column1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column3 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column4 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column5 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column6 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column7 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column9 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Column8 As System.Windows.Forms.DataGridViewButtonColumn
    Friend WithEvents TabPage4 As System.Windows.Forms.TabPage
    Friend WithEvents TextBox1 As System.Windows.Forms.TextBox
    Friend WithEvents FlatLabel12 As ESUS_LAUNCHER_A3.FlatLabel
    Friend WithEvents TextBox6 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox10 As System.Windows.Forms.TextBox
    Friend WithEvents TextBox9 As System.Windows.Forms.TextBox
    Friend WithEvents FlatCheckBox19 As ESUS_LAUNCHER_A3.FlatCheckBox
    Friend WithEvents FlatLabel14 As ESUS_LAUNCHER_A3.FlatLabel
    Friend WithEvents FlatLabel13 As ESUS_LAUNCHER_A3.FlatLabel
    Friend WithEvents FlatCheckBox18 As ESUS_LAUNCHER_A3.FlatCheckBox
    Friend WithEvents FlatComboBox4 As ESUS_LAUNCHER_A3.FlatComboBox
    Friend WithEvents FlatProgressBar1 As ESUS_LAUNCHER_A3.FlatProgressBar
    Friend WithEvents FlatCheckBox17 As ESUS_LAUNCHER_A3.FlatCheckBox
    Friend WithEvents FlatCheckBox15 As ESUS_LAUNCHER_A3.FlatCheckBox
    Friend WithEvents FlatCheckBox14 As ESUS_LAUNCHER_A3.FlatCheckBox
    Friend WithEvents FlatCheckBox13 As ESUS_LAUNCHER_A3.FlatCheckBox
    Friend WithEvents FlatCheckBox12 As ESUS_LAUNCHER_A3.FlatCheckBox
    Friend WithEvents FlatCheckBox11 As ESUS_LAUNCHER_A3.FlatCheckBox
    Friend WithEvents FlatCheckBox10 As ESUS_LAUNCHER_A3.FlatCheckBox
    Friend WithEvents FlatCheckBox9 As ESUS_LAUNCHER_A3.FlatCheckBox
    Friend WithEvents FlatCheckBox8 As ESUS_LAUNCHER_A3.FlatCheckBox
    Friend WithEvents FlatCheckBox7 As ESUS_LAUNCHER_A3.FlatCheckBox
    Friend WithEvents FlatCheckBox6 As ESUS_LAUNCHER_A3.FlatCheckBox
    Friend WithEvents FlatCheckBox5 As ESUS_LAUNCHER_A3.FlatCheckBox
    Friend WithEvents FlatCheckBox4 As ESUS_LAUNCHER_A3.FlatCheckBox
    Friend WithEvents FlatCheckBox3 As ESUS_LAUNCHER_A3.FlatCheckBox
    Friend WithEvents FlatCheckBox2 As ESUS_LAUNCHER_A3.FlatCheckBox
    Friend WithEvents FlatCheckBox1 As ESUS_LAUNCHER_A3.FlatCheckBox
    Friend WithEvents FlatLabel6 As ESUS_LAUNCHER_A3.FlatLabel
    Friend WithEvents FlatTextBox1 As ESUS_LAUNCHER_A3.FlatTextBox
    Friend WithEvents FlatTextBox3 As ESUS_LAUNCHER_A3.FlatTextBox
    Friend WithEvents FlatTextBox4 As ESUS_LAUNCHER_A3.FlatTextBox
    Friend WithEvents FlatTrackBar4 As ESUS_LAUNCHER_A3.FlatTrackBar
    Friend WithEvents FlatTextBox2 As ESUS_LAUNCHER_A3.FlatTextBox
    Friend WithEvents FlatTrackBar3 As ESUS_LAUNCHER_A3.FlatTrackBar
    Friend WithEvents FlatTrackBar2 As ESUS_LAUNCHER_A3.FlatTrackBar
    Friend WithEvents FlatTrackBar1 As ESUS_LAUNCHER_A3.FlatTrackBar
    Friend WithEvents FlatButton1 As ESUS_LAUNCHER_A3.FlatButton
    Friend WithEvents FlatComboBox2 As ESUS_LAUNCHER_A3.FlatComboBox
    Friend WithEvents FlatTabControl2 As ESUS_LAUNCHER_A3.FlatTabControl
    Friend WithEvents TabPage5 As System.Windows.Forms.TabPage
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Button7 As System.Windows.Forms.Button
    Friend WithEvents ProgressBar2 As System.Windows.Forms.ProgressBar
    Friend WithEvents ListBox2 As System.Windows.Forms.ListBox
    Friend WithEvents FlatTextBox7 As ESUS_LAUNCHER_A3.FlatTextBox
    Friend WithEvents Button5 As System.Windows.Forms.Button
    Friend WithEvents Button4 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents TabPage6 As System.Windows.Forms.TabPage
    Friend WithEvents FlatCheckBox16 As ESUS_LAUNCHER_A3.FlatCheckBox
    Friend WithEvents FlatLabel5 As ESUS_LAUNCHER_A3.FlatLabel
    Friend WithEvents FlatLabel4 As ESUS_LAUNCHER_A3.FlatLabel
    Friend WithEvents FlatLabel2 As ESUS_LAUNCHER_A3.FlatLabel
    Friend WithEvents FlatTextBox8 As ESUS_LAUNCHER_A3.FlatTextBox
    Friend WithEvents FlatButton4 As ESUS_LAUNCHER_A3.FlatButton
    Friend WithEvents FlatLabel1 As ESUS_LAUNCHER_A3.FlatLabel
    Friend WithEvents FlatButton3 As ESUS_LAUNCHER_A3.FlatButton
    Friend WithEvents FlatTextBox5 As ESUS_LAUNCHER_A3.FlatTextBox
    Friend WithEvents TabPage7 As System.Windows.Forms.TabPage
    Friend WithEvents FlatLabel7 As ESUS_LAUNCHER_A3.FlatLabel
    Friend WithEvents FlatButton5 As ESUS_LAUNCHER_A3.FlatButton
    Friend WithEvents FlatButton6 As ESUS_LAUNCHER_A3.FlatButton
    Friend WithEvents CheckedListBox3 As System.Windows.Forms.CheckedListBox
    Friend WithEvents FlatComboBox1 As ESUS_LAUNCHER_A3.FlatComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents MaterialRaisedButton10 As MaterialSkin.Controls.MaterialRaisedButton
    Friend WithEvents MaterialRaisedButton11 As MaterialSkin.Controls.MaterialRaisedButton
    Friend WithEvents FlatToggle1 As ESUS_LAUNCHER_A3.FlatToggle
    Friend WithEvents FlatComboBox3 As ESUS_LAUNCHER_A3.FlatComboBox
    Friend WithEvents FlatLabel10 As ESUS_LAUNCHER_A3.FlatLabel
    Friend WithEvents BottomToolStripPanel As System.Windows.Forms.ToolStripPanel
    Friend WithEvents TopToolStripPanel As System.Windows.Forms.ToolStripPanel
    Friend WithEvents RightToolStripPanel As System.Windows.Forms.ToolStripPanel
    Friend WithEvents LeftToolStripPanel As System.Windows.Forms.ToolStripPanel
    Friend WithEvents ContentPanel As System.Windows.Forms.ToolStripContentPanel
    Friend WithEvents FlatGroupBox1 As ESUS_LAUNCHER_A3.FlatGroupBox
    Friend WithEvents FlatTextBox9 As ESUS_LAUNCHER_A3.FlatTextBox
    Friend WithEvents FlatLabel16 As ESUS_LAUNCHER_A3.FlatLabel
    Friend WithEvents FlatTextBox6 As ESUS_LAUNCHER_A3.FlatTextBox
    Friend WithEvents FlatLabel15 As ESUS_LAUNCHER_A3.FlatLabel
    Friend WithEvents FlatLabel17 As ESUS_LAUNCHER_A3.FlatLabel
    Friend WithEvents CheckBox1 As System.Windows.Forms.CheckBox
    Friend WithEvents WebBrowser1 As System.Windows.Forms.WebBrowser
    Friend WithEvents FlatButton7 As ESUS_LAUNCHER_A3.FlatButton
    Friend WithEvents Button8 As Button
End Class
