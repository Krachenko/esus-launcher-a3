﻿Imports System.Threading
Imports System.IO
Imports Newtonsoft.Json
Imports System.Net
Imports Newtonsoft.Json.Linq
Imports MaterialSkin
Imports System.Environment
Imports System.Xml
Imports Microsoft.VisualBasic.FileIO
Imports System.Collections.Specialized
Imports System.Text
Imports System.IO.Path
Imports Microsoft.VisualBasic.Devices
Imports System.Management
Imports Microsoft.Win32
Imports System.Net.Sockets
Imports System
Imports System.IO.Compression
Imports System.Globalization
Imports System.Windows.Forms
Public Class Form1
    Dim mods As String = Nothing
    Dim directorio As String
    Dim custom As String
    Dim pantalla As String
    Dim ventana As String
    Dim isChecked As Boolean
    Dim window As String
    Dim splash As String
    Dim world As String
    Dim mundovacio As String
    Dim nopause As String
    Dim solopbo As String
    Dim ningunapausa As String
    Dim malloc As String
    Dim mostrarerrores As String
    Dim showscripterror As String
    Dim norpt As String
    Dim nologs As String
    Dim cpus As Integer
    Dim gpu As String
    Dim ram As String
    Dim cpucount As String
    Dim carpetadll As String
    Dim hilosvirtuales As String
    Dim hyperthreading As String
    Dim extrahilos As String
    Dim dias As String
    Dim lunes As String
    Dim martes As String
    Dim miercoles As String
    Dim jueves As String
    Dim viernes As String
    Dim sabado As String
    Dim domingo As String
    Dim seleccion As String
    Dim carpetaa3 As String
    Dim StringCol As New Specialized.StringCollection
    Dim File As File
    Dim pordefecto As String = "Por Defecto.txt"
    Dim pordefectoprioridad As String = "Por Defecto.prioridad.dat"
    Dim perfilesusprioridad As String = "perfilesus(noborrar).prioridad.dat"
    Dim perfilesus As String = "perfilesus(noborrar).dat"
    Dim saltarintro As String
    Dim dx9 As String
    Dim arma3 As String
    Dim parametros As String
    Dim config As String = "configuracion.cfg"
    Dim arma3exe As String
    Dim steam As String
    Dim favorito As String = "nada"
    Dim rutacortada As String = Nothing
    Dim externos As String = "pexternos.cfg"
    Dim externoselegidos As String = "pexternoselegidos.cfg"
    Dim wc As System.Net.WebClient
    Dim rc As System.Net.WebClient


    Public Property TextAlign As HorizontalAlignment
    Private programDataFilePath As String = Combine(GetFolderPath(SpecialFolder.ApplicationData),
                                                My.Application.Info.AssemblyName)
    Private perfilpordefecto As String = Combine(programDataFilePath, pordefecto)
    Private perfilpordefectoprioridad As String = Combine(programDataFilePath, pordefectoprioridad)
    Private perfilesuson As String = Combine(programDataFilePath, perfilesus)
    Private perfilesusprioridadon As String = Combine(programDataFilePath, perfilesusprioridad)
    Private configuracion As String = Combine(programDataFilePath, config)
    Private programasexternos As String = Combine(programDataFilePath, externos)
    Private programasexternoselegidos As String = Combine(programDataFilePath, externoselegidos)
    Private Sub TabPage3_Enter(sender As System.Object, e As System.EventArgs) Handles TabPage3.Enter
        WebBrowser1.Refresh()
        WebBrowser1.Refresh(WebBrowserRefreshOption.Completely)
    End Sub


    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Dim SkinManager As MaterialSkinManager = MaterialSkinManager.Instance
        SkinManager.AddFormToManage(Me)
        SkinManager.Theme = MaterialSkinManager.Themes.LIGHT
        SkinManager.ColorScheme = New ColorScheme(Primary.BlueGrey800, Primary.BlueGrey900, Primary.BlueGrey500, Accent.LightBlue200, TextShade.WHITE)
        Me.SetStyle(ControlStyles.OptimizedDoubleBuffer, True)
        If Not My.Computer.FileSystem.DirectoryExists(programDataFilePath) Then
            My.Computer.FileSystem.CreateDirectory(programDataFilePath)
        End If
        Dim fs As FileStream = Nothing
        If (Not File.Exists(perfilpordefecto)) Then
            fs = File.Create(perfilpordefecto)
            fs = File.Create(perfilpordefectoprioridad)
            fs = File.Create(perfilesuson)
            fs = File.Create(perfilesusprioridadon)
            Using fs
            End Using
        End If
        If File.Exists(configuracion) Then
            Call leerconfig()
            If FlatCheckBox16.Checked = True Then
                arma3exe = FlatTextBox5.Text
                arma3 = FlatTextBox8.Text
                FlatButton3.Enabled = True
                FlatButton4.Enabled = True
                Try
                    steam = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Valve\Steam", "InstallPath", Nothing)
                Catch ex As Exception

                End Try
                steam = steam & "\" & "Steam.exe"
                If (Not File.Exists(steam)) Then
                    Try
                        steam = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\Valve\Steam", "SteamExe", Nothing)
                    Catch ex As Exception
                    End Try
                End If
            Else
                FlatButton3.Enabled = False
                FlatButton4.Enabled = False
                Try
                    arma3 = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\bohemia interactive\arma 3", "main", Nothing)
                    steam = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Valve\Steam", "InstallPath", Nothing)
                    steam = steam & "\" & "Steam.exe"
                    arma3exe = arma3 & "\" & "arma3.exe"
                Catch ex As Exception
                End Try
                If IsNothing(arma3) Then
                    Try
                        steam = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\Valve\Steam", "SteamPath", Nothing)
                        steam = steam.Replace("/", "\")
                        arma3 = steam & "\steamapps\common\Arma 3"
                        arma3exe = arma3 + "\arma3.exe"

                    Catch ex As Exception
                    End Try
                    If (Not File.Exists(steam)) Then
                        Try
                            steam = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\Valve\Steam", "SteamExe", Nothing)
                        Catch ex As Exception

                        End Try
                    End If
                Else
                End If
            End If
        Else
            Dim ar As FileStream = Nothing
            ar = File.Create(configuracion)
            ar = File.Create(programasexternos)
            ar = File.Create(programasexternoselegidos)
            Using ar
            End Using
            Try
                arma3 = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\bohemia interactive\arma 3", "main", Nothing)
                steam = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\Valve\Steam", "InstallPath", Nothing)
                steam = steam & "\" & "Steam.exe"
                arma3exe = arma3 & "\" & "arma3.exe"
            Catch ex As Exception
            End Try
            If IsNothing(arma3) Then
                Try
                    steam = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\Valve\Steam", "SteamPath", Nothing)
                    steam = steam.Replace("/", "\")
                    arma3 = steam & "\steamapps\common\Arma 3"
                    arma3exe = arma3 & "\arma3.exe"
                Catch ex As Exception
                End Try
                If (Not File.Exists(steam)) Then
                    Try
                        steam = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\Valve\Steam", "SteamExe", Nothing)
                    Catch ex As Exception
                    End Try
                End If
            Else
            End If
        End If
        Dim leerdatos As Thread
        Dim actualizadorhilo As Thread
        Dim clienside As Thread
        Dim datostwitch As Thread
        actualizadorhilo = New Thread(AddressOf actualizacion)
        leerdatos = New Thread(AddressOf llenardatos)
        clienside = New Thread(AddressOf leerclientside)
        datostwitch = New Thread(AddressOf twitch)
        leerdatos.Start()
        actualizadorhilo.Start()
        clienside.Start()
        FlatLabel2.Text = arma3
        FlatLabel1.Text = arma3exe
        ListBox2.Items.Clear()
        Try
            For Each File As String In My.Computer.FileSystem.GetFiles _
     (programDataFilePath, FileIO.SearchOption.SearchTopLevelOnly, "*.txt")
                ListBox2.Items.Add(System.IO.Path.GetFileNameWithoutExtension(File))
            Next
        Catch ex As Exception
        End Try
        Try
            For Each File As String In My.Computer.FileSystem.GetFiles _
     (programDataFilePath, FileIO.SearchOption.SearchTopLevelOnly, "*.txt")
                FlatComboBox1.Items.Add(System.IO.Path.GetFileNameWithoutExtension(File))
            Next
        Catch ex As Exception
        End Try
        FlatComboBox1.SelectedItem = "Por Defecto"
        If (Not File.Exists(configuracion)) Then
            FlatComboBox1.SelectedItem = "Por Defecto"
        Else
            Try
                Dim veinticuatro As String = System.IO.File.ReadAllLines(configuracion)(23)
                If veinticuatro.Contains("nada") Then
                    FlatComboBox1.SelectedItem = "Por Defecto"
                Else
                    FlatComboBox1.SelectedItem = veinticuatro
                End If
            Catch ex As Exception
            End Try
        End If
        Call leermods()
        datostwitch.Start()
        FlatTextBox9.Text = CheckedListBox1.Items.Count
        FlatTextBox6.Text = CheckedListBox1.CheckedItems.Count

    End Sub
    Sub actualizacion()
        Try
            Dim request As System.Net.HttpWebRequest = System.Net.HttpWebRequest.Create("https://www.dropbox.com/s/bxneubjb6sq5gbz/version.txt?raw=1")
            Dim response As System.Net.HttpWebResponse = request.GetResponse()
            Dim sr As System.IO.StreamReader = New System.IO.StreamReader(response.GetResponseStream())
            Dim newestversion As String = sr.ReadToEnd()
            Dim currentversion = "29"
            If newestversion.Contains(currentversion) Then
            Else
                MessageBox.Show("Hay una nueva actualización disponible de Esus Launcher!! REV. " + newestversion + vbNewLine + "Comenzará su descarga automáticamente...",
                                      "NUEVA ACTUALIZACIÓN!",
                                      MessageBoxButtons.OK,
                                      MessageBoxIcon.Information,
                                      MessageBoxDefaultButton.Button1)
                Dim path As String
                path = My.Application.Info.DirectoryPath
                Dim actualizador As String
                actualizador = path + "\Actualizador.exe"
                If File.Exists(actualizador) Then
                    Dim process As System.Diagnostics.Process = Nothing
                    Dim processStartInfo As System.Diagnostics.ProcessStartInfo
                    processStartInfo = New System.Diagnostics.ProcessStartInfo()
                    processStartInfo.FileName = actualizador
                    If System.Environment.OSVersion.Version.Major >= 6 Then ' Windows Vista or higher
                        processStartInfo.Verb = "runas"
                    Else
                        ' No need to prompt to run as admin
                    End If
                    processStartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Normal
                    processStartInfo.UseShellExecute = True
                    Try
                        process = System.Diagnostics.Process.Start(processStartInfo)
                    Catch ex As Exception

                    Finally
                        If Not (process Is Nothing) Then
                            process.Dispose()
                        End If
                    End Try
                    Application.Exit()
                Else
                    rc = New System.Net.WebClient()
                    AddHandler rc.DownloadProgressChanged, AddressOf encambiodeprogreso
                    AddHandler rc.DownloadFileCompleted, AddressOf endescargacompletada
                    rc.DownloadFileAsync(New Uri("https://www.dropbox.com/s/xvllvdbwtt0946n/Actualizador.exe?raw=1"), actualizador)
                    System.Threading.Thread.Sleep(5000)
                    Dim process As System.Diagnostics.Process = Nothing
                    Dim processStartInfo As System.Diagnostics.ProcessStartInfo
                    processStartInfo = New System.Diagnostics.ProcessStartInfo()
                    processStartInfo.FileName = actualizador
                    If System.Environment.OSVersion.Version.Major >= 6 Then ' Windows Vista or higher
                        processStartInfo.Verb = "runas"
                    Else
                        ' No need to prompt to run as admin
                    End If
                    processStartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Normal
                    processStartInfo.UseShellExecute = True
                    Try
                        process = System.Diagnostics.Process.Start(processStartInfo)
                    Catch ex As Exception

                    Finally
                        If Not (process Is Nothing) Then
                            process.Dispose()
                        End If
                    End Try
                    Application.Exit()
                End If
            End If
        Catch ex As Exception

        End Try

    End Sub


    Private Sub encambiodeprogreso(ByVal sender As Object, ByVal e As System.Net.DownloadProgressChangedEventArgs)
        Dim totalSize As Long = e.TotalBytesToReceive
        Dim downloadedBytes As Long = e.BytesReceived
        Dim percentage As Integer = e.ProgressPercentage
    End Sub
    Private Sub endescargacompletada(ByVal sender As Object, ByVal e As System.ComponentModel.AsyncCompletedEventArgs)
        If e.Cancelled Then
        ElseIf Not e.Error Is Nothing Then
        Else

        End If
    End Sub
    Enum InfoTypes
        OperatingSystemName
        ProcessorName
        AmountOfMemory
        VideocardName
        VideocardMem
    End Enum

    Public Function GetInfo(ByVal InfoType As InfoTypes) As String
        Dim Info As New ComputerInfo
        Dim Value As String = ""
        Dim vganame As String = ""
        Dim vgamem As String = ""
        Dim proc As String = ""
        Dim searcher As New Management.ManagementObjectSearcher(
            "root\CIMV2", "SELECT * FROM Win32_VideoController")
        Dim searcher1 As New Management.ManagementObjectSearcher(
            "SELECT * FROM Win32_Processor")
        If InfoType = InfoTypes.VideocardMem Then
            For Each queryObject As ManagementObject In searcher.Get
                vgamem = queryObject.GetPropertyValue("AdapterRAM").ToString
            Next
            Value = Math.Round((((CDbl(Convert.ToDouble(Val(vgamem))) / 1024)) / 1024), 2)
        End If
        Return Value
    End Function
    Sub leerconfig()
        CheckedListBox3.Items.Clear()
        CheckedListBox3.BeginUpdate()
        If IO.File.Exists(programasexternos) Then
            Dim r As New IO.StreamReader(programasexternos)
            Dim txt As New TextBox
            txt.Text = r.ReadToEnd
            CheckedListBox3.Items.Clear()
            For i As Integer = 0 To txt.Lines.Count - 2
                CheckedListBox3.Items.Add(txt.Lines(i))
            Next
            r.Close()
        End If
        If My.Computer.FileSystem.FileExists(programasexternoselegidos) Then
            Using rdr As New System.IO.StreamReader(programasexternoselegidos)
                Do While rdr.Peek() >= 0
                    Dim itm As String = rdr.ReadLine
                    itm = itm.Trim
                    If itm = Nothing Then
                    Else
                        For i As Integer = 0 To CheckedListBox3.Items.Count - 1
                            If CheckedListBox3.Items(i).ToString = itm Then
                                CheckedListBox3.SetItemCheckState(i, CheckState.Checked)
                                Exit For
                            End If
                        Next
                    End If
                Loop
            End Using
        Else
        End If

        CheckedListBox3.EndUpdate()
        Try
            Dim uno As String = System.IO.File.ReadAllLines(configuracion)(0)
            Dim dos As String = System.IO.File.ReadAllLines(configuracion)(1)
            Dim tres As String = System.IO.File.ReadAllLines(configuracion)(2)
            Dim cuatro As String = System.IO.File.ReadAllLines(configuracion)(3)
            Dim cinco As String = System.IO.File.ReadAllLines(configuracion)(4)
            Dim seis As String = System.IO.File.ReadAllLines(configuracion)(5)
            Dim siete As String = System.IO.File.ReadAllLines(configuracion)(6)
            Dim ocho As String = System.IO.File.ReadAllLines(configuracion)(7)
            Dim nueve As String = System.IO.File.ReadAllLines(configuracion)(8)
            Dim diez As String = System.IO.File.ReadAllLines(configuracion)(9)
            Dim once As String = System.IO.File.ReadAllLines(configuracion)(10)
            Dim doce As String = System.IO.File.ReadAllLines(configuracion)(11)
            Dim trece As String = System.IO.File.ReadAllLines(configuracion)(12)
            Dim catorce As String = System.IO.File.ReadAllLines(configuracion)(13)
            Dim quince As String = System.IO.File.ReadAllLines(configuracion)(14)
            Dim dieciseis As String = System.IO.File.ReadAllLines(configuracion)(15)
            Dim diecisiete As String = System.IO.File.ReadAllLines(configuracion)(16)
            Dim dieciocho As String = System.IO.File.ReadAllLines(configuracion)(17)
            Dim diecinueve As String = System.IO.File.ReadAllLines(configuracion)(18)
            Dim veinte As String = System.IO.File.ReadAllLines(configuracion)(19)
            Dim veintiuno As String = System.IO.File.ReadAllLines(configuracion)(20)
            Dim veintidos As String = System.IO.File.ReadAllLines(configuracion)(21)
            Dim veintitres As String = System.IO.File.ReadAllLines(configuracion)(22)
            Dim veinticinco As String = System.IO.File.ReadAllLines(configuracion)(24)
            Dim veintiseis As String = System.IO.File.ReadAllLines(configuracion)(25)
            Dim veintisiete As String = System.IO.File.ReadAllLines(configuracion)(26)
            Dim veintiocho As String = System.IO.File.ReadAllLines(configuracion)(27)
            Dim veintinueve As String = System.IO.File.ReadAllLines(configuracion)(29)
            Dim treinta As String = System.IO.File.ReadAllLines(configuracion)(30)
            Dim treintayuno As String = System.IO.File.ReadAllLines(configuracion)(31)
            Dim treintaydos As String = System.IO.File.ReadAllLines(configuracion)(32)
            Dim treintaytres As String = System.IO.File.ReadAllLines(configuracion)(33)
            Dim treintaycuatro As String = System.IO.File.ReadAllLines(configuracion)(34)
            Dim treintaycinco As String = System.IO.File.ReadAllLines(configuracion)(35)
            Dim treintayseis As String = System.IO.File.ReadAllLines(configuracion)(36)
            Dim treintaysiete As String = System.IO.File.ReadAllLines(configuracion)(37)

            If uno.Contains("0") Then
                FlatCheckBox1.Checked = False
            Else
                FlatCheckBox1.Checked = True
            End If
            If dos.Contains("0") Then
                FlatCheckBox2.Checked = False
            Else
                FlatCheckBox2.Checked = True
            End If
            If tres.Contains("0") Then
                FlatCheckBox3.Checked = False
            Else
                FlatCheckBox3.Checked = True
            End If
            If cuatro.Contains("0") Then
                FlatCheckBox4.Checked = False
            Else
                FlatCheckBox4.Checked = True
            End If
            If cinco.Contains("0") Then
                FlatCheckBox5.Checked = False
            Else
                FlatCheckBox5.Checked = True
            End If
            If seis.Contains("0") Then
                FlatCheckBox6.Checked = False
            Else
                FlatCheckBox6.Checked = True
            End If
            If siete.Contains("0") Then
                FlatCheckBox7.Checked = False
            Else
                FlatCheckBox7.Checked = True
            End If
            If ocho.Contains("0") Then
                FlatCheckBox8.Checked = False
            Else
                FlatCheckBox8.Checked = True
            End If
            If nueve.Contains("0") Then
                FlatCheckBox9.Checked = False
            Else
                FlatCheckBox9.Checked = True
            End If
            If diez.Contains("0") Then
                FlatCheckBox10.Checked = False
            Else
                FlatCheckBox10.Checked = True
            End If
            If once.Contains("0") Then
                FlatCheckBox11.Checked = False
            Else
                FlatCheckBox11.Checked = True
            End If
            If doce.Contains("0") Then
                FlatCheckBox12.Checked = False
            Else
                FlatCheckBox12.Checked = True
            End If
            If trece.Contains("0") Then
                FlatCheckBox13.Checked = False
            Else
                FlatCheckBox13.Checked = True
            End If
            If catorce.Contains("0") Then
                FlatCheckBox14.Checked = False
            Else
                FlatCheckBox14.Checked = True
            End If
            If quince.Contains("0") Then
                FlatCheckBox15.Checked = False
            Else
                FlatCheckBox15.Checked = True
            End If
            If dieciseis.Contains("nada") Then
                FlatTextBox1.Text = Nothing
                FlatTrackBar1.Value = Nothing
            Else
                FlatTextBox1.Text = dieciseis
                FlatTrackBar1.Value = dieciseis
            End If
            If diecisiete.Contains("nada") Then
                FlatTextBox3.Text = Nothing
                FlatTrackBar3.Value = Nothing
            Else
                FlatTrackBar3.Value = diecisiete
                FlatTextBox3.Text = diecisiete
            End If
            If dieciocho.Contains("nada") Then
                FlatTextBox2.Text = Nothing
                FlatTrackBar2.Value = Nothing
            Else
                FlatTrackBar2.Value = dieciocho
                FlatTextBox2.Text = dieciocho
            End If
            If diecinueve.Contains("nada") Then
                FlatTextBox4.Text = Nothing
                FlatTrackBar4.Value = Nothing
            Else
                FlatTextBox4.Text = diecinueve
                FlatTrackBar4.Value = diecinueve
            End If
            If veinte.Contains("nada") Then
                TextBox6.Text = Nothing
            Else
                TextBox6.Text = veinte
            End If

            If veintiuno.Contains("0") Then
                FlatCheckBox16.Checked = False
                FlatLabel2.Text = arma3
                FlatLabel1.Text = arma3exe
                FlatTextBox5.Enabled = False
                FlatTextBox5.Text = Nothing
                FlatTextBox8.Enabled = False
                FlatTextBox8.Text = Nothing
            Else
                FlatCheckBox16.Checked = True
                FlatTextBox5.Enabled = True
                FlatTextBox5.Text = veintidos
                FlatTextBox8.Enabled = True
                FlatTextBox8.Text = veintitres
            End If
            If veinticinco.Contains("") Then
            Else
                CheckedListBox3.Items.Add(veinticinco)
            End If
            If veintiseis.Contains("") Then
            Else
                CheckedListBox3.Items.Add(veintiseis)
            End If
            If veintisiete.Contains("") Then
            Else
                CheckedListBox3.Items.Add(veintisiete)
            End If
            If veintiocho.Contains("") Then
            Else
                CheckedListBox3.Items.Add(veintiocho)
            End If
            If veintinueve = "nada" Then
                FlatComboBox2.SelectedIndex = 0
            ElseIf veintinueve = "cerrar" Then
                FlatComboBox2.SelectedIndex = 1
            Else : veintinueve = "minimizar"
                FlatComboBox2.SelectedIndex = 2
            End If
            If FlatCheckBox11.Checked = True Then
                TextBox6.Visible = True
            Else
                TextBox6.Visible = False
            End If
            If FlatCheckBox12.Checked = True Then
                FlatTrackBar1.Visible = True
                FlatTextBox1.Visible = True
            Else
                FlatTrackBar1.Visible = False
                FlatTextBox1.Visible = False
            End If
            If FlatCheckBox13.Checked = True Then
                FlatTrackBar3.Visible = True
                FlatTextBox3.Visible = True
            Else
                FlatTrackBar3.Visible = False
                FlatTextBox3.Visible = False
            End If
            If FlatCheckBox14.Checked = True Then
                FlatTrackBar2.Visible = True
                FlatTextBox2.Visible = True
            Else
                FlatTrackBar2.Visible = False
                FlatTextBox2.Visible = False
            End If
            If FlatCheckBox15.Checked = True Then
                FlatTrackBar4.Visible = True
                FlatTextBox4.Visible = True
            Else
                FlatTrackBar4.Visible = False
                FlatTextBox4.Visible = False
            End If
            If treinta.Contains("0") Then
                FlatCheckBox17.Checked = False
            Else
                FlatCheckBox17.Checked = True
            End If
            If treintayuno.Contains("0") Then
                FlatCheckBox18.Checked = False
            Else
                FlatCheckBox18.Checked = True
            End If
            If FlatCheckBox18.Checked = True Then
                TextBox9.Visible = True
                TextBox10.Visible = True
                FlatLabel14.Visible = True
                FlatLabel13.Visible = True
                TextBox1.Visible = True
                FlatLabel12.Visible = True
                CheckBox1.Visible = True
            Else
                TextBox9.Visible = False
                TextBox10.Visible = False
                FlatLabel14.Visible = False
                FlatLabel13.Visible = False
                TextBox1.Visible = False
                FlatLabel12.Visible = False
                CheckBox1.Visible = False
            End If
            If treintaydos.Contains("0") Then
                FlatCheckBox19.Checked = False
            Else
                FlatCheckBox19.Checked = True
            End If
            If FlatCheckBox19.Checked = True Then
                FlatComboBox4.Visible = True
                FlatComboBox4.Items.Clear()
                Try
                    Dim dirInfo As New DirectoryInfo(My.Computer.FileSystem.SpecialDirectories.MyDocuments + "\Arma 3 - Other Profiles")
                    For Each f In dirInfo.GetDirectories()
                        For Each fl In f.GetFiles()
                            If fl.Name.EndsWith(".Arma3Profile") Then
                                FlatComboBox4.Items.Add(Uri.UnescapeDataString(f.Name))
                                Exit For
                            End If
                        Next
                    Next
                Catch ex As Exception
                End Try
            Else
                FlatComboBox4.Visible = False
            End If
            If treintaytres.Contains("nada") Then
                FlatComboBox4.SelectedItem = Nothing
            Else
                Try
                    FlatComboBox4.SelectedItem = treintaytres
                Catch ex As Exception
                End Try
            End If
            If treintaycuatro.Contains("nada") Then
                TextBox9.Text = Nothing
            Else
                TextBox9.Text = treintaycuatro
            End If
            If treintaycinco.Contains("nada") Then
                TextBox10.Text = Nothing
            Else
                TextBox10.Text = treintaycinco
            End If
            If treintayseis.Contains("nada") Then
                TextBox1.Text = Nothing
            Else
                TextBox1.Text = treintayseis
            End If
            If treintaysiete.Contains("0") Then
                CheckBox1.Checked = False
            Else
                CheckBox1.Checked = True
            End If

        Catch ex As Exception

        End Try

    End Sub

    Sub leermods()
        Try
            CheckedListBox1.Items.Clear()
            For Each folder In System.IO.Directory.GetDirectories(arma3, "@*")
                CheckedListBox1.Items.Add(Path.GetFileName(folder))
                For Each folder2 In System.IO.Directory.GetDirectories(folder, "@*")
                    CheckedListBox1.Items.Add(Path.GetFileName(folder) + "/" + Path.GetFileName(folder2))
                    CheckedListBox1.Items.Remove(Path.GetFileName(folder))
                Next
            Next
            Dim rutamods As String = Combine(programDataFilePath, FlatComboBox1.SelectedItem.ToString & ".txt")
            If My.Computer.FileSystem.FileExists(rutamods) Then
                Using rdr As New System.IO.StreamReader(rutamods)
                    Do While rdr.Peek() >= 0
                        Dim itm As String = rdr.ReadLine
                        itm = itm.Trim
                        If itm = Nothing Then
                        Else
                            For i As Integer = 0 To CheckedListBox1.Items.Count - 1
                                If CheckedListBox1.Items(i).ToString = itm Then
                                    CheckedListBox1.SetItemCheckState(i, CheckState.Checked)
                                    Exit For
                                End If
                            Next
                        End If
                    Loop
                End Using
            Else
            End If
            Dim prioridadmods As String = Combine(programDataFilePath, FlatComboBox1.SelectedItem.ToString & ".prioridad.dat")
            If IO.File.Exists(prioridadmods) Then
                Dim r As New IO.StreamReader(prioridadmods)
                Dim txt As New TextBox
                txt.Text = r.ReadToEnd
                ListBox1.Items.Clear()
                For i As Integer = 0 To txt.Lines.Count - 2
                    If CheckedListBox1.Items.Contains(txt.Lines(i)) = True Then
                        ListBox1.Items.Add(txt.Lines(i))
                    End If
                Next
                r.Close()
            End If
        Catch ex As Exception
        End Try
    End Sub

    Sub leerclientside()
        Try
            Try
                CheckedListBox2.Items.Clear()
                ListBox3.Items.Clear()
                Dim instance As WebClient = New WebClient
                Dim address As String = "https://www.dropbox.com/s/ksfamn2n6hhosep/clientside.txt?raw=1"
                RichTextBox1.Text = instance.DownloadString(address)
                Dim rutaaddons As String
                For Each line In RichTextBox1.Lines
                    rutaaddons = arma3 & "\" & line
                    If My.Computer.FileSystem.DirectoryExists(rutaaddons) Then
                        CheckedListBox2.Items.Add(line)
                    Else
                    End If
                Next
            Catch ex As Exception
            End Try
            If My.Computer.FileSystem.FileExists(perfilesuson) Then
                Using rdr As New System.IO.StreamReader(perfilesuson)
                    Do While rdr.Peek() >= 0
                        Dim itm As String = rdr.ReadLine
                        itm = itm.Trim
                        If itm = Nothing Then
                        Else
                            For i As Integer = 0 To CheckedListBox2.Items.Count - 1
                                If CheckedListBox2.Items(i).ToString = itm Then
                                    CheckedListBox2.SetItemCheckState(i, CheckState.Checked)
                                    Exit For
                                End If
                            Next
                        End If
                    Loop
                End Using
            Else
            End If
            ListBox3.Items.Clear()
            If CheckedListBox2.CheckedItems.Count > 0 Then
                For i As Integer = 0 To CheckedListBox2.CheckedItems.Count - 1
                    ListBox3.Items.Add(CheckedListBox2.CheckedItems(i))
                Next
            End If
            If IO.File.Exists(perfilesusprioridadon) Then
                Dim r As New IO.StreamReader(perfilesusprioridadon)
                Dim txt As New TextBox
                txt.Text = r.ReadToEnd
                ListBox3.Items.Clear()
                For i As Integer = 0 To txt.Lines.Count - 2
                    If CheckedListBox2.Items.Contains(txt.Lines(i)) Then
                        ListBox3.Items.Add(txt.Lines(i))
                    End If
                Next
                r.Close()
            End If
        Catch ex As Exception
        End Try
    End Sub


    Sub construirparametro()
        Dim rutascambiadas As String = Nothing
        Dim builder As New StringBuilder()
        Dim construir As New StringBuilder()
        Dim constructor As New StringBuilder()
        parametros = "-applaunch 107410"
        If FlatCheckBox17.Checked = True Then
            parametros = parametros & " -usebe -nolauncher "
        Else
            parametros = parametros & " -nolauncher "
        End If
        If FlatToggle1.Checked = True Then
            If FlatCheckBox16.Checked = True Then
                If FlatTextBox5.Text <> "" Then
                    rutacortada = FlatTextBox5.Text
                    If rutacortada.Substring(0, rutacortada.Length - 10) = FlatTextBox8.Text Then
                        If RichTextBox2.Text <> "" Then
                            For Each line As Object In RichTextBox2.Lines
                                line = line + ";"
                                constructor.Append(line.ToString())
                            Next
                            parametros = parametros & "-mod=" & constructor.ToString()
                            If ListBox3.Items.Count = 0 Then
                            Else
                                For Each i As Object In ListBox3.Items
                                    i = i + ";"
                                    construir.Append(i.ToString())
                                Next
                                parametros = parametros & construir.ToString
                            End If
                        Else
                            parametros = parametros & "-mod="
                        End If
                    Else
                        If RichTextBox2.Text <> "" Then
                            For Each line As Object In RichTextBox2.Lines
                                line = FlatTextBox8.Text + "\" + line + ";"
                                constructor.Append(line.ToString())
                            Next
                            parametros = parametros & "-mod=" & constructor.ToString()
                            If ListBox3.Items.Count = 0 Then
                            Else
                                For Each i As Object In ListBox3.Items
                                    i = FlatTextBox8.Text + "\" + i + ";"
                                    construir.Append(i.ToString())
                                Next
                                parametros = parametros & construir.ToString
                            End If
                        Else
                            parametros = parametros & "-mod="
                        End If
                    End If
                End If
            Else
                If RichTextBox2.Text <> "" Then
                    For Each line As Object In RichTextBox2.Lines
                        line = line + ";"
                        constructor.Append(line.ToString())
                    Next
                    parametros = parametros & "-mod=" & constructor.ToString()
                    If ListBox3.Items.Count = 0 Then
                    Else
                        For Each i As Object In ListBox3.Items
                            i = i + ";"
                            construir.Append(i.ToString())
                        Next
                        parametros = parametros & construir.ToString
                    End If
                Else
                    parametros = parametros & "-mod="
                End If
            End If

        Else
            If FlatCheckBox16.Checked = True Then
                rutacortada = FlatTextBox5.Text
                If rutacortada.Substring(0, rutacortada.Length - 10) = FlatTextBox8.Text Then
                    If ListBox1.Items.Count = 0 Then
                        parametros = parametros & "-mod="
                    Else
                        For Each i As Object In ListBox1.Items
                            i = i + ";"
                            builder.Append(i.ToString())
                        Next
                        parametros = parametros & "-mod=" & builder.ToString
                    End If
                Else
                    If ListBox1.Items.Count = 0 Then
                        parametros = parametros & "-mod="

                    Else
                        For Each i As Object In ListBox1.Items
                            i = FlatTextBox8.Text + "\" + i + ";"
                            builder.Append(i.ToString())
                        Next
                        parametros = parametros & "-mod=" & builder.ToString
                    End If
                End If
            Else
                If ListBox1.Items.Count = 0 Then
                    parametros = parametros & "-mod="

                Else
                    For Each i As Object In ListBox1.Items
                        i = i + ";"
                        builder.Append(i.ToString())
                    Next
                    parametros = parametros & "-mod=" & builder.ToString
                End If
            End If
        End If
        If FlatCheckBox1.Checked = False Then
        Else
            parametros = parametros & " -nosplash"
        End If
        If FlatCheckBox2.Checked = False Then
        Else
            parametros = parametros & " -skipIntro"
        End If
        If FlatCheckBox3.Checked = False Then
        Else
            parametros = parametros & " -world=empty"
        End If
        If FlatCheckBox4.Checked = False Then
        Else
            parametros = parametros & " -noPause"
        End If
        If FlatCheckBox5.Checked = False Then
        Else
            parametros = parametros & " -noLogs"
        End If
        If FlatCheckBox6.Checked = False Then
        Else
            parametros = parametros & " -window"
        End If
        If FlatCheckBox7.Checked = False Then
        Else
            parametros = parametros & " -showScriptErrors"
        End If
        If FlatCheckBox8.Checked = False Then
        Else
            parametros = parametros & " -filePatching"
        End If
        If FlatCheckBox9.Checked = False Then
        Else
            parametros = parametros & " -winxp"
        End If
        If FlatCheckBox10.Checked = False Then
        Else
            parametros = parametros & " -enableHT"
        End If
        If FlatCheckBox11.Checked = False Then
        Else
            parametros = parametros & " " & TextBox6.Text & ""
        End If
        If FlatCheckBox12.Checked = False Then
        Else
            parametros = parametros & " -cpuCount=" & FlatTextBox1.Text
        End If
        If FlatCheckBox13.Checked = False Then
        Else
            parametros = parametros & " -maxMem=" & FlatTextBox3.Text
        End If
        If FlatCheckBox14.Checked = False Then
        Else
            parametros = parametros & " -maxVRAM=" & FlatTextBox2.Text
        End If
        If FlatCheckBox15.Checked = False Then
        Else
            parametros = parametros & " -exThreads=" & FlatTextBox4.Text
        End If
        If FlatCheckBox19.Checked = True Then
            If FlatComboBox4.SelectedItem = Nothing Then
            Else
                parametros = parametros & " -name=" & FlatComboBox4.SelectedItem.ToString
            End If
        End If
    End Sub

    Sub llenardatos()
        With DataGridView1.ColumnHeadersDefaultCellStyle
            .BackColor = Color.White
            .ForeColor = Color.Black
            .Font = New Font(DataGridView1.Font, FontStyle.Bold)
        End With
        DataGridView1.Rows.Clear()
        Try
            Dim client2302 As New WebClient()
            Dim stream2302 As Stream = client2302.OpenRead("https://api.gamerlabs.net/?type=arma3&host=188.165.200.127&port=2302")
            Dim reader2302 As New StreamReader(stream2302)
            Dim jsonData2302 As String = reader2302.ReadToEnd()
            If jsonData2302.Contains("Could not connect to UDP socket") Then
            Else
                Dim json2302 As JObject = JObject.Parse(jsonData2302)
                Dim nombre2302 As String = (json2302.SelectToken("data").SelectToken("name"))
                Dim puerto2302 As String = (json2302.SelectToken("data").SelectToken("raw").SelectToken("port"))
                Dim mision2302 As String = (json2302.SelectToken("data").SelectToken("raw").SelectToken("game"))
                Dim mapa2302 As String = (json2302.SelectToken("data").SelectToken("map"))
                Dim jugadores2302 As String = (json2302.SelectToken("data").SelectToken("raw").SelectToken("numplayers"))
                Dim slots2302 As String = (json2302.SelectToken("data").SelectToken("maxplayers"))
                Dim firmas2302 As String = (json2302.SelectToken("data").SelectToken("raw").SelectToken("secure"))
                Dim contraseña2302 As String = (json2302.SelectToken("data").SelectToken("password"))
                If firmas2302.Contains("0") Then
                    firmas2302 = "NO"
                Else
                    firmas2302 = "SI"
                End If
                If contraseña2302.Contains("True") Then
                    contraseña2302 = "SI"
                Else
                    contraseña2302 = "NO"
                End If
                DataGridView1.Rows.Add(nombre2302, puerto2302, mision2302, mapa2302, jugadores2302, slots2302, firmas2302, contraseña2302)
            End If
        Catch ex As Exception
        End Try
        Try
            Dim client2307 As New WebClient()
            Dim stream2307 As Stream = client2307.OpenRead("https://api.gamerlabs.net/?type=arma3&host=188.165.200.127&port=2307")
            Dim reader2307 As New StreamReader(stream2307)
            Dim jsonData2307 As String = reader2307.ReadToEnd()
            If jsonData2307.Contains("Could not connect to UDP socket") Then
            Else
                Dim json2307 As JObject = JObject.Parse(jsonData2307)
                Dim nombre2307 As String = (json2307.SelectToken("data").SelectToken("name"))
                Dim puerto2307 As String = (json2307.SelectToken("data").SelectToken("raw").SelectToken("port"))
                Dim mision2307 As String = (json2307.SelectToken("data").SelectToken("raw").SelectToken("game"))
                Dim mapa2307 As String = (json2307.SelectToken("data").SelectToken("map"))
                Dim jugadores2307 As String = (json2307.SelectToken("data").SelectToken("raw").SelectToken("numplayers"))
                Dim slots2307 As String = (json2307.SelectToken("data").SelectToken("maxplayers"))
                Dim firmas2307 As String = (json2307.SelectToken("data").SelectToken("raw").SelectToken("secure"))
                Dim contraseña2307 As String = (json2307.SelectToken("data").SelectToken("password"))
                If firmas2307.Contains("0") Then
                    firmas2307 = "NO"
                Else
                    firmas2307 = "SI"
                End If
                If contraseña2307.Contains("True") Then
                    contraseña2307 = "SI"
                Else
                    contraseña2307 = "NO"
                End If
                DataGridView1.Rows.Add(nombre2307, puerto2307, mision2307, mapa2307, jugadores2307, slots2307, firmas2307, contraseña2307)
            End If
        Catch ex As Exception
        End Try
        Try
            Dim client2312 As New WebClient()
            Dim stream2312 As Stream = client2312.OpenRead("https://api.gamerlabs.net/?type=arma3&host=188.165.200.127&port=2312")
            Dim reader2312 As New StreamReader(stream2312)
            Dim jsonData2312 As String = reader2312.ReadToEnd()
            If jsonData2312.Contains("Could not connect to UDP socket") Then
            Else
                Dim json2312 As JObject = JObject.Parse(jsonData2312)
                Dim nombre2312 As String = (json2312.SelectToken("data").SelectToken("name"))
                Dim puerto2312 As String = (json2312.SelectToken("data").SelectToken("raw").SelectToken("port"))
                Dim mision2312 As String = (json2312.SelectToken("data").SelectToken("raw").SelectToken("game"))
                Dim mapa2312 As String = (json2312.SelectToken("data").SelectToken("map"))
                Dim jugadores2312 As String = (json2312.SelectToken("data").SelectToken("raw").SelectToken("numplayers"))
                Dim slots2312 As String = (json2312.SelectToken("data").SelectToken("maxplayers"))
                Dim firmas2312 As String = (json2312.SelectToken("data").SelectToken("raw").SelectToken("secure"))
                Dim contraseña2312 As String = (json2312.SelectToken("data").SelectToken("password"))
                If firmas2312.Contains("0") Then
                    firmas2312 = "NO"
                Else
                    firmas2312 = "SI"
                End If
                If contraseña2312.Contains("True") Then
                    contraseña2312 = "SI"
                Else
                    contraseña2312 = "NO"
                End If
                DataGridView1.Rows.Add(nombre2312, puerto2312, mision2312, mapa2312, jugadores2312, slots2312, firmas2312, contraseña2312)
            End If
        Catch ex As Exception
        End Try
        Try
            Dim client2310 As New WebClient()
            Dim stream2310 As Stream = client2310.OpenRead("https://api.gamerlabs.net/?type=arma3&host=188.165.200.127&port=2310")
            Dim reader2310 As New StreamReader(stream2310)
            Dim jsonData2310 As String = reader2310.ReadToEnd()
            If jsonData2310.Contains("Could not connect to UDP socket") Then
            Else
                Dim json2310 As JObject = JObject.Parse(jsonData2310)
                Dim nombre2310 As String = (json2310.SelectToken("data").SelectToken("name"))
                Dim puerto2310 As String = (json2310.SelectToken("data").SelectToken("raw").SelectToken("port"))
                Dim mision2310 As String = (json2310.SelectToken("data").SelectToken("raw").SelectToken("game"))
                Dim mapa2310 As String = (json2310.SelectToken("data").SelectToken("map"))
                Dim jugadores2310 As String = (json2310.SelectToken("data").SelectToken("raw").SelectToken("numplayers"))
                Dim slots2310 As String = (json2310.SelectToken("data").SelectToken("maxplayers"))
                Dim firmas2310 As String = (json2310.SelectToken("data").SelectToken("raw").SelectToken("secure"))
                Dim contraseña2310 As String = (json2310.SelectToken("data").SelectToken("password"))
                If firmas2310.Contains("0") Then
                    firmas2310 = "NO"
                Else
                    firmas2310 = "SI"
                End If
                If contraseña2310.Contains("True") Then
                    contraseña2310 = "SI"
                Else
                    contraseña2310 = "NO"
                End If
                DataGridView1.Rows.Add(nombre2310, puerto2310, mision2310, mapa2310, jugadores2310, slots2310, firmas2310, contraseña2310)
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub DataGridView1_CellDoubleClick(ByVal sender As Object,
 ByVal e As DataGridViewCellEventArgs)
    End Sub


    Sub LUNESM()
        Try
            Dim instance As WebClient = New WebClient
            Dim address As String = "https://dl.dropbox.com/s/fmixrx85uduqqa8/lunes.txt"
            MaterialRaisedButton10.Text = "Leyendo Perfil..."
            RichTextBox2.Text = instance.DownloadString(address)
            Dim rutaaddons As String
            For Each line In RichTextBox2.Lines
                rutaaddons = arma3 & "\" & line
                If My.Computer.FileSystem.DirectoryExists(rutaaddons) Then
                Else
                    MessageBox.Show("El addon " & line & " no ha podido ser localizado entre sus addons, por favor revise Arma3Sync.",
                                 "ATENCIÓN!!",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Warning,
                                 MessageBoxDefaultButton.Button1)

                End If
            Next
            MaterialRaisedButton10.Text = "Lanzar Arma 3"
        Catch ex As WebException
            MessageBox.Show(ex.ToString)
        End Try
    End Sub
    Sub MARTESM()
        Try
            Dim instance As WebClient = New WebClient
            Dim address As String = "https://www.dropbox.com/s/lj56r2thmzsp82y/martes.txt?raw=1"
            MaterialRaisedButton10.Text = "Leyendo Perfil..."
            RichTextBox2.Text = instance.DownloadString(address)
            Dim rutaaddons As String
            For Each line In RichTextBox2.Lines
                rutaaddons = arma3 & "\" & line
                If My.Computer.FileSystem.DirectoryExists(rutaaddons) Then
                Else
                    MessageBox.Show("El addon " & line & " no ha podido ser localizado entre sus addons, por favor revise Arma3Sync.",
                                                  "ATENCIÓN!!",
                                                  MessageBoxButtons.OK,
                                                  MessageBoxIcon.Warning,
                                                  MessageBoxDefaultButton.Button1)
                End If
            Next
            MaterialRaisedButton10.Text = "Lanzar Arma 3"
        Catch ex As WebException
            MessageBox.Show(ex.ToString)
        End Try
    End Sub
    Sub MIERCOLESM()
        Try
            Dim instance As WebClient = New WebClient
            Dim address As String = "https://www.dropbox.com/s/bc5t7v5ajazlcu7/miercoles.txt?raw=1"
            MaterialRaisedButton10.Text = "Leyendo Perfil..."
            RichTextBox2.Text = instance.DownloadString(address)
            Dim rutaaddons As String
            For Each line In RichTextBox2.Lines
                rutaaddons = arma3 & "\" & line
                If My.Computer.FileSystem.DirectoryExists(rutaaddons) Then
                Else
                    MessageBox.Show("El addon " & line & " no ha podido ser localizado entre sus addons, por favor revise Arma3Sync.",
                                                  "ATENCIÓN!!",
                                                  MessageBoxButtons.OK,
                                                  MessageBoxIcon.Warning,
                                                  MessageBoxDefaultButton.Button1)
                End If
            Next
            MaterialRaisedButton10.Text = "Lanzar Arma 3"
        Catch ex As WebException
            MessageBox.Show(ex.ToString)
        End Try
    End Sub
    Sub JUEVESM()
        Try
            Dim instance As WebClient = New WebClient
            Dim address As String = "https://www.dropbox.com/s/y518is09c48js2f/jueves.txt?raw=1"
            MaterialRaisedButton10.Text = "Leyendo Perfil..."
            RichTextBox2.Text = instance.DownloadString(address)
            Dim rutaaddons As String
            For Each line In RichTextBox2.Lines
                rutaaddons = arma3 & "\" & line
                If My.Computer.FileSystem.DirectoryExists(rutaaddons) Then
                Else
                    MessageBox.Show("El addon " & line & " no ha podido ser localizado entre sus addons, por favor revise Arma3Sync.",
                                                      "ATENCIÓN!!",
                                                      MessageBoxButtons.OK,
                                                      MessageBoxIcon.Warning,
                                                      MessageBoxDefaultButton.Button1)
                End If
            Next

            MaterialRaisedButton10.Text = "Lanzar Arma 3"
        Catch ex As WebException
            MessageBox.Show(ex.ToString)
        End Try
    End Sub
    Sub VIERNESM()
        Try
            Dim instance As WebClient = New WebClient
            Dim address As String = "https://www.dropbox.com/s/wcht0q0d3q08otw/viernes.txt?raw=1"
            MaterialRaisedButton10.Text = "Leyendo Perfil..."
            RichTextBox2.Text = instance.DownloadString(address)
            Dim rutaaddons As String
            For Each line In RichTextBox2.Lines
                rutaaddons = arma3 & "\" & line
                If My.Computer.FileSystem.DirectoryExists(rutaaddons) Then
                Else
                    MessageBox.Show("El addon " & line & " no ha podido ser localizado entre sus addons, por favor revise Arma3Sync.",
                                                  "ATENCIÓN!!",
                                                  MessageBoxButtons.OK,
                                                  MessageBoxIcon.Warning,
                                                  MessageBoxDefaultButton.Button1)
                End If
            Next
            MaterialRaisedButton10.Text = "Lanzar Arma 3"
        Catch ex As WebException
            MessageBox.Show(ex.ToString)
        End Try
    End Sub
    Sub SABADOM()
        Try
            Dim instance As WebClient = New WebClient
            Dim address As String = "https://www.dropbox.com/s/z5ecuta3t8xs8tn/sabado.txt?raw=1"
            MaterialRaisedButton10.Text = "Leyendo Perfil..."
            RichTextBox2.Text = instance.DownloadString(address)
            Dim rutaaddons As String
            For Each line In RichTextBox2.Lines
                rutaaddons = arma3 & "\" & line
                If My.Computer.FileSystem.DirectoryExists(rutaaddons) Then
                Else
                    MessageBox.Show("El addon " & line & " no ha podido ser localizado entre sus addons, por favor revise Arma3Sync.",
                                                  "ATENCIÓN!!",
                                                  MessageBoxButtons.OK,
                                                  MessageBoxIcon.Warning,
                                                  MessageBoxDefaultButton.Button1)
                End If
            Next
            MaterialRaisedButton10.Text = "Lanzar Arma 3"
        Catch ex As WebException
            MessageBox.Show(ex.ToString)
        End Try
    End Sub
    Sub DOMINGOM()
        Try

            Dim instance As WebClient = New WebClient
            Dim address As String = "https://www.dropbox.com/s/0odh9iz5lq2tl7t/domingo.txt?raw=1"
            MaterialRaisedButton10.Text = "Leyendo Perfil..."
            RichTextBox2.Text = instance.DownloadString(address)
            Dim rutaaddons As String
            For Each line In RichTextBox2.Lines
                rutaaddons = arma3 & "\" & line
                If My.Computer.FileSystem.DirectoryExists(rutaaddons) Then
                Else
                    MessageBox.Show("El addon " & line & " no ha podido ser localizado entre sus addons, por favor revise Arma3Sync.",
                                                  "ATENCIÓN!!",
                                                  MessageBoxButtons.OK,
                                                  MessageBoxIcon.Warning,
                                                  MessageBoxDefaultButton.Button1)
                End If
            Next
            MaterialRaisedButton10.Text = "Lanzar Arma 3"
        Catch ex As WebException
            MessageBox.Show(ex.ToString)
        End Try
    End Sub
    Sub PERMANENTEM()
        Try
            Dim instance As WebClient = New WebClient
            Dim address As String = "https://www.dropbox.com/s/tp1wv9fpl3rci99/s.permanente.txt?raw=1"
            MaterialRaisedButton10.Text = "Leyendo Perfil..."
            RichTextBox2.Text = instance.DownloadString(address)
            Dim rutaaddons As String
            For Each line In RichTextBox2.Lines
                rutaaddons = arma3 & "\" & line
                If My.Computer.FileSystem.DirectoryExists(rutaaddons) Then
                Else
                    MessageBox.Show("El addon " & line & " no ha podido ser localizado entre sus addons, por favor revise Arma3Sync.",
                                                  "ATENCIÓN!!",
                                                  MessageBoxButtons.OK,
                                                  MessageBoxIcon.Warning,
                                                  MessageBoxDefaultButton.Button1)
                End If
            Next
            MaterialRaisedButton10.Text = "Lanzar Arma 3"
        Catch ex As WebException
            MessageBox.Show(ex.ToString)
        End Try
    End Sub
    Sub ACADEMIA1M()
        Try
            Dim instance As WebClient = New WebClient
            Dim address As String = "https://www.dropbox.com/s/djz5gv25vtzmtik/academia.txt?raw=1"
            MaterialRaisedButton10.Text = "Leyendo Perfil..."
            RichTextBox2.Text = instance.DownloadString(address)
            Dim rutaaddons As String
            For Each line In RichTextBox2.Lines
                rutaaddons = arma3 & "\" & line
                If My.Computer.FileSystem.DirectoryExists(rutaaddons) Then
                Else
                    MessageBox.Show("El addon " & line & " no ha podido ser localizado entre sus addons, por favor revise Arma3Sync.",
                                                  "ATENCIÓN!!",
                                                  MessageBoxButtons.OK,
                                                  MessageBoxIcon.Warning,
                                                  MessageBoxDefaultButton.Button1)
                End If
            Next
            MaterialRaisedButton10.Text = "Lanzar Arma 3"
        Catch ex As WebException
            MessageBox.Show(ex.ToString)
        End Try
    End Sub
    Sub ACADEMIA2M()
        Try
            Dim instance As WebClient = New WebClient
            Dim address As String = "https://www.dropbox.com/s/aio7iusa9gs788f/Academia2.txt?raw=1"
            MaterialRaisedButton10.Text = "Leyendo Perfil..."
            RichTextBox2.Text = instance.DownloadString(address)
            Dim rutaaddons As String
            For Each line In RichTextBox2.Lines
                rutaaddons = arma3 & "\" & line
                If My.Computer.FileSystem.DirectoryExists(rutaaddons) Then
                Else
                    MessageBox.Show("El addon " & line & " no ha podido ser localizado entre sus addons, por favor revise Arma3Sync.",
                                                  "ATENCIÓN!!",
                                                  MessageBoxButtons.OK,
                                                  MessageBoxIcon.Warning,
                                                  MessageBoxDefaultButton.Button1)
                End If
            Next
            MaterialRaisedButton10.Text = "Lanzar Arma 3"
        Catch ex As WebException
            MessageBox.Show(ex.ToString)
        End Try
    End Sub
    Sub OTROSM()
        Try
            Dim instance As WebClient = New WebClient
            Dim address As String = "https://www.dropbox.com/s/a3d4k1qpqy499ut/otros.txt?raw=1"
            MaterialRaisedButton10.Text = "Leyendo Perfil..."
            RichTextBox2.Text = instance.DownloadString(address)
            Dim rutaaddons As String
            For Each line In RichTextBox2.Lines
                rutaaddons = arma3 & "\" & line
                If My.Computer.FileSystem.DirectoryExists(rutaaddons) Then
                Else
                    MessageBox.Show("El addon " & line & " no ha podido ser localizado entre sus addons, por favor revise Arma3Sync.",
                                                  "ATENCIÓN!!",
                                                  MessageBoxButtons.OK,
                                                  MessageBoxIcon.Warning,
                                                  MessageBoxDefaultButton.Button1)
                End If
            Next
            MaterialRaisedButton10.Text = "Lanzar Arma 3"
        Catch ex As WebException
            MessageBox.Show(ex.ToString)
        End Try
    End Sub
    Sub PRUEBASM()
        Try
            Dim instance As WebClient = New WebClient
            Dim address As String = "https://www.dropbox.com/s/7zsa1mws02pmmob/Pruebas.txt?raw=1"
            MaterialRaisedButton10.Text = "Leyendo Perfil..."
            RichTextBox2.Text = instance.DownloadString(address)
            Dim rutaaddons As String
            For Each line In RichTextBox2.Lines
                rutaaddons = arma3 & "\" & line
                If My.Computer.FileSystem.DirectoryExists(rutaaddons) Then
                Else
                    MessageBox.Show("El addon " & line & " no ha podido ser localizado entre sus addons, por favor revise Arma3Sync.",
                                                  "ATENCIÓN!!",
                                                  MessageBoxButtons.OK,
                                                  MessageBoxIcon.Warning,
                                                  MessageBoxDefaultButton.Button1)
                End If
            Next
            MaterialRaisedButton10.Text = "Lanzar Arma 3"
        Catch ex As WebException
            MessageBox.Show(ex.ToString)
        End Try
    End Sub
    Sub loadout()
        Try
            Dim instance As WebClient = New WebClient
            Dim address As String = "https://www.dropbox.com/s/ogdckle7g2a4ryb/loadout.txt?raw=1"
            MaterialRaisedButton10.Text = "Leyendo Perfil..."
            RichTextBox2.Text = instance.DownloadString(address)
            Dim rutaaddons As String
            For Each line In RichTextBox2.Lines
                rutaaddons = arma3 & "\" & line
                If My.Computer.FileSystem.DirectoryExists(rutaaddons) Then
                Else
                    MessageBox.Show("El addon " & line & " no ha podido ser localizado entre sus addons, por favor revise Arma3Sync.",
                                                  "ATENCIÓN!!",
                                                  MessageBoxButtons.OK,
                                                  MessageBoxIcon.Warning,
                                                  MessageBoxDefaultButton.Button1)
                End If
            Next
            MaterialRaisedButton10.Text = "Lanzar Arma 3"
        Catch ex As WebException
            MessageBox.Show(ex.ToString)
        End Try
    End Sub
    Private Sub FlatComboBox3_SelectedIndexChanged(sender As Object, e As EventArgs) Handles FlatComboBox3.SelectedIndexChanged
        Dim dias As String
        dias = FlatComboBox3.SelectedItem.ToString
        RichTextBox2.Clear()
        If dias = "LUNES" Then
            Call LUNESM()
        End If
        If dias = "MARTES" Then
            Call MARTESM()
        End If
        If dias = "MIÉRCOLES" Then
            Call MIERCOLESM()
        End If
        If dias = "JUEVES" Then
            Call JUEVESM()
        End If
        If dias = "VIERNES" Then
            Call VIERNESM()
        End If
        If dias = "SÁBADO" Then
            Call SABADOM()
        End If
        If dias = "DOMINGO" Then
            Call DOMINGOM()
        End If
        If dias = "PERMANENTE" Then
            Call PERMANENTEM()
        End If
        If dias = "OTROS" Then
            Call OTROSM()
        End If
        If dias = "ACADEMIA 1" Then
            Call ACADEMIA1M()
        End If
        If dias = "ACADEMIA 2" Then
            Call ACADEMIA2M()
        End If
        If dias = "PRUEBAS" Then
            Call PRUEBASM()
        End If
        If dias = "LOADOUT" Then
            Call loadout()
        End If
        FlatLabel8.Text = "PERFIL ESUS = " & dias
    End Sub



    Private Function WmiSelect() As Object
        Throw New NotImplementedException
    End Function
    Private Sub OnDownloadProgressChanged(ByVal sender As Object, ByVal e As System.Net.DownloadProgressChangedEventArgs)
        Dim totalSize As Long = e.TotalBytesToReceive
        Dim downloadedBytes As Long = e.BytesReceived
        Dim percentage As Integer = e.ProgressPercentage
        ProgressBar2.Value = percentage
        'Put your progress UI here, you can cancel download by uncommenting the line below
        'wc.CancelAsync()
    End Sub
    Private Sub OnFileDownloadCompleted(ByVal sender As Object, ByVal e As System.ComponentModel.AsyncCompletedEventArgs)
        If e.Cancelled Then
        ElseIf Not e.Error Is Nothing Then
        Else
            MessageBox.Show("Archivos revisados!",
                                           "Información",
                                           MessageBoxButtons.OK,
                                           MessageBoxIcon.Information,
                                           MessageBoxDefaultButton.Button1)
            ProgressBar2.Visible = False
            Button7.Visible = False
        End If
    End Sub
    Private Sub FlatButton1_Click(sender As Object, e As EventArgs) Handles FlatButton1.Click
        FlatLabel6.Visible = False
        FlatLabel13.Visible = False
        FlatLabel14.Visible = False
        TextBox9.Visible = False
        TextBox10.Visible = False
        FlatComboBox4.Visible = False
        FlatComboBox2.Visible = False
        FlatCheckBox19.Visible = False
        TextBox1.Visible = False
        FlatLabel12.Visible = False
        FlatProgressBar1.Value = 0
        Timer1.Start()
        FlatProgressBar1.Visible = True
    End Sub
    Private Sub checkedlistbox1_Leave(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckedListBox1.Leave
        CheckedListBox1.Update()
    End Sub

    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        FlatProgressBar1.Value += 20
        If FlatProgressBar1.Value = "100" Then
            Timer1.Stop()
            FlatCheckBox12.Checked = True
            Try
                If Environment.ProcessorCount >= "16" Then
                    Dim numerocpu As String = "16"
                    FlatTrackBar1.Value = numerocpu
                    FlatTextBox1.Text = numerocpu
                Else
                    FlatTrackBar1.Value = Environment.ProcessorCount
                    FlatTextBox1.Text = Environment.ProcessorCount
                End If
                If FlatTrackBar1.Value.ToString < "2" Then
                    FlatTrackBar4.Value = Nothing
                ElseIf FlatTrackBar1.Value.ToString = "2" Then
                    FlatTrackBar4.Value = "1"
                ElseIf FlatTrackBar1.Value.ToString <= "4" Then
                    FlatTrackBar4.Value = "3"
                ElseIf FlatTrackBar1.Value.ToString >= "8" Then
                    FlatTrackBar4.Value = "7"
                End If
            Catch ex As Exception
                MessageBox.Show("Numero de Nucleos de Cpu no han podido ser reconocidos automáticamente, por fovor marquelos de forma manual.",
                               "Atención!",
                               MessageBoxButtons.OK,
                               MessageBoxIcon.Exclamation,
                               MessageBoxDefaultButton.Button1)
            End Try
            FlatCheckBox13.Checked = True
            FlatCheckBox14.Checked = True
            FlatCheckBox15.Checked = True
            FlatCheckBox1.Checked = True
            FlatCheckBox2.Checked = True
            FlatCheckBox3.Checked = True
            FlatCheckBox4.Checked = True
            FlatCheckBox5.Checked = True
            FlatCheckBox6.Checked = False
            FlatCheckBox7.Checked = False
            FlatCheckBox8.Checked = False
            FlatCheckBox9.Checked = False
            FlatCheckBox10.Checked = True
            FlatCheckBox11.Checked = False
            FlatCheckBox17.Checked = True
            TextBox6.Text = Nothing
            Try
                If (My.Computer.Info.TotalPhysicalMemory / (1024 * 1024)) <= 32768 Then
                    FlatTrackBar3.Value = String.Format(System.Math.Round(My.Computer.Info.TotalPhysicalMemory / (1024 * 1024)), 2).ToString()
                    FlatTextBox3.Text = String.Format(System.Math.Round(My.Computer.Info.TotalPhysicalMemory / (1024 * 1024)), 2).ToString()
                Else
                    Dim memoriaram As String = "32768"
                    FlatTrackBar3.Value = memoriaram
                    FlatTextBox3.Text = memoriaram
                End If
            Catch ex As Exception
                MessageBox.Show("Memoria ram no pudo ser localizada automáticamente, marquela de forma manual.",
                            "Atención!",
                            MessageBoxButtons.OK,
                            MessageBoxIcon.Exclamation,
                            MessageBoxDefaultButton.Button1)
            End Try
            Try
                If GetInfo(InfoTypes.VideocardMem) >= "8192" Then
                    FlatTrackBar2.Value = "8192"
                    FlatTextBox2.Text = "8192"
                Else
                    FlatTrackBar2.Value = GetInfo(InfoTypes.VideocardMem)
                    FlatTextBox2.Text = GetInfo(InfoTypes.VideocardMem)
                End If

            Catch ex As Exception
                MessageBox.Show("Memoria de video no pudo ser localizada automáticamente, marquela de forma manual.",
                          "Atención!",
                          MessageBoxButtons.OK,
                          MessageBoxIcon.Exclamation,
                          MessageBoxDefaultButton.Button1)
            End Try

            If FlatCheckBox12.Checked = True Then
                FlatTrackBar1.Visible = True
                FlatTextBox1.Visible = True
            Else
                FlatTrackBar1.Visible = False
                FlatTextBox1.Visible = False
                FlatTrackBar1.Value = Nothing
                FlatTextBox1.Text = Nothing
            End If
            If FlatCheckBox13.Checked = True Then
                FlatTrackBar3.Visible = True
                FlatTextBox3.Visible = True
            Else
                FlatTrackBar3.Visible = False
                FlatTextBox3.Visible = False
                FlatTrackBar3.Value = Nothing
                FlatTextBox3.Text = Nothing
            End If
            If FlatCheckBox14.Checked = True Then
                FlatTrackBar2.Visible = True
                FlatTextBox2.Visible = True
            Else
                FlatTrackBar2.Visible = False
                FlatTextBox2.Visible = False
                FlatTrackBar2.Value = Nothing
                FlatTextBox2.Text = Nothing
            End If
            If FlatCheckBox15.Checked = True Then
                FlatTrackBar4.Visible = True
                FlatTextBox4.Visible = True
            Else
                FlatTrackBar4.Visible = False
                FlatTextBox4.Visible = False
                FlatTrackBar4.Value = Nothing
                FlatTextBox4.Text = Nothing
            End If
            FlatProgressBar1.Visible = False
            FlatLabel6.Visible = True
            FlatComboBox2.Visible = True
            If FlatCheckBox18.Checked = True Then
                FlatLabel13.Visible = True
                FlatLabel14.Visible = True
                TextBox9.Visible = True
                TextBox10.Visible = True
                TextBox1.Visible = True
                FlatLabel12.Visible = True
            End If
            FlatCheckBox19.Visible = True
            If FlatCheckBox19.Checked = True Then
                FlatComboBox4.Visible = True
            Else
                FlatComboBox4.Visible = False
            End If
        End If
    End Sub


    Private Sub FlatToggle1_CheckedChanged(sender As Object) Handles FlatToggle1.CheckedChanged
        If FlatToggle1.Checked = True Then
            MaterialRaisedButton5.Visible = False
            CheckedListBox1.Visible = False
            CheckedListBox2.Visible = True
            ListBox1.Visible = False
            ListBox3.Visible = True
            Panel1.Enabled = False
            FlatComboBox1.Visible = False
            FlatComboBox3.Visible = True
            FlatLabel9.Visible = True
            FlatLabel8.Visible = True
            RichTextBox2.Visible = True
            FlatTextBox9.Visible = False
            FlatTextBox6.Visible = False
            FlatLabel15.Visible = False
            FlatLabel16.Visible = False
            FlatLabel17.Visible = False
        Else
            MaterialRaisedButton5.Visible = True
            CheckedListBox1.Visible = True
            CheckedListBox2.Visible = False
            ListBox1.Visible = True
            ListBox3.Visible = False
            Panel1.Enabled = True
            FlatComboBox1.Visible = True
            FlatComboBox3.Visible = False
            RichTextBox2.Visible = False
            FlatLabel9.Visible = False
            FlatLabel8.Visible = False
            FlatTextBox9.Visible = True
            FlatTextBox6.Visible = True
            FlatLabel15.Visible = True
            FlatLabel16.Visible = True
            FlatLabel17.Visible = True
            FlatTextBox9.Text = CheckedListBox1.Items.Count
            FlatTextBox6.Text = CheckedListBox1.CheckedItems.Count
        End If
    End Sub


    Private Sub MaterialRaisedButton11_Click(sender As Object, e As EventArgs) Handles MaterialRaisedButton11.Click
        Label1.Visible = True
        CheckBox1.Visible = False
        Label1.Refresh()
        System.Threading.Thread.Sleep(250)
        If FlatToggle1.Checked = False Then
            Try
                seleccion = FlatComboBox1.SelectedItem.ToString & ".txt"
                Dim rutaguardado As String
                rutaguardado = Combine(programDataFilePath, seleccion)
                Dim sb As New System.Text.StringBuilder
                For Each itm As String In CheckedListBox1.CheckedItems
                    sb.AppendLine(itm)
                Next
                If sb.ToString.Length >= 0 Then
                    My.Computer.FileSystem.WriteAllText(rutaguardado, sb.ToString, False)
                End If
                seleccion = FlatComboBox1.SelectedItem.ToString & ".prioridad.dat"
                rutaguardado = Combine(programDataFilePath, seleccion)
                Dim s As New IO.StreamWriter(rutaguardado)
                For i As Integer = 0 To ListBox1.Items.Count - 1
                    s.Write(ListBox1.Items(i) & vbNewLine)
                Next
                s.Close()
            Catch ex As Exception

            End Try
        Else
            Try
                Dim sb As New System.Text.StringBuilder
                For Each itm As String In CheckedListBox2.CheckedItems
                    sb.AppendLine(itm)
                Next
                If sb.ToString.Length >= 0 Then
                    My.Computer.FileSystem.WriteAllText(perfilesuson, sb.ToString, False)
                End If
                Dim s As New IO.StreamWriter(perfilesusprioridadon)
                For i As Integer = 0 To ListBox3.Items.Count - 1
                    s.Write(ListBox3.Items(i) & vbNewLine)
                Next
                s.Close()
            Catch ex As Exception

            End Try
        End If
        Try
            Dim x As New IO.StreamWriter(programasexternos)
            For i As Integer = 0 To CheckedListBox3.Items.Count - 1
                x.Write(CheckedListBox3.Items(i) & vbNewLine)
            Next
            x.Close()
            Dim bc As New System.Text.StringBuilder
            For Each itm As String In CheckedListBox3.CheckedItems
                bc.AppendLine(itm)
            Next
            If bc.ToString.Length >= 0 Then
                My.Computer.FileSystem.WriteAllText(programasexternoselegidos, bc.ToString, False)
            End If
        Catch ex As Exception
        End Try
        Dim a As Integer
        Dim aryText(37) As String
        If FlatCheckBox1.Checked = True Then
            aryText(0) = "1"
        Else
            aryText(0) = "0"
        End If
        If FlatCheckBox2.Checked = True Then
            aryText(1) = "1"
        Else
            aryText(1) = "0"
        End If
        If FlatCheckBox3.Checked = True Then
            aryText(2) = "1"
        Else
            aryText(2) = "0"
        End If
        If FlatCheckBox4.Checked = True Then
            aryText(3) = "1"
        Else
            aryText(3) = "0"
        End If
        If FlatCheckBox5.Checked = True Then
            aryText(4) = "1"
        Else
            aryText(4) = "0"
        End If
        If FlatCheckBox6.Checked = True Then
            aryText(5) = "1"
        Else
            aryText(5) = "0"
        End If
        If FlatCheckBox7.Checked = True Then
            aryText(6) = "1"
        Else
            aryText(6) = "0"
        End If
        If FlatCheckBox8.Checked = True Then
            aryText(7) = "1"
        Else
            aryText(7) = "0"
        End If
        If FlatCheckBox9.Checked = True Then
            aryText(8) = "1"
        Else
            aryText(8) = "0"
        End If
        If FlatCheckBox10.Checked = True Then
            aryText(9) = "1"
        Else
            aryText(9) = "0"
        End If
        If FlatCheckBox11.Checked = True Then
            aryText(10) = "1"
        Else
            aryText(10) = "0"
        End If
        If FlatCheckBox12.Checked = True Then
            aryText(11) = "1"
        Else
            aryText(11) = "0"
        End If
        If FlatCheckBox13.Checked = True Then
            aryText(12) = "1"
        Else
            aryText(12) = "0"
        End If
        If FlatCheckBox14.Checked = True Then
            aryText(13) = "1"
        Else
            aryText(13) = "0"
        End If
        If FlatCheckBox15.Checked = True Then
            aryText(14) = "1"
        Else
            aryText(14) = "0"
        End If
        If FlatTextBox1.Text <> "" Then
            aryText(15) = FlatTextBox1.Text
        Else
            aryText(15) = "nada"
        End If
        If FlatTextBox3.Text <> "" Then
            aryText(16) = FlatTextBox3.Text
        Else
            aryText(16) = "nada"
        End If
        If FlatTextBox2.Text <> "" Then
            aryText(17) = FlatTextBox2.Text
        Else
            aryText(17) = "nada"
        End If
        If FlatTextBox4.Text <> "" Then
            aryText(18) = FlatTextBox4.Text
        Else
            aryText(18) = "nada"
        End If
        If TextBox6.Text <> "" Then
            aryText(19) = TextBox6.Text
        Else
            aryText(19) = "nada"
        End If
        If FlatCheckBox16.Checked = True Then
            aryText(20) = "1"
        Else
            aryText(20) = "0"
        End If
        If FlatCheckBox16.Checked = True Then
            aryText(21) = FlatTextBox5.Text
            aryText(22) = FlatTextBox8.Text
        Else
            aryText(21) = "rutaporregistro"
            aryText(22) = "rutaporregistro"
        End If
        aryText(23) = favorito
        Try
            aryText(24) = CheckedListBox3.Items(0).ToString()
            aryText(25) = CheckedListBox3.Items(1).ToString()
            aryText(26) = CheckedListBox3.Items(2).ToString()
            aryText(27) = CheckedListBox3.Items(3).ToString()
            aryText(28) = CheckedListBox3.Items(4).ToString()
        Catch ex As Exception
        End Try
        Dim index = FlatComboBox2.SelectedIndex
        Select Case index
            Case 0
                aryText(29) = "nada"
            Case 1
                aryText(29) = "cerrar"
            Case 2
                aryText(29) = "minimizar"
        End Select
        If FlatCheckBox17.Checked = True Then
            aryText(30) = "1"
        Else
            aryText(30) = "0"
        End If
        If FlatCheckBox18.Checked = True Then
            aryText(31) = "1"
        Else
            aryText(31) = "0"
        End If
        If FlatCheckBox19.Checked = True Then
            aryText(32) = "1"
        Else
            aryText(32) = "0"
        End If
        If FlatCheckBox19.Checked = True Then
            If FlatComboBox4.SelectedItem = Nothing Then
                aryText(33) = "nada"
            Else
                aryText(33) = FlatComboBox4.SelectedItem.ToString
            End If
        End If
        If FlatCheckBox18.Checked = True Then
            If TextBox9.Text = Nothing Then
                aryText(34) = "nada"
            Else
                aryText(34) = TextBox9.Text
            End If
            If TextBox10.Text = Nothing Then
                aryText(35) = "nada"
            Else
                aryText(35) = TextBox10.Text
            End If
            If TextBox1.Text = Nothing Then
                aryText(36) = "nada"
            Else
                aryText(36) = TextBox1.Text
            End If
        Else
            aryText(34) = "nada"
            aryText(35) = "nada"
            If TextBox1.Text = Nothing Then
                aryText(36) = "nada"
            Else
                aryText(36) = TextBox1.Text
            End If
        End If
        aryText(37) = CheckBox1.CheckState
        Try
            Dim escritor As New System.IO.StreamWriter(configuracion)
            For a = 0 To 37
                escritor.WriteLine(aryText(a))
            Next
            escritor.Close()
        Catch ex As Exception

        End Try
        Label1.Visible = False
        If FlatCheckBox18.Checked = True Then
            CheckBox1.Visible = True
        End If
    End Sub


    Private Sub MaterialRaisedButton4_Click(sender As Object, e As EventArgs) Handles MaterialRaisedButton4.Click
        If CheckedListBox1.Visible = True Then
            CheckedListBox1.BeginUpdate()
            CheckedListBox1.Items.Clear()
            ListBox1.Items.Clear()
            For Each folder In System.IO.Directory.GetDirectories(arma3, "@*")
                CheckedListBox1.Items.Add(Path.GetFileName(folder))
                For Each folder2 In System.IO.Directory.GetDirectories(folder, "@*")
                    CheckedListBox1.Items.Add(Path.GetFileName(folder) + "/" + Path.GetFileName(folder2))
                    CheckedListBox1.Items.Remove(Path.GetFileName(folder))
                Next
            Next
            Call leermods()
            CheckedListBox1.EndUpdate()
        Else
            MaterialRaisedButton10.Text = "LEYENDO PERFIL..."
            Call leerclientside()
            Dim dias As String
            If FlatComboBox3.SelectedItem <> Nothing Then
                dias = FlatComboBox3.SelectedItem.ToString
                RichTextBox2.Clear()
                If dias = "LUNES" Then
                    Call LUNESM()
                End If
                If dias = "MARTES" Then
                    Call MARTESM()
                End If
                If dias = "MIÉRCOLES" Then
                    Call MIERCOLESM()
                End If
                If dias = "JUEVES" Then
                    Call JUEVESM()
                End If
                If dias = "VIERNES" Then
                    Call VIERNESM()
                End If
                If dias = "SÁBADO" Then
                    Call SABADOM()
                End If
                If dias = "DOMINGO" Then
                    Call DOMINGOM()
                End If
                If dias = "PERMANENTE" Then
                    Call PERMANENTEM()
                End If
                If dias = "OTROS" Then
                    Call OTROSM()
                End If
                If dias = "ACADEMIA 1" Then
                    Call ACADEMIA1M()
                End If
                If dias = "ACADEMIA 2" Then
                    Call ACADEMIA2M()
                End If
                If dias = "PRUEBAS" Then
                    Call PRUEBASM()
                End If
            End If
            MaterialRaisedButton10.Text = "LANZAR ARMA 3"

        End If
        FlatTextBox9.Text = CheckedListBox1.Items.Count
        FlatTextBox6.Text = CheckedListBox1.CheckedItems.Count
    End Sub


    Private Sub MaterialRaisedButton3_Click(sender As Object, e As EventArgs) Handles MaterialRaisedButton3.Click
        If CheckedListBox1.Visible = True Then
            CheckedListBox1.BeginUpdate()
            For i As Integer = 0 To CheckedListBox1.Items.Count - 1
                If CheckedListBox1.GetItemChecked(i) = True Then
                    CheckedListBox1.SetItemChecked(i, False)
                ElseIf CheckedListBox1.GetItemChecked(i) = False Then
                    CheckedListBox1.SetItemChecked(i, True)
                End If
            Next
            CheckedListBox1.EndUpdate()
            ListBox1.Items.Clear()
            If CheckedListBox1.CheckedItems.Count > 0 Then
                For i As Integer = 0 To CheckedListBox1.CheckedItems.Count - 1
                    ListBox1.Items.Add(CheckedListBox1.CheckedItems(i))
                Next
            End If
        Else
            For i As Integer = 0 To CheckedListBox2.Items.Count - 1
                If CheckedListBox2.GetItemChecked(i) = True Then
                    CheckedListBox2.SetItemChecked(i, False)
                ElseIf CheckedListBox2.GetItemChecked(i) = False Then
                    CheckedListBox2.SetItemChecked(i, True)
                End If
            Next
            ListBox3.Items.Clear()
            If CheckedListBox2.CheckedItems.Count > 0 Then
                For i As Integer = 0 To CheckedListBox2.CheckedItems.Count - 1
                    ListBox3.Items.Add(CheckedListBox2.CheckedItems(i))
                Next
            End If
        End If
        FlatTextBox9.Text = CheckedListBox1.Items.Count
        FlatTextBox6.Text = CheckedListBox1.CheckedItems.Count
    End Sub





    Private Sub MaterialRaisedButton2_Click(sender As Object, e As EventArgs) Handles MaterialRaisedButton2.Click
        If CheckedListBox1.Visible = True Then
            Dim checked As Boolean = True
            CheckedListBox1.BeginUpdate()
            For i As Integer = 0 To CheckedListBox1.Items.Count - 1
                CheckedListBox1.SetItemChecked(i, checked)
            Next
            CheckedListBox1.EndUpdate()
            ListBox1.Items.Clear()
            If CheckedListBox1.CheckedItems.Count > 0 Then
                For i As Integer = 0 To CheckedListBox1.CheckedItems.Count - 1
                    ListBox1.Items.Add(CheckedListBox1.CheckedItems(i))
                Next
            End If
        Else
            Dim checked As Boolean = True
            For i As Integer = 0 To CheckedListBox2.Items.Count - 1
                CheckedListBox2.SetItemChecked(i, checked)
            Next
            ListBox3.Items.Clear()
            If CheckedListBox2.CheckedItems.Count > 0 Then
                For i As Integer = 0 To CheckedListBox2.CheckedItems.Count - 1
                    ListBox3.Items.Add(CheckedListBox2.CheckedItems(i))
                Next
            End If
        End If
        FlatTextBox9.Text = CheckedListBox1.Items.Count
        FlatTextBox6.Text = CheckedListBox1.CheckedItems.Count
    End Sub

    Private Sub MaterialRaisedButton1_Click(sender As Object, e As EventArgs) Handles MaterialRaisedButton1.Click
        If CheckedListBox1.Visible = True Then
            CheckedListBox1.BeginUpdate()
            Dim checked As Boolean = False
            For i As Integer = 0 To CheckedListBox1.Items.Count - 1
                CheckedListBox1.SetItemChecked(i, checked)
            Next
            ListBox1.Items.Clear()
            CheckedListBox1.EndUpdate()
        Else
            Dim checked As Boolean = False
            For i As Integer = 0 To CheckedListBox2.Items.Count - 1
                CheckedListBox2.SetItemChecked(i, checked)
            Next
            ListBox3.Items.Clear()
        End If
        FlatTextBox9.Text = CheckedListBox1.Items.Count
        FlatTextBox6.Text = CheckedListBox1.CheckedItems.Count
    End Sub

    Private Sub CopiarToolStripMenuItem_Click_1(sender As Object, e As EventArgs) Handles CopiarToolStripMenuItem.Click
        Dim builder As New StringBuilder()
        If ListBox1.Items.Count = 0 Then
            MessageBox.Show("No se ha podido copiar nada, ya que no hay ningún mod seleccionado.",
                                            "IMPOSIBLE COPIAR!",
                                            MessageBoxButtons.OK,
                                            MessageBoxIcon.Exclamation,
                                            MessageBoxDefaultButton.Button1)
        Else
            For Each i As Object In ListBox1.Items
                i = i + vbNewLine
                builder.Append(i.ToString())
            Next
            builder.ToString()
        End If
        Try
            Clipboard.SetText(builder.ToString())
            MessageBox.Show("Se ha copiado correctamente a su portapapeles la lista de addons seleccionados.",
                                  "COPIADO!",
                                  MessageBoxButtons.OK,
                                  MessageBoxIcon.Information,
                                  MessageBoxDefaultButton.Button1)
        Catch ex As Exception
        End Try
    End Sub


    Private Sub CheckedListBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CheckedListBox1.SelectedIndexChanged
        ListBox1.Items.Clear()
        If CheckedListBox1.CheckedItems.Count > 0 Then
            For i As Integer = 0 To CheckedListBox1.CheckedItems.Count - 1
                ListBox1.Items.Add(CheckedListBox1.CheckedItems(i))
            Next
        End If
        FlatTextBox6.Text = CheckedListBox1.CheckedItems.Count
    End Sub

    Private Sub MaterialRaisedButton5_Click(sender As Object, e As EventArgs) Handles MaterialRaisedButton5.Click
        CargarListavb.Show()
    End Sub


    Sub lanzarjuego()
        Call construirparametro()
        For Each itemChecked In CheckedListBox3.CheckedItems
            Try
                Dim elegido As String
                elegido = itemChecked
                Process.Start(elegido)
            Catch ex As Exception

            End Try
        Next
        If FlatCheckBox18.Checked = True Then
            If CheckBox1.Checked = True Then
                If TextBox9.Text = Nothing Then
                    Process.Start(steam, parametros)
                Else
                    parametros = parametros & " -connect=" & TextBox9.Text
                    If TextBox10.Text = Nothing Then
                        FlatComboBox2.SelectedIndex = 1
                        Puerto.Show()
                    Else
                        parametros = parametros & " -port=" & TextBox10.Text
                        If TextBox1.Text = Nothing Then
                            Process.Start(steam, parametros)
                        Else
                            parametros = parametros & " -password=" & TextBox1.Text
                            Process.Start(steam, parametros)
                        End If
                    End If
                End If
            Else
                Process.Start(steam, parametros)
            End If
        Else
            Process.Start(steam, parametros)
        End If
        Dim index = FlatComboBox2.SelectedIndex
        Select Case index
            Case 0

            Case 1
                Me.WindowState = FormWindowState.Minimized
            Case 2
                Me.Close()
        End Select
    End Sub
    Private Sub MaterialRaisedButton10_Click_1(sender As Object, e As EventArgs) Handles MaterialRaisedButton10.Click
        Call lanzarjuego()
    End Sub


    Private Sub MaterialRaisedButton8_Click(sender As Object, e As EventArgs) Handles MaterialRaisedButton8.Click
        If ListBox1.Visible = True Then
            If ListBox1.SelectedItem = Nothing Then
            Else
                If ListBox1.SelectedIndex < ListBox1.Items.Count - 1 Then
                    Dim I = ListBox1.SelectedIndex + 2
                    ListBox1.Items.Insert(I, ListBox1.SelectedItem)
                    ListBox1.Items.RemoveAt(ListBox1.SelectedIndex)
                    ListBox1.SelectedIndex = I - 1
                End If
            End If
        Else
            If ListBox3.SelectedItem = Nothing Then
            Else
                If ListBox3.SelectedIndex < ListBox3.Items.Count - 1 Then
                    Dim I = ListBox3.SelectedIndex + 2
                    ListBox3.Items.Insert(I, ListBox3.SelectedItem)
                    ListBox3.Items.RemoveAt(ListBox3.SelectedIndex)
                    ListBox3.SelectedIndex = I - 1
                End If
            End If
        End If
    End Sub

    Private Sub MaterialRaisedButton7_Click(sender As Object, e As EventArgs) Handles MaterialRaisedButton7.Click
        If ListBox1.Visible = True Then
            Dim limite As Integer = (ListBox1.Items.Count - 1)
            Dim item As String
            If ListBox1.SelectedItem = Nothing Then
                item = Nothing
            Else
                item = ListBox1.SelectedItem
                ListBox1.Items.Remove(item)
                ListBox1.Items.Insert(limite, item)
                ListBox1.SelectedIndex = limite
            End If
        Else
            Dim limite As Integer = (ListBox3.Items.Count - 1)
            Dim item As String
            If ListBox3.SelectedItem = Nothing Then
                item = Nothing
            Else
                item = ListBox3.SelectedItem
                ListBox3.Items.Remove(item)
                ListBox3.Items.Insert(limite, item)
                ListBox3.SelectedIndex = limite
            End If
        End If
    End Sub

    Private Sub MaterialRaisedButton9_Click(sender As Object, e As EventArgs) Handles MaterialRaisedButton9.Click
        If ListBox1.Visible = True Then
            If ListBox1.SelectedItem = Nothing Then
            Else
                If ListBox1.SelectedIndex > 0 Then
                    Dim I = ListBox1.SelectedIndex - 1
                    ListBox1.Items.Insert(I, ListBox1.SelectedItem)
                    ListBox1.Items.RemoveAt(ListBox1.SelectedIndex)
                    ListBox1.SelectedIndex = I
                End If
            End If
        Else
            If ListBox3.SelectedItem = Nothing Then
            Else
                If ListBox3.SelectedIndex > 0 Then
                    Dim I = ListBox3.SelectedIndex - 1
                    ListBox3.Items.Insert(I, ListBox3.SelectedItem)
                    ListBox3.Items.RemoveAt(ListBox3.SelectedIndex)
                    ListBox3.SelectedIndex = I
                End If
            End If
        End If
    End Sub

    Private Sub MaterialRaisedButton6_Click(sender As Object, e As EventArgs) Handles MaterialRaisedButton6.Click
        If ListBox1.Visible = True Then
            Dim item As String
            If ListBox1.SelectedItem = Nothing Then
                item = Nothing
            Else
                item = ListBox1.SelectedItem
                ListBox1.Items.Remove(item)
                ListBox1.Items.Insert(0, item)
                ListBox1.SelectedIndex = 0
            End If
        Else
            Dim item As String
            If ListBox3.SelectedItem = Nothing Then
                item = Nothing
            Else
                item = ListBox3.SelectedItem
                ListBox3.Items.Remove(item)
                ListBox3.Items.Insert(0, item)
                ListBox3.SelectedIndex = 0
            End If
        End If
    End Sub


    Private Sub Button2_Click_1(sender As Object, e As EventArgs) Handles Button2.Click
        If FlatTextBox7.Text <> "" Then
            Dim crearperfil As System.IO.FileStream
            Dim crearprioridad As System.IO.FileStream
            Dim nuevoperfil As String = Combine(programDataFilePath, FlatTextBox7.Text & ".txt")
            Dim nuevaprioridad As String = Combine(programDataFilePath, FlatTextBox7.Text & ".prioridad.dat")
            crearperfil = File.Create(nuevoperfil)
            crearperfil.Close()
            crearprioridad = File.Create(nuevaprioridad)
            crearprioridad.Close()
            FlatTextBox7.Text = Nothing
            ListBox2.Items.Clear()
            FlatComboBox1.Items.Clear()
            Try
                For Each File As String In My.Computer.FileSystem.GetFiles _
         (programDataFilePath, FileIO.SearchOption.SearchTopLevelOnly, "*.txt")
                    ListBox2.Items.Add(System.IO.Path.GetFileNameWithoutExtension(File))
                    FlatComboBox1.Items.Add(System.IO.Path.GetFileNameWithoutExtension(File))
                    FlatComboBox1.SelectedItem = "Por Defecto"
                Next
            Catch ex As Exception
            End Try
        Else
            MessageBox.Show("Debes escribir un nombre para el perfil",
                  "IMPORTANTE!",
                  MessageBoxButtons.OK,
                  MessageBoxIcon.Exclamation,
                  MessageBoxDefaultButton.Button1)

        End If
    End Sub


    Private Sub Button6_Click_1(sender As Object, e As EventArgs) Handles Button6.Click
        Process.Start("ts3server://188.165.200.127?port=9987")

    End Sub

    Private Sub FlatButton2_Click(sender As Object, e As EventArgs) Handles FlatButton2.Click
        Call llenardatos()
    End Sub


    Private Sub DataGridView1_CellContentClick(sender As Object, e As DataGridViewCellEventArgs) Handles DataGridView1.CellContentClick
        Try
            If e.ColumnIndex = 8 Then
                If e.RowIndex.ToString = "0" Then
                    Dim puertoactual As String
                    puertoactual = DataGridView1(1, 0).Value.ToString()
                    Try
                        listajugadores.Close()
                        listajugadores.Show()
                        Dim client2302 As New WebClient()
                        Dim stream2302 As Stream = client2302.OpenRead("https://api.gamerlabs.net/?type=arma3&host=188.165.200.127&port=" & puertoactual)
                        Dim reader2302 As New StreamReader(stream2302)
                        Dim jsonData2302 As String = reader2302.ReadToEnd()
                        Dim parsedObject = JObject.Parse(jsonData2302)
                        Dim jugadores = parsedObject("data")("players")
                        For Each players In jugadores
                            Dim nombre As String = players("name")
                            Dim URL As String = players("score")
                            Dim source As String = players("time")
                            Dim cortartiempo As String = source.Substring(0, source.IndexOf("."))
                            Dim ts As TimeSpan = TimeSpan.FromSeconds(cortartiempo) 'New TimeSpan(0, 0, 0, 227, 0)
                            Dim mydate As DateTime = New DateTime(ts.Ticks)
                            listajugadores.DataGridView1.Rows.Add(nombre, mydate.ToString(("HH:mm")))
                        Next
                    Catch ex As Exception
                    End Try
                End If
                If e.RowIndex.ToString = "1" Then
                    Dim puertoactual As String
                    puertoactual = DataGridView1(1, 1).Value.ToString()
                    Try
                        listajugadores.Close()
                        listajugadores.Show()
                        Dim client2302 As New WebClient()
                        Dim stream2302 As Stream = client2302.OpenRead("https://api.gamerlabs.net/?type=arma3&host=188.165.200.127&port=" & puertoactual)
                        Dim reader2302 As New StreamReader(stream2302)
                        Dim jsonData2302 As String = reader2302.ReadToEnd()
                        Dim parsedObject = JObject.Parse(jsonData2302)
                        Dim jugadores = parsedObject("data")("players")
                        For Each players In jugadores
                            Dim nombre As String = players("name")
                            Dim URL As String = players("score")
                            Dim source As String = players("time")
                            Dim cortartiempo As String = source.Substring(0, source.IndexOf("."))
                            Dim ts As TimeSpan = TimeSpan.FromSeconds(cortartiempo) 'New TimeSpan(0, 0, 0, 227, 0)
                            Dim mydate As DateTime = New DateTime(ts.Ticks)
                            listajugadores.DataGridView1.Rows.Add(nombre, mydate.ToString(("HH:mm")))
                        Next
                    Catch ex As Exception
                    End Try
                End If
                If e.RowIndex.ToString = "2" Then
                    Dim puertoactual As String
                    puertoactual = DataGridView1(1, 2).Value.ToString()
                    Try
                        listajugadores.Close()
                        listajugadores.Show()
                        Dim client2302 As New WebClient()
                        Dim stream2302 As Stream = client2302.OpenRead("https://api.gamerlabs.net/?type=arma3&host=188.165.200.127&port=" & puertoactual)
                        Dim reader2302 As New StreamReader(stream2302)
                        Dim jsonData2302 As String = reader2302.ReadToEnd()
                        Dim parsedObject = JObject.Parse(jsonData2302)
                        Dim jugadores = parsedObject("data")("players")
                        For Each players In jugadores
                            Dim nombre As String = players("name")
                            Dim URL As String = players("score")
                            Dim source As String = players("time")
                            Dim cortartiempo As String = source.Substring(0, source.IndexOf("."))
                            Dim ts As TimeSpan = TimeSpan.FromSeconds(cortartiempo) 'New TimeSpan(0, 0, 0, 227, 0)
                            Dim mydate As DateTime = New DateTime(ts.Ticks)
                            listajugadores.DataGridView1.Rows.Add(nombre, mydate.ToString(("HH:mm")))
                        Next
                    Catch ex As Exception
                    End Try
                End If

                If e.RowIndex.ToString = "3" Then
                    Dim puertoactual As String
                    puertoactual = DataGridView1(1, 3).Value.ToString()
                    Try
                        listajugadores.Close()
                        listajugadores.Show()
                        Dim client2302 As New WebClient()
                        Dim stream2302 As Stream = client2302.OpenRead("https://api.gamerlabs.net/?type=arma3&host=188.165.200.127&port=" & puertoactual)
                        Dim reader2302 As New StreamReader(stream2302)
                        Dim jsonData2302 As String = reader2302.ReadToEnd()
                        Dim parsedObject = JObject.Parse(jsonData2302)
                        Dim jugadores = parsedObject("data")("players")
                        For Each players In jugadores
                            Dim nombre As String = players("name")
                            Dim URL As String = players("score")
                            Dim source As String = players("time")
                            Dim cortartiempo As String = source.Substring(0, source.IndexOf("."))
                            Dim ts As TimeSpan = TimeSpan.FromSeconds(cortartiempo) 'New TimeSpan(0, 0, 0, 227, 0)
                            Dim mydate As DateTime = New DateTime(ts.Ticks)
                            listajugadores.DataGridView1.Rows.Add(nombre, mydate.ToString(("HH:mm")))
                        Next
                    Catch ex As Exception
                    End Try
                End If
            End If
        Catch ex As Exception

        End Try

    End Sub


    Private Sub FlatComboBox1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles FlatComboBox1.SelectedIndexChanged
        CheckedListBox1.BeginUpdate()
        Call leermods()
        CheckedListBox1.EndUpdate()
    End Sub

    Private Sub FlatTrackBar1_Scroll(sender As Object) Handles FlatTrackBar1.Scroll
        FlatTextBox1.Text = FlatTrackBar1.Value.ToString
    End Sub

    Private Sub FlatTrackBar3_Scroll(sender As Object) Handles FlatTrackBar3.Scroll
        FlatTextBox3.Text = FlatTrackBar3.Value.ToString
    End Sub

    Private Sub FlatTrackBar2_Scroll(sender As Object) Handles FlatTrackBar2.Scroll
        FlatTextBox2.Text = FlatTrackBar2.Value.ToString
    End Sub

    Private Sub FlatTrackBar4_Scroll(sender As Object) Handles FlatTrackBar4.Scroll
        FlatTextBox4.Text = FlatTrackBar4.Value.ToString
    End Sub
    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        If ListBox2.SelectedItem = Nothing Then
            MessageBox.Show("Debes elegir un perfil para que sea el favorito.",
                    "IMPORTANTE!",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Exclamation,
                    MessageBoxDefaultButton.Button1)
        Else
            Dim item As String
            If ListBox2.SelectedItem = Nothing Then
                item = Nothing
            Else
                item = ListBox2.SelectedItem
                ListBox2.Items.Remove(item)
                ListBox2.Items.Insert(0, item)
                ListBox2.SelectedIndex = 0
            End If
            Try
                FlatComboBox1.Items.Clear()
                For Each File As String In My.Computer.FileSystem.GetFiles _
         (programDataFilePath, FileIO.SearchOption.SearchTopLevelOnly, "*.txt")
                    FlatComboBox1.Items.Add(System.IO.Path.GetFileNameWithoutExtension(File))
                Next
            Catch ex As Exception
            End Try
            Dim perfil As String
            perfil = ListBox2.SelectedItem
            FlatComboBox1.Items.Remove(perfil)
            FlatComboBox1.Items.Insert(0, perfil)
            FlatComboBox1.SelectedIndex = 0
            favorito = FlatComboBox1.SelectedItem.ToString
        End If
    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Dim mods As String
        Dim prioridades As String
        If ListBox2.SelectedItem = Nothing Then
            MessageBox.Show("Debes elegir el perfil que deseas borrar.",
                             "IMPORTANTE!",
                             MessageBoxButtons.OK,
                             MessageBoxIcon.Exclamation,
                             MessageBoxDefaultButton.Button1)
        Else
            mods = programDataFilePath & "\" & ListBox2.SelectedItem.ToString & ".txt"
            prioridades = programDataFilePath & "\" & ListBox2.SelectedItem.ToString & ".prioridad.dat"
            If ListBox2.SelectedItem = "Por Defecto" Then
                MessageBox.Show("Ese es el perfil Por Defecto, no puedes borrarlo.",
                                           "ATENCIÓN!",
                                           MessageBoxButtons.OK,
                                           MessageBoxIcon.Exclamation,
                                           MessageBoxDefaultButton.Button1)
            Else
                Try
                    My.Computer.FileSystem.DeleteFile(mods)
                    My.Computer.FileSystem.DeleteFile(prioridades)
                    ListBox2.Items.Clear()
                    FlatComboBox1.Items.Clear()
                    Try
                        For Each File As String In My.Computer.FileSystem.GetFiles _
                 (programDataFilePath, FileIO.SearchOption.SearchTopLevelOnly, "*.txt")
                            ListBox2.Items.Add(System.IO.Path.GetFileNameWithoutExtension(File))
                            FlatComboBox1.Items.Add(System.IO.Path.GetFileNameWithoutExtension(File))
                        Next
                        FlatComboBox1.SelectedItem = "Por Defecto"
                    Catch ex As Exception
                    End Try
                Catch ex As Exception
                End Try
            End If
        End If
    End Sub




    Private Sub CopiarParametrosToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles CopiarParametrosToolStripMenuItem.Click
        Call construirparametro()
        Try
            Clipboard.SetText(parametros)
            MessageBox.Show("Los parámetros se han copiado correctamente en su portapapeles.",
                                    "COPIADO!",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Information,
                                    MessageBoxDefaultButton.Button1)
        Catch ex As Exception
            MessageBox.Show("No hay nada para copiar")
        End Try
    End Sub

    Private Sub FlatButton3_Click(sender As Object, e As EventArgs) Handles FlatButton3.Click
        Dim fd As OpenFileDialog = New OpenFileDialog()
        Dim strFileName As String
        fd.Title = "Selecciona la ruta de Arma 3.exe"
        fd.Filter = "All files (*.*)|*.*|All files (*.*)|*.*"
        fd.FilterIndex = 2
        fd.RestoreDirectory = True
        If fd.ShowDialog() = DialogResult.OK Then
            strFileName = fd.FileName
            FlatTextBox5.Text = strFileName
        End If
    End Sub

    Private Sub FlatButton4_Click(sender As Object, e As EventArgs) Handles FlatButton4.Click
        FolderBrowserDialog1.Description = "Selecciona un directorio de addons de Arma 3"
        If FolderBrowserDialog1.ShowDialog() = DialogResult.OK Then
            FlatTextBox8.Text = FolderBrowserDialog1.SelectedPath
        End If
    End Sub

    Private Sub FlatButton6_Click(sender As Object, e As EventArgs) Handles FlatButton6.Click
        Dim fd As OpenFileDialog = New OpenFileDialog()
        Dim strFileName As String
        fd.Title = "Selecciona el exe que quieras añadir"
        fd.Filter = "All files (*.*)|*.*|All files (*.*)|*.*"
        fd.FilterIndex = 2
        fd.RestoreDirectory = True
        If fd.ShowDialog() = DialogResult.OK Then
            strFileName = fd.FileName
            CheckedListBox3.Items.Add(strFileName)
        End If
    End Sub

    Private Sub FlatButton5_Click(sender As Object, e As EventArgs) Handles FlatButton5.Click
        If CheckedListBox3.SelectedItem = Nothing Then
        Else
            CheckedListBox3.Items.Remove(CheckedListBox3.SelectedItem)
        End If
    End Sub

    Private Sub Button7_Click(sender As Object, e As EventArgs) Handles Button7.Click
        ProgressBar2.Visible = True
        Dim path As String
        path = My.Application.Info.DirectoryPath
        Dim actualizador As String
        actualizador = path + "\Actualizador.exe"
        If File.Exists(actualizador) Then
            My.Computer.FileSystem.DeleteFile(actualizador)
        End If
        wc = New System.Net.WebClient()
        AddHandler wc.DownloadProgressChanged, AddressOf OnDownloadProgressChanged
        AddHandler wc.DownloadFileCompleted, AddressOf OnFileDownloadCompleted
        wc.DownloadFileAsync(New Uri("https://www.dropbox.com/s/xvllvdbwtt0946n/Actualizador.exe?raw=1"), actualizador)
    End Sub

    Private Sub FlatComboBox2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles FlatComboBox2.SelectedIndexChanged

    End Sub

    Private Sub FlatTextBox7_TextChanged(sender As Object, e As EventArgs) Handles FlatTextBox7.TextChanged
        If FlatTextBox7.Text = "Contraseñainthenight" Then
            MessageBox.Show("Los botones ocultos ahora se encuentran visibles",
                                 "Información!",
                                 MessageBoxButtons.OK,
                                 MessageBoxIcon.Information,
                                 MessageBoxDefaultButton.Button1)
            Button7.Visible = True
        End If

    End Sub


    Private Sub CheckedListBox2_SelectedIndexChanged(sender As Object, e As EventArgs) Handles CheckedListBox2.SelectedIndexChanged
        ListBox3.Items.Clear()
        If CheckedListBox2.CheckedItems.Count > 0 Then
            For i As Integer = 0 To CheckedListBox2.CheckedItems.Count - 1
                ListBox3.Items.Add(CheckedListBox2.CheckedItems(i))
            Next
        End If
    End Sub








    Private Sub PictureBox1_Click(sender As Object, e As EventArgs) Handles PictureBox1.Click
        Process.Start("www.clanesus.com")
    End Sub

    Private Sub TabPage3_Click(sender As Object, e As EventArgs) Handles TabPage3.Click

    End Sub

    Private Sub Button9_Click(sender As Object, e As EventArgs) Handles Button9.Click
        Call twitch()
    End Sub
    Sub twitch()
        Try
            Dim jsondiablo As String = New WebClient() _
   .DownloadString("https://api.twitch.tv/kraken/streams/sanchez_esus")
            Dim rootdiablo As JObject = JObject.Parse(jsondiablo)
            Dim streamdiablo As JToken = rootdiablo("stream")
            If Not streamdiablo.ToString = "" Then
                PictureBox8.Image = My.Resources.online
                Dim game As String = streamdiablo("game").ToString()
                Dim viewers As String = streamdiablo("viewers").ToString()
                Label13.Text = game
                Label12.Text = "Viendo = " + viewers
            Else
                PictureBox8.Image = My.Resources.offline
                Label13.Text = "..."
                Label12.Text = "..."
            End If
        Catch ex As Exception

        End Try


        Try
            Dim jsonsanchez As String = New WebClient() _
.DownloadString("https://api.twitch.tv/kraken/streams/diablo_esus")
            Dim rootsanchez As JObject = JObject.Parse(jsonsanchez)
            Dim streamsanchez As JToken = rootsanchez("stream")
            If Not streamsanchez.ToString = "" Then
                PictureBox9.Image = My.Resources.online
                Dim game As String = streamsanchez("game").ToString()
                Dim viewers As String = streamsanchez("viewers").ToString()
                Label9.Text = game
                Label8.Text = "Viendo = " + viewers
            Else
                PictureBox9.Image = My.Resources.offline
                Label9.Text = "..."
                Label8.Text = "..."
            End If
        Catch ex As Exception

        End Try



        Try

            Dim jsonpit As String = New WebClient() _
    .DownloadString("https://api.twitch.tv/kraken/streams/lupago93")
            Dim rootpit As JObject = JObject.Parse(jsonpit)
            Dim streampit As JToken = rootpit("stream")
            If Not streampit.ToString = "" Then
                PictureBox10.Image = My.Resources.online
                Dim game As String = streampit("game").ToString()
                Dim viewers As String = streampit("viewers").ToString()
                Label7.Text = game
                Label6.Text = "Viendo = " + viewers
            Else
                PictureBox10.Image = My.Resources.offline
                Label7.Text = "..."
                Label6.Text = "..."
            End If
        Catch ex As Exception

        End Try

        Try
            Dim jsonlupago As String = New WebClient() _
    .DownloadString("https://api.twitch.tv/kraken/streams/pitesus")
            Dim rootlupago As JObject = JObject.Parse(jsonlupago)
            Dim streamlupago As JToken = rootlupago("stream")
            If Not streamlupago.ToString = "" Then
                PictureBox11.Image = My.Resources.online
                Dim game As String = streamlupago("game").ToString()
                Dim viewers As String = streamlupago("viewers").ToString()
                Label11.Text = game
                Label10.Text = "Viendo = " + viewers
            Else
                PictureBox11.Image = My.Resources.offline
                Label11.Text = "..."
                Label10.Text = "..."
            End If
        Catch ex As Exception

        End Try

    End Sub


    Private Sub PictureBox5_Click(sender As Object, e As EventArgs) Handles PictureBox5.Click
        Process.Start("http://www.twitch.tv/diablo_esus")
    End Sub

    Private Sub PictureBox4_Click(sender As Object, e As EventArgs) Handles PictureBox4.Click
        Process.Start("http://www.twitch.tv/sanchez_esus")

    End Sub

    Private Sub PictureBox7_Click(sender As Object, e As EventArgs) Handles PictureBox7.Click
        Process.Start("http://www.twitch.tv/lupago93")

    End Sub

    Private Sub PictureBox6_Click(sender As Object, e As EventArgs) Handles PictureBox6.Click
        Process.Start("http://www.twitch.tv/pitesus")

    End Sub


    Private Sub RichTextBox2_TextChanged(sender As Object, e As EventArgs) Handles RichTextBox2.TextChanged
        Dim lineasescritas As New List(Of String)
        For Each s As String In RichTextBox2.Lines
            If Not s.Trim.Equals(String.Empty) Then
                lineasescritas.Add(s)
            End If
        Next
        RichTextBox2.Lines = lineasescritas.ToArray
    End Sub


    Private Sub LanzarArma3exeToolStripMenuItem_Click(sender As Object, e As EventArgs) Handles LanzarArma3exeToolStripMenuItem.Click
        Call construirparametro()
        For Each itemChecked In CheckedListBox3.CheckedItems
            Try
                Dim elegido As String
                elegido = itemChecked
                Process.Start(elegido)
            Catch ex As Exception

            End Try
        Next
        Process.Start(arma3exe, parametros)
        Dim index = FlatComboBox2.SelectedIndex
        Select Case index
            Case 0

            Case 1
                Me.WindowState = FormWindowState.Minimized
            Case 2
                Me.Close()
        End Select
    End Sub

    Private Sub FlatCheckBox12_CheckedChanged(sender As Object) Handles FlatCheckBox12.CheckedChanged
        If FlatCheckBox12.Checked = True Then
            FlatTrackBar1.Visible = True
            FlatTextBox1.Visible = True
        Else
            FlatTrackBar1.Visible = False
            FlatTextBox1.Visible = False
            FlatTrackBar1.Value = Nothing
            FlatTextBox1.Text = Nothing
        End If
    End Sub

    Private Sub FlatCheckBox13_CheckedChanged(sender As Object) Handles FlatCheckBox13.CheckedChanged
        If FlatCheckBox13.Checked = True Then
            FlatTrackBar3.Visible = True
            FlatTextBox3.Visible = True
        Else
            FlatTrackBar3.Visible = False
            FlatTextBox3.Visible = False
            FlatTrackBar3.Value = Nothing
            FlatTextBox3.Text = Nothing
        End If
    End Sub

    Private Sub FlatCheckBox14_CheckedChanged(sender As Object) Handles FlatCheckBox14.CheckedChanged
        If FlatCheckBox14.Checked = True Then
            FlatTrackBar2.Visible = True
            FlatTextBox2.Visible = True
        Else
            FlatTrackBar2.Visible = False
            FlatTextBox2.Visible = False
            FlatTrackBar2.Value = Nothing
            FlatTextBox2.Text = Nothing
        End If
    End Sub

    Private Sub FlatCheckBox15_CheckedChanged(sender As Object) Handles FlatCheckBox15.CheckedChanged
        If FlatCheckBox15.Checked = True Then
            FlatTrackBar4.Visible = True
            FlatTextBox4.Visible = True
        Else
            FlatTrackBar4.Visible = False
            FlatTextBox4.Visible = False
            FlatTrackBar4.Value = Nothing
            FlatTextBox4.Text = Nothing
        End If
    End Sub

    Private Sub FlatCheckBox16_CheckedChanged(sender As Object) Handles FlatCheckBox16.CheckedChanged
        If FlatCheckBox16.Checked = True Then
            FlatLabel1.Visible = False
            FlatLabel2.Visible = False
            FlatTextBox5.Enabled = True
            FlatTextBox8.Enabled = True
            FlatButton3.Enabled = True
            FlatButton4.Enabled = True
        Else
            FlatButton3.Enabled = False
            FlatButton4.Enabled = False
            FlatLabel1.Visible = True
            FlatLabel2.Visible = True
            FlatTextBox5.Enabled = False
            FlatTextBox8.Enabled = False
            FlatTextBox5.Text = Nothing
            FlatTextBox8.Text = Nothing
            Try
                arma3 = My.Computer.Registry.GetValue("HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node\bohemia interactive\arma 3", "main", Nothing)
                arma3exe = arma3 & "\" & "arma3.exe"
            Catch ex As Exception
            End Try
            If IsNothing(arma3) Then
                Try
                    steam = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\Valve\Steam", "SteamPath", Nothing)
                    steam = steam.Replace("/", "\")
                    arma3 = steam & "\steamapps\common\Arma 3"
                    arma3exe = arma3 + "\arma3.exe"
                Catch ex As Exception
                End Try
            Else
            End If
        End If
        FlatLabel2.Text = arma3
        FlatLabel1.Text = arma3exe
    End Sub

    Private Sub FlatCheckBox18_CheckedChanged(sender As Object) Handles FlatCheckBox18.CheckedChanged
        If FlatCheckBox18.Checked = True Then
            TextBox9.Visible = True
            TextBox10.Visible = True
            FlatLabel14.Visible = True
            FlatLabel13.Visible = True
            TextBox9.Text = "188.165.200.127"
            TextBox1.Visible = True
            FlatLabel12.Visible = True
            CheckBox1.Visible = True
        Else
            TextBox9.Visible = False
            TextBox10.Visible = False
            TextBox9.Text = Nothing
            TextBox10.Text = Nothing
            FlatLabel14.Visible = False
            TextBox1.Visible = False
            FlatLabel12.Visible = False
            FlatLabel13.Visible = False
            CheckBox1.Visible = False
        End If
    End Sub


    Private Sub FlatCheckBox11_CheckedChanged(sender As Object) Handles FlatCheckBox11.CheckedChanged
        If FlatCheckBox11.Checked = True Then
            TextBox6.Visible = True
        Else
            TextBox6.Visible = False
            TextBox6.Text = Nothing
        End If
    End Sub

    Private Sub FlatCheckBox19_CheckedChanged(sender As Object) Handles FlatCheckBox19.CheckedChanged
        If FlatCheckBox19.Checked = True Then
            FlatComboBox4.Items.Clear()
            Try
                Dim dirInfo As New DirectoryInfo(My.Computer.FileSystem.SpecialDirectories.MyDocuments + "\Arma 3 - Other Profiles")
                For Each f In dirInfo.GetDirectories()
                    For Each fl In f.GetFiles()
                        If fl.Name.EndsWith(".Arma3Profile") Then
                            FlatComboBox4.Items.Add(Uri.UnescapeDataString(f.Name))
                            Exit For
                        End If
                    Next
                Next
            Catch ex As Exception
            End Try
            FlatComboBox4.Visible = True
        Else
            FlatComboBox4.Visible = False
        End If
    End Sub

    Private Sub TabPage1_Click(sender As Object, e As EventArgs) Handles TabPage1.Click

    End Sub

    Private Sub FlatCheckBox20_CheckedChanged(sender As Object)

    End Sub

    Private Sub Button8_Click(sender As Object, e As EventArgs)
        TextBox9.Text = "188.165.200.127"
    End Sub

    Private Sub TabPage4_Click(sender As Object, e As EventArgs) Handles TabPage4.Click

    End Sub


    Private Sub TextBox9_TextChanged(sender As Object, e As EventArgs) Handles TextBox9.TextChanged

    End Sub

    Private Sub Button5_Click(sender As Object, e As EventArgs) Handles Button5.Click
        Dim mods As String
        Dim prioridades As String
        If ListBox2.SelectedItem = "Por Defecto" Then
            MessageBox.Show("Ese es el perfil por defecto, no puedes renombrarlo!",
                                    "IMPORTANTE!",
                                    MessageBoxButtons.OK,
                                    MessageBoxIcon.Exclamation,
                                    MessageBoxDefaultButton.Button1)
        ElseIf FlatTextBox7.Text <> "" Then
            mods = programDataFilePath & "\" & ListBox2.SelectedItem.ToString & ".txt"
            prioridades = programDataFilePath & "\" & ListBox2.SelectedItem.ToString & ".prioridad.dat"
            My.Computer.FileSystem.RenameFile(mods,
    FlatTextBox7.Text & ".txt")
            My.Computer.FileSystem.RenameFile(prioridades,
    FlatTextBox7.Text & ".prioridad.dat")
            FlatComboBox1.Items.Clear()
            ListBox2.Items.Clear()
            For Each File As String In My.Computer.FileSystem.GetFiles _
                 (programDataFilePath, FileIO.SearchOption.SearchTopLevelOnly, "*.txt")
                ListBox2.Items.Add(System.IO.Path.GetFileNameWithoutExtension(File))
                FlatComboBox1.Items.Add(System.IO.Path.GetFileNameWithoutExtension(File))
            Next
            FlatComboBox1.SelectedItem = "Por Defecto"
            FlatTextBox7.Text = Nothing
        Else
            If ListBox2.SelectedItem = Nothing Then
                MessageBox.Show("Debes seleccionar el perfil que será renombrado",
                        "IMPORTANTE!",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Exclamation,
                        MessageBoxDefaultButton.Button1)
            Else
                MessageBox.Show("Tienes que escribir el nombre con el que será renombrado el perfil.",
                        "IMPORTANTE!",
                        MessageBoxButtons.OK,
                        MessageBoxIcon.Exclamation,
                        MessageBoxDefaultButton.Button1)
            End If

        End If
    End Sub

    Private Sub A3StartupParametersWikiToolStripMenuItem_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub tsbAbout_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub tsbUsage_Click(sender As Object, e As EventArgs)

    End Sub

    Private Sub AcercaDeToolStripMenuItem_Click(sender As Object, e As EventArgs)

    End Sub




    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        WebBrowser1.Refresh()
        WebBrowser1.Refresh(WebBrowserRefreshOption.Completely)
    End Sub

    Private Sub FlatButton7_Click(sender As Object, e As EventArgs) Handles FlatButton7.Click
        Dim result As DialogResult
        result = MessageBox.Show("Steam realizará una comprobación de los archivos de su juego ¿Desea hacerlo?", "Información", MessageBoxButtons.YesNo, MessageBoxIcon.Information)
        If result = DialogResult.No Then
        ElseIf result = DialogResult.Yes Then
            Process.Start("steam://validate/107410")
        End If


    End Sub

    Private Sub Button8_Click_1(sender As Object, e As EventArgs) Handles Button8.Click
        Process.Start("https://bitbucket.org/Krachenko/esus-launcher-a3/src")
    End Sub
End Class
