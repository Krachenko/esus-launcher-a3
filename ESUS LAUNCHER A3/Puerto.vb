﻿Imports System.Threading
Imports System.IO
Imports Newtonsoft.Json
Imports System.Net
Imports Newtonsoft.Json.Linq
Imports MaterialSkin
Imports System.Environment
Imports System.Xml
Imports Microsoft.VisualBasic.FileIO
Imports System.Collections.Specialized
Imports System.Text
Imports System.IO.Path
Imports Microsoft.VisualBasic.Devices
Imports System.Management
Imports Microsoft.Win32
Imports System.Net.Sockets
Imports System
Imports System.IO.Compression
Imports System.Globalization
Imports System.Windows.Forms
Public Class Puerto
    Dim mods As String = Nothing
    Dim directorio As String
    Dim custom As String
    Dim pantalla As String
    Dim ventana As String
    Dim isChecked As Boolean
    Dim window As String
    Dim splash As String
    Dim world As String
    Dim mundovacio As String
    Dim nopause As String
    Dim solopbo As String
    Dim ningunapausa As String
    Dim malloc As String
    Dim mostrarerrores As String
    Dim showscripterror As String
    Dim norpt As String
    Dim nologs As String
    Dim cpus As Integer
    Dim gpu As String
    Dim ram As String
    Dim cpucount As String
    Dim carpetadll As String
    Dim hilosvirtuales As String
    Dim hyperthreading As String
    Dim extrahilos As String
    Dim dias As String
    Dim lunes As String
    Dim martes As String
    Dim miercoles As String
    Dim jueves As String
    Dim viernes As String
    Dim sabado As String
    Dim domingo As String
    Dim seleccion As String
    Dim carpetaa3 As String
    Dim StringCol As New Specialized.StringCollection
    Dim File As File
    Dim pordefecto As String = "Por Defecto.txt"
    Dim pordefectoprioridad As String = "Por Defecto.prioridad.dat"
    Dim perfilesusprioridad As String = "perfilesus(noborrar).prioridad.dat"
    Dim perfilesus As String = "perfilesus(noborrar).dat"
    Dim saltarintro As String
    Dim dx9 As String
    Dim arma3 As String
    Dim parametros As String
    Dim config As String = "configuracion.cfg"
    Dim arma3exe As String
    Dim steam As String
    Dim favorito As String = "nada"
    Dim rutacortada As String = Nothing
    Dim externos As String = "pexternos.cfg"
    Dim externoselegidos As String = "pexternoselegidos.cfg"
    Dim wc As System.Net.WebClient
    Dim rc As System.Net.WebClient


    Public Property TextAlign As HorizontalAlignment
    Private programDataFilePath As String = Combine(GetFolderPath(SpecialFolder.ApplicationData), _
                                                My.Application.Info.AssemblyName)
    Private perfilpordefecto As String = Combine(programDataFilePath, pordefecto)
    Private perfilpordefectoprioridad As String = Combine(programDataFilePath, pordefectoprioridad)
    Private perfilesuson As String = Combine(programDataFilePath, perfilesus)
    Private perfilesusprioridadon As String = Combine(programDataFilePath, perfilesusprioridad)
    Private configuracion As String = Combine(programDataFilePath, config)
    Private programasexternos As String = Combine(programDataFilePath, externos)
    Private programasexternoselegidos As String = Combine(programDataFilePath, externoselegidos)
    Sub lanzarpuerto()
        If FlatTextBox1.Text = Nothing Then
            MsgBox("Necesitas escribir un puerto!")
        Else
            Form1.TextBox10.Text = FlatTextBox1.Text
            Dim veintinueve As String = System.IO.File.ReadAllLines(configuracion)(29)
            If veintinueve = "nada" Then
                Form1.FlatComboBox2.SelectedIndex = 0
            ElseIf veintinueve = "cerrar" Then
                Form1.FlatComboBox2.SelectedIndex = 1
            Else : veintinueve = "minimizar"
                Form1.FlatComboBox2.SelectedIndex = 2
            End If
            Call Form1.lanzarjuego()
            Me.Close()
        End If
    End Sub
    Private Sub FlatButton1_Click(sender As Object, e As EventArgs) Handles FlatButton1.Click
        Call lanzarpuerto()

    End Sub
    Protected Overrides Function ProcessCmdKey(ByRef msg As System.Windows.Forms.Message, _
                                              ByVal keyData As System.Windows.Forms.Keys) _
                                              As Boolean

        If msg.WParam.ToInt32() = CInt(Keys.Enter) Then
            Call lanzarpuerto()
            Return True
        End If
        Return MyBase.ProcessCmdKey(msg, keyData)
    End Function
    Private Sub FormSkin1_Click(sender As Object, e As EventArgs) Handles FormSkin1.Click

    End Sub

    Private Sub FlatButton2_Click(sender As Object, e As EventArgs) Handles FlatButton2.Click
        Form1.WindowState = FormWindowState.Normal
        Me.Close()
    End Sub
End Class