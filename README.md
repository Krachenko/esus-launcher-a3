Es un lanzador para el videojuego Arma 3.

INSTALACIÓN.

- Descargamos el archivo .zip

- Ejecutamos EsusLaunchera3.exe

CARACTERÍSTICAS:

-Cargar  addons de forma dinámica 

-Es Totalmente Portable (SIN INSTALACIÓN)

-Los datos se consiguen mediante un query directo a los servidores de steam, parseados a json por lo que la diferencia de velocidad entre que un servidor pasa a estar online y lo podemos ver en la lista es muy rápida.

- Podemos exportar nuestros mods clicados

- Visor de Teamspeak con botón para conectarnos directamente.


-Client Side dinámicos ya no hace falta actualizar el lanzador cada vez que se añade 


ENLACE DE DESCARGA:

[https://bitbucket.org/Krachenko/esus-launcher-a3/downloads/EsusLauncherA3.zip](https://bitbucket.org/Krachenko/esus-launcher-a3/downloads/EsusLauncherA3.zip)